<?php

return [
  'gcm' => [
      'priority' => 'normal',
      'dry_run' => false,
      'apiKey' => 'My_ApiKey',
  ],
  'fcm' => [
        'priority' => 'normal',
        'dry_run' => false,
        'apiKey' => 'AAAA7f3Niu4:APA91bE0hbbxKPvV49gGmQepbwhdkmO35XKdmoiANq1zeDmAS0PjYrew38_tH8sFE9IKxB7kyCddcMAfUtCavocYM0TLrMD92o_9b-vXT5_ovuQnT5wkEh4mzl_K8YoR_YIBv_njuAEPGRQ1puA4eVKSyAPjyKTl4A',
  ],
  'apn' => [
      'certificate' => __DIR__ . '/iosCertificates/apns-dev-cert.pem',
      'passPhrase' => '1234', //Optional
      'passFile' => __DIR__ . '/iosCertificates/yourKey.pem', //Optional
      'dry_run' => true
  ]
];