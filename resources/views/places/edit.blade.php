
@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="title">Establecimientos</h5>
                </div>
                <div class="card-body">
                    {!! Form::model($place, ['route' => ['places.update', $place->id], 'method' => 'patch', 'files' => true]) !!}
                    <div class="row">
                        @include('places.fields')
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

