<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Nombre:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Phone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phone', 'Telefono:') !!}
    {!! Form::text('phone', null, ['class' => 'form-control']) !!}
</div>

<!-- Website Field -->
<div class="form-group col-sm-6">
    {!! Form::label('website', 'Sitio web:') !!}
    {!! Form::text('website', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('image', 'Imagen principal') !!}
    {!! Form::file('image', ['class' => 'form-control']) !!}
</div>

{{--
<!-- Geolocation Field -->
<div class="form-group col-sm-6">
    {!! Form::label('geolocation', 'Geolocation:') !!}
    {!! Form::text('geolocation', null, ['class' => 'form-control']) !!}
</div>
--}}

<!-- Category Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('category_id', 'Categoria:') !!}
    {!! Form::select('category_id',$categories ?? [], $place->category_id ?? null,['class'=>'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Estado:') !!}
    {!! Form::select('status',\App\Util\PromotionTypes::status() ?? [], $place->status ?? null,['class'=>'form-control']) !!}
</div>


<!-- Rating Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rating', 'Rating:') !!}
    {!! Form::number('rating', null, ['class' => 'form-control']) !!}
</div>

{!! \App\Helpers\InputTranslation::textarea('Descripción','description', $place ?? null) !!}

<div class="form-group col-sm-6">
    {!! Form::label('loyalty', 'Fidelización de clientes:') !!}
    {!! Form::select('loyalty',\App\Util\PromotionTypes::status() ?? [], $place->loyalty ?? null,['class'=>'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('points_value', '1 punto equivale a (Euros)') !!}
    {!! Form::number('points_value', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('latitude', 'Latitud:') !!}
    {!! Form::text('latitude', null, ['class' => 'form-control', 'readonly' => 'true', 'id' => 'latitude']) !!}
</div>
<div class="form-group col-sm-6">
    {!! Form::label('longitude', 'Longitud:') !!}
    {!! Form::text('longitude', null, ['class' => 'form-control', 'readonly' => 'true', 'id' => 'longitude']) !!}
</div>
<!-- Address Field -->
<div class="form-group col-sm-6">
    {!! Form::label('address', 'Dirección:') !!}
    {!! Form::text('address', null, ['class' => 'form-control', 'id' => 'pac-input']) !!}
</div>

<div class="form-group col-sm-12">
    <div id="map" style="height: 300px"></div>
    <script>
        @isset($place)
            window.placeUbication = @json($place->geolocation)
                @endisset

            // This example adds a search box to a map, using the Google Place Autocomplete
            // feature. People can enter geographical searches. The search box will return a
            // pick list containing a mix of places and predicted search terms.

            // This example requires the Places library. Include the libraries=places
            // parameter when you first load the API. For example:
            // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

            function initAutocomplete() {
                window.map = new google.maps.Map(document.getElementById('map'), {
                    center: {lat: 41.3947688, lng: 2.0787279},
                    zoom: 13,
                    mapTypeId: 'roadmap'
                });

                if (window.placeUbication) {
                    var myLatLng = {
                        lat: window.placeUbication.coordinates[1],
                        lng: window.placeUbication.coordinates[0]
                    };
                    $('#latitude').val(window.placeUbication.coordinates[1]);
                    $('#longitude').val(window.placeUbication.coordinates[0]);
                    window.map.panTo(myLatLng);
                    window.marker = new google.maps.Marker({
                        position: myLatLng,
                        map: map,
                        title: 'Place',
                        draggable: true,
                    });
                    google.maps.event.addListener(window.marker, 'dragend', function (evt) {
                        var lat = marker.getPosition().lat();
                        var lng = marker.getPosition().lng();
                        $('#latitude').val(lat);
                        $('#longitude').val(lng);
                    })
                }

                // Create the search box and link it to the UI element.
                var input = document.getElementById('pac-input');
                var searchBox = new google.maps.places.SearchBox(input);
                map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

                // Bias the SearchBox results towards current map's viewport.
                map.addListener('bounds_changed', function () {
                    searchBox.setBounds(map.getBounds());
                });

                var markers = [];
                // Listen for the event fired when the user selects a prediction and retrieve
                // more details for that place.
                searchBox.addListener('places_changed', function () {
                    var places = searchBox.getPlaces();

                    if (places.length == 0) {
                        return;
                    }

                    // Clear out the old markers.

                    // For each place, get the icon, name and location.
                    var bounds = new google.maps.LatLngBounds();
                    place = places[0];
                    if (!place.geometry) {
                        console.log("Returned place contains no geometry");
                        return;
                    }
                    window.map.panTo(place.geometry.location);

                    if (window.marker) {
                        window.marker.setPosition(place.geometry.location);
                        $('#latitude').val(place.geometry.location.lat);
                        $('#longitude').val(place.geometry.location.lng);
                    } else {
                        $('#latitude').val(place.geometry.location.lat);
                        $('#longitude').val(place.geometry.location.lng);
                        window.marker = new google.maps.Marker({
                            position: place.geometry.location,
                            map: map,
                            title: 'Place',
                            draggable: true
                        });
                        google.maps.event.addListener(window.marker, 'dragend', function (evt) {
                            var lat = marker.getPosition().lat();
                            var lng = marker.getPosition().lng();
                            $('#latitude').val(lat);
                            $('#longitude').val(lng);
                        })
                    }
                });


            }

    </script>

</div>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCHwzfaqqEwXah9fHzMT-V_zwawP4aMnGY&libraries=places&callback=initAutocomplete"
        async defer></script>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('places.index') !!}" class="btn btn-default">Cancelar</a>
</div>
