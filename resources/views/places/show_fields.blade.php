<div class="row">
    <div class="col col-lg-12">
        <div class="info-content tablas">
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">Imagen</th>
                    <th scope="col">Acción</th>
                </tr>
                </thead>
                <tbody>
                @if($place->photos != null)
                    @foreach($place->photos as $img)
                        <tr>
                            <td>
                                <img src="/images/{{$img}}" class="img-responsive" style="width: 50px;">
                            </td>
                            <td>
                                {!! Form::model($place, ['route' => ['places.update', $place->id], 'method' => 'patch','files' => true]) !!}
                                <input type="hidden" name="action" value="remove_image">
                                <input type="hidden" name="photo_name" value="{{$img}}">
                                {!! Form::submit('Eliminar', ['class' => 'btn btn-danger']) !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>

</div>
<div class="row">
    <div class="col col-lg-12">
        {!! Form::model($place, ['route' => ['places.update', $place->id], 'method' => 'patch','files' => true]) !!}
        {!! Form::label('photo', 'Agregar imagen') !!}
        {!! Form::file('photo', ['class' => 'form-control']) !!}
        {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
        {!! Form::close() !!}
    </div>
</div>
<br/>
<br/>
<div class="row">
    <div class="col col-lg-4">
        <a href="/places/{{$place->_id}}/points" class="btn btn-primary">Registro de puntos</a>
    </div>
    <div class="col col-lg-4">
        <a href="/places/{{$place->_id}}/bonus" class="btn btn-info">Registro de bonos</a>
    </div>
</div>