@extends('layouts.app')
@section('content')
    <div class="container-fluid my-5 py-5">
        <!--#Notas y titulares-->
        <div class="col-12 pb-2 mt-3">
            <h2 class="text-center titulo-dash-int">Establecimientos</h2>
            <hr width="10%" align="center">

        </div>
        <!--#Notas y titulares-->
        <div class="info">
            <div class="info-content">
                {!! Form::open(['route' => 'places.store', 'files' => true]) !!}
                <div class="row">
                    @include('places.fields')
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection





