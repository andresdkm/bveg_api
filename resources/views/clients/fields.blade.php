<div class="form-group col-sm-6">
    {!! Form::label('role', 'Rol:') !!}
    {!! Form::select('role',\App\Util\ClientTypes::types() ?? [], $client->role ?? null,['class'=>'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('places_admin', 'Restaurantes a administrar:') !!}
    {{ Form::select('places_admin[]', $places, $client->places_admin ?? null, ['id' => 'dogs', 'multiple' => 'multiple', 'class'=>'form-control']) }}
</div>
<div class="form-group col-sm-12">

    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('clients.index') !!}" class="btn btn-default">Cancelar</a>
</div>
