@extends('layouts.app')
@section('content')
    <!--#Contenido-->
    <!--#Notas y titulares-->
    <div class="container pt-5 pb-2 mt-5 text-center">
        <h2 class="text-center titulo-dash-int">¿Qué deseas hacer?</h2>
        <hr width="10%" align="center">
        <p>Selecciona el módulo al que deseas acceder y nosotros te guiaremos en el camino.</p>
    </div>
    <!--#Notas y titulares-->
    <!--#Dashboard-->
    <div class="container-fluid mb-3">
        <!-- spinner -->
        <div class="loading" style="display: none"></div>
        <div class="row d-flex">

            <div class="col-12 col-md-3 botones-dash hvr-shrink">
                <a href="{!! route('places.index') !!}" class="btn btn-dashboard shadow-sm">
                    <i class="fa fa-shopping-bag fa-4x"></i><br/>
                    Establecimientos
                </a>
            </div>

            <div class="col-12 col-md-3 botones-dash hvr-shrink">
                <a href="{!! route('clients.index') !!}" class="btn btn-dashboard shadow-sm">
                    <i class="fa fa-user fa-4x"></i><br/>
                    Clientes
                </a>
            </div>

            <div class="col-12 col-md-3 botones-dash hvr-shrink">
                <a href="{!! route('comments.index') !!}" class="btn btn-dashboard shadow-sm">
                    <i class="fa fa-comment fa-4x"></i><br/>
                    Comentarios
                </a>
            </div>

            <div class="col-12 col-md-3 botones-dash hvr-shrink">
                <a href="{!! route('promotions.index') !!}" class="btn btn-dashboard shadow-sm">
                    <i class="fa fa-diamond fa-4x"></i><br/>
                    Promociones
                </a>
            </div>

            <div class="col-12 col-md-3 botones-dash hvr-shrink">
                <a href="{!! route('banners.index') !!}" class="btn btn-dashboard shadow-sm">
                    <i class="fa fa-percent fa-4x"></i><br/>
                    Banners
                </a>
            </div>

            <div class="col-12 col-md-3 botones-dash hvr-shrink">
                <a href="{!! route('users.index') !!}" class="btn btn-dashboard shadow-sm">
                    <i class="fa fa-user-secret fa-4x"></i><br/>
                    Administradores
                </a>
            </div>
            {{--<div class="col-12 col-md-3 botones-dash hvr-shrink">
                <a href="{!! route('patients.index') !!}" class="btn btn-dashboard shadow-sm">
                    <i class="fa fa-user fa-4x"></i><br/>
                    Pacientes
                </a>
            </div>

            <div class="col-12 col-md-3 botones-dash hvr-shrink">
                <a href="{!! route('doctors.index') !!}" class="btn btn-dashboard shadow-sm">
                    <i class="fa fa-user fa-4x"></i><br/>
                    Doctores
                </a>
            </div>

            <div class="col-12 col-md-3 botones-dash hvr-shrink">
                <a href="{!! route('specialities.index') !!}" class="btn btn-dashboard shadow-sm">
                    <i class="fa fa-user fa-4x"></i><br/>
                    Especialidades
                </a>
            </div>
--}}

        </div>
    </div>
    <!--#Dashboard-->
    </div>
@endsection
