{!! \App\Helpers\InputTranslation::input('Nombre','name', $promotion ?? null) !!}
<!-- Submit Field -->

<div class="form-group col-sm-6">
    {!! Form::label('place_id', 'Lugar:') !!}
    {!! Form::select('place_id',$places ?? [], $promotion->place_id ?? null,['class'=>'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('type', 'Tipo:') !!}
    {!! Form::select('type',\App\Util\PromotionTypes::types() ?? [], $promotion->type ?? null,['class'=>'form-control']) !!}
</div>

{!! \App\Helpers\InputTranslation::textarea('Descripción','description', $promotion ?? null) !!}

<div class="form-group col-sm-6">
    {!! Form::label('discount', 'Descuento (%):') !!}
    {!! Form::number('discount', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('amount', 'Precio Normal ($):') !!}
    {!! Form::number('amount', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('amount_offer', 'Precio con descuento($):') !!}
    {!! Form::number('amount_offer', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('image', 'Imagen:') !!}
    {!! Form::file('image', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('points', 'Puntos:') !!}
    {!! Form::number('points', null, ['class' => 'form-control']) !!}
</div>


<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('promotions.index') !!}" class="btn btn-default">Cancelar</a>
</div>
