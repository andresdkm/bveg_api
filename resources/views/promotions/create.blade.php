@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="title">Crear promoción</h5>
                </div>
                <div class="card-body">
                    {!! Form::open(['route' => 'promotions.store', 'files' => true]) !!}
                    <div class="row">

                        @include('promotions.fields')
                    </div>

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
@endsection



