
@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="title">Bonos</h5>
                </div>
                <div class="card-body">
                    @include('bonuses.table')
                </div>
            </div>
        </div>
    </div>
@endsection







