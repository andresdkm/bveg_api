<!DOCTYPE html>
<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta http-equiv = "Content-Type" content = "text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/css/bootstrap.min.css" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/estilos.css">
    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/glyphicons.css">
    <title>BVEG CMS</title>
    <!--Favicon-->
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="/favicon/apple-touch-icon-57x57.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/favicon/apple-touch-icon-114x114.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="public/favicon/apple-touch-icon-72x72.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/favicon/apple-touch-icon-144x144.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="60x60" href="/favicon/apple-touch-icon-60x60.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="/favicon/apple-touch-icon-120x120.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="76x76" href="/favicon/apple-touch-icon-76x76.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="/favicon/apple-touch-icon-152x152.png"/>
    <link rel="icon" type="image/png" href="/favicon/favicon-196x196.png" sizes="196x196"/>
    <link rel="icon" type="image/png" href="/favicon/favicon-96x96.png" sizes="96x96"/>
    <link rel="icon" type="image/png" href="/favicon/favicon-32x32.png" sizes="32x32"/>
    <link rel="icon" type="image/png" href="/favicon/favicon-16x16.png" sizes="16x16"/>
    <link rel="icon" type="image/png" href="/favicon/favicon-128.png" sizes="128x128"/>
    <meta name="application-name" content="&nbsp;"/>
    <meta name="msapplication-TileColor" content="#FFFFFF"/>
    <meta name="msapplication-TileImage" content="/favicon/mstile-144x144.png"/>
    <meta name="msapplication-square70x70logo" content="/favicon/mstile-70x70.png"/>
    <meta name="msapplication-square150x150logo" content="/favicon/mstile-150x150.png"/>
    <meta name="msapplication-wide310x150logo" content="/favicon/mstile-310x150.png"/>
    <meta name="msapplication-square310x310logo" content="/favicon/mstile-310x310.png"/>
    @yield('css')
</head>
<body class="hold-transition login-page">
<div class="container">
    <div class="row text-center mt-4">
        <div class="col-md-8 offset-md-2">
            <div class="mb-2"><img class="logo-reg img-fluid" src="/imgs/logo.png" style="width: 100px" alt="bveg"/></div>
            <p class="marca-sc small mt-2">BVEG V2</p>
        </div>
    </div>
    <div class="row text-center mb-3 mt-1">
        <div class="col-md-6 offset-md-3">
            <form class="text-center form-inicio"  method="post" action="{{ url('/login') }}">
                {!! csrf_field() !!}
                <h1 class="hd-karupa-1 mb-3">Si ya tienes cuenta</h1>
                <div class="form-group has-feedback {{ $errors->has('email') ? ' has-error' : '' }}">
                    <input type="email" class="form-control form-control-sm" id="cedula" name="email" placeholder="Usuario" required>
                    @if ($errors->has('email'))
                        <div class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </div>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                    <input type="password" name="password" class="form-control form-control-sm" id="password" placeholder="Contraseña" required>
                    @if ($errors->has('password'))
                        <div class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </div>
                    @endif
                </div>
                <div class="row">
                    <div class="col-6 col-md-6 col-lg-offset-3">
                        <button type="submit" class="btn btn-primary-karupa btn-block my-2">Entrar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-md-6 offset-md-3 pb-3">
            <p class="marca-sc text-center small">Ésta aplicación se visualiza y funciona mejor en los siguientes navegadores</p>
            <div class="row">
                <div class="col-6 col-md-3">
                    <a class="btn btn-gris-karupa btn-block" href="https://www.google.com/intl/es_ALL/chrome/" target="_blank"><i class="fa fa-chrome mr-2" aria-hidden="true"></i>Chrome</a>
                </div>
                <div class="col-6 col-md-3">
                    <a class="btn btn-gris-karupa btn-block" href="https://www.mozilla.org/es-ES/firefox/new/" target="_blank"><i class="fa fa-firefox mr-2" aria-hidden="true"></i>Firefox</a>
                </div>
                <div class="col-6 col-md-3">
                    <a class="btn btn-gris-karupa btn-block" href="https://support.apple.com/es_ES/downloads/safari" target="_blank"><i class="fa fa-safari mr-2" aria-hidden="true"></i>Safari</a>
                </div>
                <div class="col-6 col-md-3">
                    <a class="btn btn-gris-karupa btn-block" href="https://www.microsoft.com/es-co/windows/microsoft-edge" target="_blank"><i class="fa fa-edge mr-2" aria-hidden="true"></i>Edge</a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- /.login-box -->

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="/js/scroll-to-top.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="/js/jquery.mask.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script src="https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="{{ asset('js/jquery.inputmask.bundle.js') }}"></script>
<script src="/js/scripts.js"></script>

@yield('scripts')
</body>
</html>
