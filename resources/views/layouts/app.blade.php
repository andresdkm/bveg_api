<!doctype html>
<html lang="es">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta http-equiv = "Content-Type" content = "text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/css/bootstrap.min.css" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/estilos.css">
    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/glyphicons.css">
    <title>BVEG CMS</title>
    <!--Favicon-->
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="/favicon/apple-touch-icon-57x57.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/favicon/apple-touch-icon-114x114.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="public/favicon/apple-touch-icon-72x72.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/favicon/apple-touch-icon-144x144.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="60x60" href="/favicon/apple-touch-icon-60x60.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="/favicon/apple-touch-icon-120x120.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="76x76" href="/favicon/apple-touch-icon-76x76.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="/favicon/apple-touch-icon-152x152.png"/>
    <link rel="icon" type="image/png" href="/favicon/favicon-196x196.png" sizes="196x196"/>
    <link rel="icon" type="image/png" href="/favicon/favicon-96x96.png" sizes="96x96"/>
    <link rel="icon" type="image/png" href="/favicon/favicon-32x32.png" sizes="32x32"/>
    <link rel="icon" type="image/png" href="/favicon/favicon-16x16.png" sizes="16x16"/>
    <link rel="icon" type="image/png" href="/favicon/favicon-128.png" sizes="128x128"/>
    <meta name="application-name" content="&nbsp;"/>
    <meta name="msapplication-TileColor" content="#FFFFFF"/>
    <meta name="msapplication-TileImage" content="/favicon/mstile-144x144.png"/>
    <meta name="msapplication-square70x70logo" content="/favicon/mstile-70x70.png"/>
    <meta name="msapplication-square150x150logo" content="/favicon/mstile-150x150.png"/>
    <meta name="msapplication-wide310x150logo" content="/favicon/mstile-310x150.png"/>
    <meta name="msapplication-square310x310logo" content="/favicon/mstile-310x310.png"/>
    @yield('css')
</head>
<body>
<!--#Navbar-->
<nav class="shadow-sm navbar navbar-expand-lg navbar-light bg-navbar-dashboard fixed-top">
    <div class="logo-dashboard">
        <a href="/"><img src="/imgs/logo.png" style="width: 50px"/></a>
    </div>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarText">
        <ul class="navbar-nav ml-auto">
            <li class="navbar-text">
                <span>Bienvenid@ <strong>{{Auth::user()->name}}</strong></span>
            </li>
            <li class="nav-item">
                <a href="/home" class="nav-item nav-link nav-borde"><i class="fa fa-home nav-icon" aria-hidden="true"></i>Ir al inicio</a>
            </li>
            <li class="nav-item">
                <a href="http://karupa.com.co/guia-usuario" target="_blank" class="nav-item nav-link nav-borde"><i class="fa fa-file-text nav-icon" aria-hidden="true"></i>Guía de usuario</a>
            </li>
            <li class="nav-item">
                <a href="{!! url('/logout') !!}" class="nav-item nav-link nav-borde" ><i class="fa fa-sign-out nav-icon" aria-hidden="true"></i>Cerrar sesión</a>
            </li>
        </ul>
    </div>
</nav>
<!--#Navbar-->
@include('flash::message')
@include('adminlte-templates::common.errors')
@yield('content')
<!--#Totop-->
<button class="hvr-float" onclick="topFunction()" id="totop" title="Ir arriba"><i class="fa fa-chevron-up" aria-hidden="true"></i></button>
<!--#Totop-->
<footer>
    <div class="container-fluid mt-5 px-0">
        <span class="txt-footer py-2 fixed-bottom">&copy; 2018 - Todos los derechos reservados &reg;BVEG</span>
    </div>
</footer>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="/js/scroll-to-top.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="/js/jquery.mask.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script src="https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="{{ asset('js/jquery.inputmask.bundle.js') }}"></script>
<script src="/js/scripts.js"></script>

@yield('scripts')
<script>
    window.user = {{Auth::user()->id}};
</script>
<script type="text/javascript">
    $(window).on('load',function(){
        $('#pop-up-auto').modal('show');
    });
</script>
</body>
</html>

