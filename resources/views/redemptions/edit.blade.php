@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Redemption
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($redemption, ['route' => ['redemptions.update', $redemption->id], 'method' => 'patch']) !!}

                        @include('redemptions.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection