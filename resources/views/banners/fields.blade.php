
{!! \App\Helpers\InputTranslation::input('Titulo','title', $banner ?? null) !!}

{!! \App\Helpers\InputTranslation::input('Subtitulo','subtitle', $banner ?? null) !!}

{!! \App\Helpers\InputTranslation::textarea('Descripción','description', $banner ?? null) !!}

<div class="form-group col-sm-6">
    {!! Form::label('image', 'Imagen principal') !!}
    {!! Form::file('image', ['class' => 'form-control', 'required' => true]) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('banners.index') !!}" class="btn btn-default">Cancelar</a>
</div>
