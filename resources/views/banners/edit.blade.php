@extends('layouts.app')
@section('content')
    <div class="container-fluid my-5 py-5">
        <!--#Notas y titulares-->
        <div class="col-12 pb-2 mt-3">
            <h2 class="text-center titulo-dash-int">Editar banner</h2>
            <hr width="10%" align="center">

        </div>
        <!--#Notas y titulares-->
        <div class="info">
            <div class="info-content">
                {!! Form::model($banner, ['route' => ['banners.update', $banner->id], 'method' => 'patch', 'files' => true]) !!}
                <div class="row">
                    @include('banners.fields')
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection



