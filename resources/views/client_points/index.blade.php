@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="title">Registro de puntos</h5>
                </div>
                <div class="card-body">
                    @include('client_points.table')
                </div>
            </div>
        </div>
    </div>
@endsection



