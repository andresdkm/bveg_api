@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="title">Crear puntos</h5>
                </div>
                <div class="card-body">
                    {!! Form::open(['route' => 'clientPoints.store']) !!}
                    <div class="row">
                        @include('client_points.fields')
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

