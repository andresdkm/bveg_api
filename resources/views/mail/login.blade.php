<head>
    <style type="text/css">

        body {
            width: 100%;
            background-color: #fff;
            margin: 0;
            padding: 0;
            -webkit-font-smoothing: antialiased;
            mso-margin-top-alt: 0px;
            mso-margin-bottom-alt: 0px;
            mso-padding-alt: 0px 0px 0px 0px;
        }

        p, h1, h2, h3, h4 {
            margin-top: 0;
            margin-bottom: 0;
            padding-top: 0;
            padding-bottom: 0;
        }

        span.preheader {
            display: none;
            font-size: 1px;
        }

        html {
            width: 100%;
        }

        table {
            font-size: 12px;
            border: 0;
        }

        .menu-space {
            padding-right: 25px;
        }

        table {
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }

        img {
            -ms-interpolation-mode: bicubic;
            border: none;
        }

        a, a:hover {
            text-decoration: none;
            color: #FFF;
        }


        @media only screen and (max-width: 640px) {
            body {
                width: auto !important;
            }

            body[yahoo] table[class=main] {
                width: 440px !important;
            }

            body[yahoo] table[class=two-left] {
                width: 420px !important;
                margin: 0px auto;
            }

            body[yahoo] table[class=full] {
                width: 100% !important;
                margin: 0px auto;
            }

            body[yahoo] table[class=alaine] {
                text-align: center;
            }

            body[yahoo] table[class=menu-space] {
                padding-right: 0px;
            }

            body[yahoo] table[class=banner] {
                width: 438px !important;
            }

            body[yahoo] table[class=menu] {
                width: 438px !important;
                margin: 0px auto;
                border-bottom: #e1e0e2 solid 1px;
            }

            body[yahoo] table[class=date] {
                width: 438px !important;
                margin: 0px auto;
                text-align: center;
            }

            body[yahoo] table[class=two-left-inner] {
                width: 400px !important;
                margin: 0px auto;
            }

            body[yahoo] table[class=menu-icon] {
                display: block;
            }

            body[yahoo] table[class=two-left-menu] {
                text-align: center;
            }


        }

        @media only screen and (max-width: 479px) {
            body {
                width: auto !important;
            }

            body[yahoo] table[class=main] {
                width: 310px !important;
            }

            body[yahoo] table[class=two-left] {
                width: 300px !important;
                margin: 0px auto;
            }

            body[yahoo] table[class=full] {
                width: 100% !important;
                margin: 0px auto;
            }

            body[yahoo] table[class=alaine] {
                text-align: center;
            }

            body[yahoo] table[class=menu-space] {
                padding-right: 0px;
            }

            body[yahoo] table[class=banner] {
                width: 308px !important;
            }

            body[yahoo] table[class=menu] {
                width: 308px !important;
                margin: 0px auto;
                border-bottom: #e1e0e2 solid 1px;
            }

            body[yahoo] table[class=date] {
                width: 308px !important;
                margin: 0px auto;
                text-align: center;
            }

            body[yahoo] table[class=two-left-inner] {
                width: 280px !important;
                margin: 0px auto;
            }

            body[yahoo] table[class=menu-icon] {
                display: none;
            }

            body[yahoo] table[class=two-left-menu] {
                width: 310px !important;
                margin: 0px auto;
            }


        }


    </style>

</head>

<body yahoo="fix" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">


<!--Main Table Start-->

<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#fff"
       style="background:#fff;">
    <tr>
        <td align="center" valign="top">


            <!--Logo part Start-->

            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="center" valign="top">
                        <table width="700" border="0" align="center" cellpadding="0" cellspacing="0" class="main">
                            <tr>
                                <td align="left" valign="top" bgcolor="#00B84A" style="background:#00B84A;">
                                    <table width="630" border="0" align="center" cellpadding="0" cellspacing="0"
                                           class="two-left">
                                        <tr>
                                            <td height="35" align="left" valign="top" style="line-height:35px;">&nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top">


                                                <table width="135" border="0" align="left" cellpadding="0"
                                                       cellspacing="0" class="two-left">
                                                    <tr>
                                                        <td align="center" valign="top">
                                                            <table width="135" border="0" cellspacing="0"
                                                                   cellpadding="0">
                                                                <tr>
                                                                    <td width="27" align="left" valign="top">
                                                                        <table width="100%" border="0" align="center"
                                                                               cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td align="center" valign="top"><a
                                                                                            href="#"><img
                                                                                                editable="true"
                                                                                                mc:edit="sm1"
                                                                                                src="{{URL::asset('/img/facebook.png')}}"
                                                                                                width="20" height="20"
                                                                                                alt=""/></a></td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td width="27" align="left" valign="top">
                                                                        <table width="100%" border="0" align="center"
                                                                               cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td align="center" valign="top"><a
                                                                                            href="#"><img
                                                                                                editable="true"
                                                                                                mc:edit="sm2"
                                                                                                src="{{URL::asset('/img/twitter.png')}}"
                                                                                                width="20" height="20"
                                                                                                alt=""/></a></td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td width="27" align="left" valign="top">
                                                                        <table width="100%" border="0" align="center"
                                                                               cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td align="center" valign="top"><a
                                                                                            href="#"><img
                                                                                                editable="true"
                                                                                                mc:edit="sm3"
                                                                                                src="images/google.png"
                                                                                                width="20" height="20"
                                                                                                alt=""/></a></td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td width="27" align="left" valign="top">
                                                                        <table width="100%" border="0" align="center"
                                                                               cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td align="center" valign="top"><a
                                                                                            href="#"><img
                                                                                                editable="true"
                                                                                                mc:edit="sm4"
                                                                                                src="images/rss.png"
                                                                                                width="20" height="20"
                                                                                                alt=""/></a></td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td width="27" align="left" valign="top">
                                                                        <table width="100%" border="0" align="center"
                                                                               cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td align="center" valign="top"><a
                                                                                            href="#"><img
                                                                                                editable="true"
                                                                                                mc:edit="sm5"
                                                                                                src="images/mail.png"
                                                                                                width="20" height="20"
                                                                                                alt=""/></a></td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="35" align="left" valign="top"
                                                            style="line-height:35px;">&nbsp;
                                                        </td>
                                                    </tr>
                                                </table>

                                                <table width="365" border="0" align="right" cellpadding="0"
                                                       cellspacing="0" class="two-left">
                                                    <tr>
                                                        <td align="left" valign="top">


                                                            <table width="105" border="0" align="left" cellpadding="0"
                                                                   cellspacing="0" class="two-left">
                                                                <tr>
                                                                    <td align="center" valign="top"><a href="#"><img
                                                                                    editable="true" mc:edit="sm6"
                                                                                    src="{{ $message->embed(public_path() . '/imgs/logo.png') }}"
                                                                                    height="100" alt=""/></a></td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="20" align="left" valign="top"
                                                                        style="line-height:20px;">&nbsp;
                                                                    </td>
                                                                </tr>
                                                            </table>

                                                            <table border="0" align="right" cellpadding="0"
                                                                   cellspacing="0" class="two-left">
                                                                <tr>
                                                                    <td align="center" valign="top"
                                                                        style="font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; text-transform:uppercase; color:#FFF; padding-top:4px;"
                                                                        mc:edit="sm7">
                                                                        <multiline><a href="#"
                                                                                      style="text-decoration:none; color:#FFF;">Inicio</a>
                                                                            &nbsp;&nbsp;{{-- <a href="#"
                                                                                            style="text-decoration:none; color:#FFF;">About</a>
                                                                            &nbsp;&nbsp; <a href="#"
                                                                                            style="text-decoration:none; color:#FFF;">Support</a>--}}
                                                                        </multiline>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="35" align="left" valign="top"
                                                                        style="line-height:35px;">&nbsp;
                                                                    </td>
                                                                </tr>
                                                            </table>


                                                        </td>
                                                    </tr>
                                                </table>


                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

            <!--Logo part End-->


            <!--Welcome part Start-->

            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="center" valign="top">
                        <table width="700" border="0" align="center" cellpadding="0" cellspacing="0" class="main">
                            <tr>
                                <td align="left" valign="top" bgcolor="#00B84A" style="background:#00B84A;">
                                    <table width="530" border="0" align="center" cellpadding="0" cellspacing="0"
                                           class="two-left">
                                        <tr>
                                            <td height="95" align="left" valign="top" style="line-height:95px;">&nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" valign="top">
                                                <table width="100%" border="0" align="center" cellpadding="0"
                                                       cellspacing="0">
                                                    <tr>
                                                        <td align="center" valign="top">
                                                            <table width="100%" border="0" align="center"
                                                                   cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td align="center" valign="top"
                                                                        style="font-family:Arial, Helvetica, sans-serif; font-size:18px; font-weight:bold; text-transform:uppercase; color:#FFF;"
                                                                        mc:edit="sm11">
                                                                        <multiline>
                                                                            {{$title}}
                                                                        </multiline>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" valign="top"
                                                            style="font-family:Verdana, Geneva, sans-serif; font-size:16px; font-weight:normal;color:#FFF; line-height:36px; padding-bottom:15px;"
                                                            mc:edit="sm13">
                                                            <multiline>
                                                               {{$body}}
                                                            </multiline>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" valign="top">
                                                            <table width="185" border="0" align="center" cellpadding="0"
                                                                   cellspacing="0">
                                                                <tr>
                                                                    <td align="center" valign="top">&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="45" align="center" valign="middle"
                                                                        style=" border:#ffffff solid 2px; font-family:Arial, Helvetica, sans-serif; font-size:18px; font-weight:bold; text-transform:uppercase; color:#FFF;"
                                                                        mc:edit="sm14">
                                                                        <multiline><a href="#"
                                                                                      style="text-decoration:none; color:#FFF;">Download</a>
                                                                        </multiline>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="95" align="left" valign="top" style="line-height:95px;">&nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

            <!--Welcome part End-->


            <!--2-add Start-->
            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="center" valign="top">
                        <table width="700" border="0" align="center" cellpadding="0" cellspacing="0" class="main">
                            <tr>
                                <td align="left" valign="top" bgcolor="#FFFFFF" style="background:#FFF;">
                                    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td width="50%" align="left" valign="top">
                                                <table width="100%" border="0" align="center" cellpadding="0"
                                                       cellspacing="0">
                                                    <tr>
                                                        <td align="center" valign="top"><img editable="true"
                                                                                             mc:edit="sm81"
                                                                                             src="{{ $message->embed(public_path() . '/imgs/mas-cosas.jpg')}}"
                                                                                             height="150"
                                                                                             alt=""
                                                                                             style="display:block;width:100% !important; height:auto !important; "/>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td width="50%" align="left" valign="top">
                                                <table width="100%" border="0" align="center" cellpadding="0"
                                                       cellspacing="0">
                                                    <tr>
                                                        <td align="center" valign="top"><img editable="true"
                                                                                             mc:edit="sm82"
                                                                                             src="{{ $message->embed(public_path() . '/imgs/Peluqueria.jpg')}}"
                                                                                             height="150"
                                                                                             alt=""
                                                                                             style="display:block;width:100% !important; height:auto !important; "/>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

            <!--2-add End-->


            <!--contact-Text Start-->

            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="center" valign="top">
                        <table width="700" border="0" align="center" cellpadding="0" cellspacing="0" class="main">
                            <tr>
                                <td align="left" valign="top" bgcolor="#00B84A" style="background:#00B84A;">
                                    <table width="580" border="0" align="center" cellpadding="0" cellspacing="0"
                                           class="two-left">
                                        <tr>
                                            <td height="10" align="left" valign="top" style="line-height:10px;">&nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top">

                                                <table width="240" border="0" align="left" cellpadding="0"
                                                       cellspacing="0" class="two-left">
                                                    <tr>
                                                        <td align="center" valign="top"
                                                            style="font-family:Arial, Helvetica, sans-serif; font-size:16px; font-weight:bold; text-transform:uppercase; color:#FFF;"
                                                            mc:edit="sm110">
                                                            <multiline>Contacto</multiline>
                                                        </td>
                                                    </tr>


                                                </table>


                                                <table width="280" border="0" align="right" cellpadding="0"
                                                       cellspacing="0" class="two-left">
                                                    <tr>
                                                        <td align="left" valign="top">
                                                            <table width="280" border="0" align="left" cellpadding="0"
                                                                   cellspacing="0" class="two-left">
                                                                <tr>
                                                                    <td align="center" valign="top"
                                                                        style="font-family:Arial, Helvetica, sans-serif; font-size:16px; font-weight:bold; text-transform:uppercase; color:#FFF;"
                                                                        mc:edit="sm113">
                                                                        <multiline>Acerca</multiline>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>


                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="40" align="left" valign="top" style="line-height:40px;">&nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

            <!--contact-Text End-->


            <!--copyright Start-->

            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="center" valign="top">
                        <table width="700" border="0" align="center" cellpadding="0" cellspacing="0" class="main">
                            <tr>
                                <td align="left" valign="top" bgcolor="#2e7f29" style="background:#2e7f29;">
                                    <table width="580" border="0" align="center" cellpadding="0" cellspacing="0"
                                           class="two-left">
                                        <tr>
                                            <td height="60" align="left" valign="middle"
                                                style="font-family:Verdana, Geneva, sans-serif; font-size:12px; font-weight:normal;color:#FFF;"
                                                mc:edit="sm116">
                                                <multiline>Copyright &copy; 2019 BVEG.
                                                </multiline>
                                                <unsubscribe>Unsubscribe</unsubscribe>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

            <!--copyright End-->


        </td>
    </tr>
</table>

<!--Main Table End-->

</body>
