<h1>Hola, has recibido un bono regalo</h1>
<br/>
<img src="{{ $message->embed($pathToImage) }}">
<p>Para usar en el restaurante {{$placeName}}</p>
<p>Pur un valor {{$value}} euros</p>
<a href="https://api.bveg.es/bonus/{{$code}}">Pulsa aqui para agregar este bono a tu cuenta</a>