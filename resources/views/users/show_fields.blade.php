<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $user->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $user->name !!}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{!! $user->status !!}</p>
</div>

<!-- Photo Field -->
<div class="form-group">
    {!! Form::label('photo', 'Photo:') !!}
    <p>{!! $user->photo !!}</p>
</div>

<!-- Correo Field -->
<div class="form-group">
    {!! Form::label('correo', 'Correo:') !!}
    <p>{!! $user->correo !!}</p>
</div>

<!-- Dispositive Field -->
<div class="form-group">
    {!! Form::label('dispositive', 'Dispositive:') !!}
    <p>{!! $user->dispositive !!}</p>
</div>

<!-- Type So Field -->
<div class="form-group">
    {!! Form::label('type_so', 'Type So:') !!}
    <p>{!! $user->type_so !!}</p>
</div>

<!-- Password Field -->
<div class="form-group">
    {!! Form::label('password', 'Password:') !!}
    <p>{!! $user->password !!}</p>
</div>

<!-- Last Login Field -->
<div class="form-group">
    {!! Form::label('last_login', 'Last Login:') !!}
    <p>{!! $user->last_login !!}</p>
</div>

<!-- Photo 1 Id Field -->
<div class="form-group">
    {!! Form::label('photo_1_id', 'Photo 1 Id:') !!}
    <p>{!! $user->photo_1_id !!}</p>
</div>

<!-- Date Reg Field -->
<div class="form-group">
    {!! Form::label('date_reg', 'Date Reg:') !!}
    <p>{!! $user->date_reg !!}</p>
</div>

<!-- Points Field -->
<div class="form-group">
    {!! Form::label('points', 'Points:') !!}
    <p>{!! $user->points !!}</p>
</div>

<!-- Cms Password Field -->
<div class="form-group">
    {!! Form::label('cms_password', 'Cms Password:') !!}
    <p>{!! $user->cms_password !!}</p>
</div>

<!-- Role Field -->
<div class="form-group">
    {!! Form::label('role', 'Role:') !!}
    <p>{!! $user->role !!}</p>
</div>

<!-- Country Code Field -->
<div class="form-group">
    {!! Form::label('country_code', 'Country Code:') !!}
    <p>{!! $user->country_code !!}</p>
</div>

<!-- Phone Field -->
<div class="form-group">
    {!! Form::label('phone', 'Phone:') !!}
    <p>{!! $user->phone !!}</p>
</div>

