
@extends('layouts.app')
@section('content')
    <div class="container-fluid my-5 py-5">
        <!--#Notas y titulares-->
        <div class="col-12 pb-2 mt-3">
            <h2 class="text-center titulo-dash-int">Editar administrador</h2>
            <hr width="10%" align="center">

        </div>
        <!--#Notas y titulares-->
        <div class="info">
            <div class="info-content">
                {!! Form::model($user, ['route' => ['users.update', $user->id], 'method' => 'patch']) !!}
                <div class="row">
                    @include('users.fields')
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection



