<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewBonus extends Mailable {
	use Queueable, SerializesModels;

	/**
	 * Create a new message instance.
	 *
	 * @return void
	 */
	private $imageData;

	private $placeName;

	private $value;

	private $code;

	/**
	 * NewBonus constructor.
	 *
	 * @param $imageData
	 * @param $placeName
	 * @param $value
	 */
	public function __construct( $imageData, $placeName, $value, $code ) {
		$this->imageData = $imageData;
		$this->placeName = $placeName;
		$this->value     = $value;
		$this->code = $code;
	}


	/**
	 * Build the message.
	 *
	 * @return $this
	 */
	public function build() {
		return $this->view( 'mail.bonus' )
		            ->with( 'pathToImage', storage_path( 'app/public' . $this->imageData ) )
		            ->with( 'placeName', $this->placeName )
		            ->with( 'value', $this->value )
		            ->with('code',$this->code);

	}
}
