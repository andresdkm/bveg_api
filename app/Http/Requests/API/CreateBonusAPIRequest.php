<?php

namespace App\Http\Requests\API;

use App\Util\OwnerUtil;
use InfyOm\Generator\Request\APIRequest;

class CreateBonusAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return OwnerUtil::getAuthorize($this);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'client_id' => 'required',
            'place_id' => 'required',
            'author_id' => 'required',
            'type' => 'required'
        ];
    }
}
