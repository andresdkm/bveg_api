<?php

namespace App\Http\Requests\API;

use App\Models\Place;
use InfyOm\Generator\Request\APIRequest;

class CreatePlaceAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        	'name' => 'required',
	        'phone' => 'required',
	        'website' => 'required',
	        'address' => 'required',
	        'latitude' => 'required',
	        'longitude' => 'required',
	        'category_id' => 'required',
	        'description' => 'required',
	        'lang' => 'required'
        ];
    }
}
