<?php

namespace App\Http\Requests\API;

use App\Models\ClientPoint;
use App\Util\OwnerUtil;
use InfyOm\Generator\Request\APIRequest;

class CreateClientPointAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return OwnerUtil::getAuthorize($this);

    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
	        'place_id' => 'required',
	        'author_id' => 'required',
            'description' => 'required',
            'points'
        ];
    }
}
