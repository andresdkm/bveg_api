<?php

namespace App\Http\Requests\API;

use InfyOm\Generator\Request\APIRequest;

class CreateClientAPIRequest extends APIRequest {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return [
			'country_id' => 'required',
			'email'      => 'unique:clients,email|required',
			'phone'      => 'unique:clients,phone|required'
		];
	}

	/**
	 * Get the error messages for the defined validation rules.
	 *
	 * @return array
	 */
	public function messages() {
		return [
			'email.unique'   => __( 'validation.email.unique' ),
			'phone.unique'   => __( 'validation.phone.unique' ),
			'email.required' => __( 'validation.email.required' ),
			'phone.required' => __( 'validation.phone.required' ),

		];
	}
}
