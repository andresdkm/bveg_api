<?php

namespace App\Http\Requests;

use App\Models\Client;
use Illuminate\Foundation\Http\FormRequest;

class UpdatePlaceConfigurationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $authorId = $this->get('client_id');
        $placeId = $this->get('place_id');
        $client = Client::find($authorId);
        if (empty($client)) {
            return false;
        }
        if (!isset($client->places_admin)) {
            return false;
        }
        return in_array($placeId, $client->places_admin);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
