<?php

namespace App\Http\Middleware;

use App\Util\TranslateUtil;
use Closure;
use Illuminate\Support\Facades\App;

class TranslateMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->has('lang')) {
            $lang = $request->get('lang');
            if (in_array($lang, TranslateUtil::$languagesAvailable)) {
                session()->put('language', $lang);
	            App::setLocale($lang);
            }
        }
        return $next($request);
    }
}
