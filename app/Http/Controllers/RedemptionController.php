<?php

namespace App\Http\Controllers;

use App\DataTables\RedemptionDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateRedemptionRequest;
use App\Http\Requests\UpdateRedemptionRequest;
use App\Repositories\RedemptionRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class RedemptionController extends AppBaseController
{
    /** @var  RedemptionRepository */
    private $redemptionRepository;

    public function __construct(RedemptionRepository $redemptionRepo)
    {
        $this->redemptionRepository = $redemptionRepo;
    }

    /**
     * Display a listing of the Redemption.
     *
     * @param RedemptionDataTable $redemptionDataTable
     * @return Response
     */
    public function index(RedemptionDataTable $redemptionDataTable)
    {
        return $redemptionDataTable->render('redemptions.index');
    }

    /**
     * Show the form for creating a new Redemption.
     *
     * @return Response
     */
    public function create()
    {
        return view('redemptions.create');
    }

    /**
     * Store a newly created Redemption in storage.
     *
     * @param CreateRedemptionRequest $request
     *
     * @return Response
     */
    public function store(CreateRedemptionRequest $request)
    {
        $input = $request->all();

        $redemption = $this->redemptionRepository->create($input);

        Flash::success('Redemption saved successfully.');

        return redirect(route('redemptions.index'));
    }

    /**
     * Display the specified Redemption.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $redemption = $this->redemptionRepository->findWithoutFail($id);

        if (empty($redemption)) {
            Flash::error('Redemption not found');

            return redirect(route('redemptions.index'));
        }

        return view('redemptions.show')->with('redemption', $redemption);
    }

    /**
     * Show the form for editing the specified Redemption.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $redemption = $this->redemptionRepository->findWithoutFail($id);

        if (empty($redemption)) {
            Flash::error('Redemption not found');

            return redirect(route('redemptions.index'));
        }

        return view('redemptions.edit')->with('redemption', $redemption);
    }

    /**
     * Update the specified Redemption in storage.
     *
     * @param  int              $id
     * @param UpdateRedemptionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRedemptionRequest $request)
    {
        $redemption = $this->redemptionRepository->findWithoutFail($id);

        if (empty($redemption)) {
            Flash::error('Redemption not found');

            return redirect(route('redemptions.index'));
        }

        $redemption = $this->redemptionRepository->update($request->all(), $id);

        Flash::success('Redemption updated successfully.');

        return redirect(route('redemptions.index'));
    }

    /**
     * Remove the specified Redemption from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $redemption = $this->redemptionRepository->findWithoutFail($id);

        if (empty($redemption)) {
            Flash::error('Redemption not found');

            return redirect(route('redemptions.index'));
        }

        $this->redemptionRepository->delete($id);

        Flash::success('Redemption deleted successfully.');

        return redirect(route('redemptions.index'));
    }
}
