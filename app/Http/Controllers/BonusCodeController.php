<?php

namespace App\Http\Controllers;

use App\Models\Bonus;
use App\Util\BonusUtil;
use Carbon\Carbon;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class BonusCodeController extends Controller
{

    public function show($bonusId)
    {

        $bonus = Bonus::find($bonusId);

        if (empty($bonus)) {
            return "";
        }
        if ($bonus->status == BonusUtil::$state_available) {
            $image = QrCode::format('png')
                ->merge(storage_path('images/icon.png'), 0.2, true)
                ->size(300)->errorCorrection('H')
                ->backgroundColor(0, 184, 74)
                ->generate($bonus->public_code);
        } else {
            $image = QrCode::format('png')
                ->size(300)->errorCorrection('H')
                ->backgroundColor(147, 58, 22)
                ->generate($bonus->public_code.$bonus->_id);
        }
        return response($image)->header('Content-type', 'image/png');

    }

    public function view($code)
    {
        return view('bonus.show');
    }
}
