<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use App\Http\Requests\API\UpdateCommentAPIRequest;
use App\Http\Requests\CreateDeviceRequest;
use App\Models\Comment;
use App\Repositories\ClientRepository;
use Illuminate\Http\Request;

class DeviceAPIController extends AppBaseController
{

	/** @var  ClientRepository */
	private $clientRepository;

	public function __construct(ClientRepository $clientRepo)
	{
		$this->clientRepository = $clientRepo;
	}


	public function store( CreateDeviceRequest $request)
	{
		$input = $request->all();

		/** @var Comment $comment */
		$client = $this->clientRepository->findWithoutFail($input['client_id']);

		if (empty($client)) {
			return $this->sendError('Client not found');
		}

		$client = $this->clientRepository->update([
			'token' => $input['token'],
			'os' => $input['os']
		], $input['client_id']);

		return $this->sendResponse($client->toArray(), 'Client updated successfully');
	}
}
