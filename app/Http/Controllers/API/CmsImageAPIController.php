<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCmsImageAPIRequest;
use App\Http\Requests\API\UpdateCmsImageAPIRequest;
use App\Models\CmsImage;
use App\Repositories\CmsImageRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class CmsImageController
 * @package App\Http\Controllers\API
 */

class CmsImageAPIController extends AppBaseController
{
    /** @var  CmsImageRepository */
    private $cmsImageRepository;

    public function __construct(CmsImageRepository $cmsImageRepo)
    {
        $this->cmsImageRepository = $cmsImageRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/cmsImages",
     *      summary="Get a listing of the CmsImages.",
     *      tags={"CmsImage"},
     *      description="Get all CmsImages",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/CmsImage")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->cmsImageRepository->pushCriteria(new RequestCriteria($request));
        $this->cmsImageRepository->pushCriteria(new LimitOffsetCriteria($request));
        $cmsImages = $this->cmsImageRepository->all();

        return $this->sendResponse($cmsImages->toArray(), 'Cms Images retrieved successfully');
    }

    /**
     * @param CreateCmsImageAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/cmsImages",
     *      summary="Store a newly created CmsImage in storage",
     *      tags={"CmsImage"},
     *      description="Store CmsImage",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="CmsImage that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/CmsImage")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CmsImage"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateCmsImageAPIRequest $request)
    {
        $input = $request->all();

        $cmsImages = $this->cmsImageRepository->create($input);

        return $this->sendResponse($cmsImages->toArray(), 'Cms Image saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/cmsImages/{id}",
     *      summary="Display the specified CmsImage",
     *      tags={"CmsImage"},
     *      description="Get CmsImage",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CmsImage",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CmsImage"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var CmsImage $cmsImage */
        $cmsImage = $this->cmsImageRepository->findWithoutFail($id);

        if (empty($cmsImage)) {
            return $this->sendError('Cms Image not found');
        }

        return $this->sendResponse($cmsImage->toArray(), 'Cms Image retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateCmsImageAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/cmsImages/{id}",
     *      summary="Update the specified CmsImage in storage",
     *      tags={"CmsImage"},
     *      description="Update CmsImage",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CmsImage",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="CmsImage that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/CmsImage")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CmsImage"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateCmsImageAPIRequest $request)
    {
        $input = $request->all();

        /** @var CmsImage $cmsImage */
        $cmsImage = $this->cmsImageRepository->findWithoutFail($id);

        if (empty($cmsImage)) {
            return $this->sendError('Cms Image not found');
        }

        $cmsImage = $this->cmsImageRepository->update($input, $id);

        return $this->sendResponse($cmsImage->toArray(), 'CmsImage updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/cmsImages/{id}",
     *      summary="Remove the specified CmsImage from storage",
     *      tags={"CmsImage"},
     *      description="Delete CmsImage",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CmsImage",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var CmsImage $cmsImage */
        $cmsImage = $this->cmsImageRepository->findWithoutFail($id);

        if (empty($cmsImage)) {
            return $this->sendError('Cms Image not found');
        }

        $cmsImage->delete();

        return $this->sendResponse($id, 'Cms Image deleted successfully');
    }
}
