<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use App\Infrastructure\Repositories\Criterias\OrderByCriteria;
use App\Infrastructure\Repositories\Criterias\WhereFieldCriteria;
use App\Models\ClientPoint;
use App\Repositories\UserPointRepository;
use Illuminate\Http\Request;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class UserPointController
 * @package App\Http\Controllers\API
 */
class PointAPIController extends AppBaseController {
	/** @var  UserPointRepository */
	private $userPointRepository;

	public function __construct( UserPointRepository $userPointRepo ) {
		$this->userPointRepository = $userPointRepo;
	}

	/**
	 * @param $clientId
	 * @param Request $request
	 *
	 * @return Response
	 *
	 * @SWG\Get(
	 *      path="/userPoints",
	 *      summary="Get a listing of the UserPoints.",
	 *      tags={"ClientPoint"},
	 *      description="Get all UserPoints",
	 *      produces={"application/json"},
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="array",
	 *                  @SWG\Items(ref="#/definitions/ClientPoint")
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
	public function index( $clientId, Request $request ) {

		$places = ClientPoint::with( 'placeLite' )->where( 'client_id', $clientId )->groupBy( 'place_id' )->get( [ 'place_id', ] );

		$data = array();
		foreach ( $places as $item ) {
			$data[] = [
				'points' => ClientPoint::where( 'client_id', $clientId )->where( 'place_id', $item->place_id )->sum( 'points' ),
				'place'  => $item->placeLite
			];
		}

		return $this->sendResponse( $data, 'User Points retrieved successfully' );
	}


	public function show( $clientId, $placeId, Request $request ) {

		$this->userPointRepository->pushCriteria( new RequestCriteria( $request ) );
		$this->userPointRepository->pushCriteria( new LimitOffsetCriteria( $request ) );
		$this->userPointRepository->pushCriteria( new WhereFieldCriteria( 'place_id', $placeId ) );
		$this->userPointRepository->pushCriteria( new WhereFieldCriteria( 'client_id', $clientId ) );
		$this->userPointRepository->pushCriteria( new OrderByCriteria( 'created_at' ) );
		$points = $this->userPointRepository->all();

		return $this->sendResponse( $points, 'User Points retrieved successfully' );
	}


}
