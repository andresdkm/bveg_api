<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use App\Models\Place;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Response;

class ResourceAPIController extends AppBaseController {

	public function store( Request $request ) {

		if ( $request->hasFile( 'file' ) ) {
			$file  = $request->file( 'file' );
			$name  = $file->getClientOriginalName();
			$parts = pathinfo( $name );
			$now   = Carbon::now();
			$name  = $now->timestamp . "." . $parts['extension'];
			Storage::put( $name, File::get( $file ) );

			if ( $request->has( 'place_id' ) ) {
				$this->uploadPlace( $request->get( 'place_id' ), $name );
			}

			return $this->sendResponse( $name, "resource store" );
		}

		return $this->sendError( "File not input", 200 );
	}

	private function uploadPlace( $placeId, $fileName ) {
		$place = Place::find( $placeId );
		if ( ! empty( $place ) ) {
			$place->push( 'photos', $fileName );
			if ( $place->image == null ) {
				$place->image = $fileName;
			}
			$place->save();
		}
	}

}