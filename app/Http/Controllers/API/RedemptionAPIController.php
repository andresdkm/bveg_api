<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use App\Http\Requests\API\CreateRedemptionAPIRequest;
use App\Http\Requests\API\UpdateRedemptionAPIRequest;
use App\Models\Redemption;
use App\Repositories\RedemptionRepository;
use Illuminate\Http\Request;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class RedemptionController
 * @package App\Http\Controllers\API
 */
class RedemptionAPIController extends AppBaseController {
	/** @var  RedemptionRepository */
	private $redemptionRepository;

	public function __construct( RedemptionRepository $redemptionRepo ) {
		$this->redemptionRepository = $redemptionRepo;
	}

	/**
	 * @param Request $request
	 *
	 * @return Response
	 *
	 * @SWG\Get(
	 *      path="/redemptions",
	 *      summary="Get a listing of the Redemptions.",
	 *      tags={"Redemption"},
	 *      description="Get all Redemptions",
	 *      produces={"application/json"},
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="array",
	 *                  @SWG\Items(ref="#/definitions/Redemption")
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
	public function index( Request $request ) {
		$this->redemptionRepository->pushCriteria( new RequestCriteria( $request ) );
		$this->redemptionRepository->pushCriteria( new LimitOffsetCriteria( $request ) );
		$redemptions = $this->redemptionRepository->all();

		return $this->sendResponse( $redemptions->toArray(), 'Redemptions retrieved successfully' );
	}

	/**
	 * @param CreateRedemptionAPIRequest $request
	 *
	 * @return Response
	 *
	 * @SWG\Post(
	 *      path="/redemptions",
	 *      summary="Store a newly created Redemption in storage",
	 *      tags={"Redemption"},
	 *      description="Store Redemption",
	 *      produces={"application/json"},
	 *      @SWG\Parameter(
	 *          name="body",
	 *          in="body",
	 *          description="Redemption that should be stored",
	 *          required=false,
	 *          @SWG\Schema(ref="#/definitions/Redemption")
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="data",
	 *                  ref="#/definitions/Redemption"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
	public function store( CreateRedemptionAPIRequest $request ) {
		$input = $request->all();

		try {
			$redemption = $this->redemptionRepository->create( $input );

		} catch ( \Exception $exception ) {
			return $this->sendError( $exception->getMessage() );
		}

		return $this->sendResponse( $redemption, 'Redemption saved successfully' );
	}

	/**
	 * @param int $id
	 *
	 * @return Response
	 *
	 * @SWG\Get(
	 *      path="/redemptions/{id}",
	 *      summary="Display the specified Redemption",
	 *      tags={"Redemption"},
	 *      description="Get Redemption",
	 *      produces={"application/json"},
	 *      @SWG\Parameter(
	 *          name="id",
	 *          description="id of Redemption",
	 *          type="integer",
	 *          required=true,
	 *          in="path"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="data",
	 *                  ref="#/definitions/Redemption"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
	public function show( $id ) {
		/** @var Redemption $redemption */
		$redemption = $this->redemptionRepository->findWithoutFail( $id );

		if ( empty( $redemption ) ) {
			return $this->sendError( 'Redemption not found' );
		}

		return $this->sendResponse( $redemption->toArray(), 'Redemption retrieved successfully' );
	}

	/**
	 * @param int $id
	 * @param UpdateRedemptionAPIRequest $request
	 *
	 * @return Response
	 *
	 * @SWG\Put(
	 *      path="/redemptions/{id}",
	 *      summary="Update the specified Redemption in storage",
	 *      tags={"Redemption"},
	 *      description="Update Redemption",
	 *      produces={"application/json"},
	 *      @SWG\Parameter(
	 *          name="id",
	 *          description="id of Redemption",
	 *          type="integer",
	 *          required=true,
	 *          in="path"
	 *      ),
	 *      @SWG\Parameter(
	 *          name="body",
	 *          in="body",
	 *          description="Redemption that should be updated",
	 *          required=false,
	 *          @SWG\Schema(ref="#/definitions/Redemption")
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="data",
	 *                  ref="#/definitions/Redemption"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
	public function update( $id, UpdateRedemptionAPIRequest $request ) {
		$input = $request->all();

		/** @var Redemption $redemption */
		$redemption = $this->redemptionRepository->findWithoutFail( $id );

		if ( empty( $redemption ) ) {
			return $this->sendError( 'Redemption not found' );
		}

		$redemption = $this->redemptionRepository->update( $input, $id );

		return $this->sendResponse( $redemption->toArray(), 'Redemption updated successfully' );
	}

	/**
	 * @param int $id
	 *
	 * @return Response
	 *
	 * @SWG\Delete(
	 *      path="/redemptions/{id}",
	 *      summary="Remove the specified Redemption from storage",
	 *      tags={"Redemption"},
	 *      description="Delete Redemption",
	 *      produces={"application/json"},
	 *      @SWG\Parameter(
	 *          name="id",
	 *          description="id of Redemption",
	 *          type="integer",
	 *          required=true,
	 *          in="path"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
	public function destroy( $id ) {
		/** @var Redemption $redemption */
		$redemption = $this->redemptionRepository->findWithoutFail( $id );

		if ( empty( $redemption ) ) {
			return $this->sendError( 'Redemption not found' );
		}

		$redemption->delete();

		return $this->sendResponse( $id, 'Redemption deleted successfully' );
	}
}
