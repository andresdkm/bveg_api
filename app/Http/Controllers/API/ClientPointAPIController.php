<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use App\Http\Requests\API\CreateClientPointAPIRequest;
use App\Infrastructure\Repositories\Criterias\WhereFieldCriteria;
use App\Models\Client;
use App\Repositories\UserPointRepository;
use Illuminate\Http\Request;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class UserPointController
 * @package App\Http\Controllers\API
 */
class ClientPointAPIController extends AppBaseController {
	/** @var  UserPointRepository */
	private $userPointRepository;

	public function __construct( UserPointRepository $userPointRepo ) {
		$this->userPointRepository = $userPointRepo;
	}

	/**
	 * @param $userId
	 * @param Request $request
	 *
	 * @return Response
	 *
	 * @throws \Prettus\Repository\Exceptions\RepositoryException
	 * @SWG\Get(
	 *      path="/userPoints",
	 *      summary="Get a listing of the UserPoints.",
	 *      tags={"ClientPoint"},
	 *      description="Get all UserPoints",
	 *      produces={"application/json"},
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="array",
	 *                  @SWG\Items(ref="#/definitions/ClientPoint")
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
	public function index( $userId, Request $request ) {
		$this->userPointRepository->pushCriteria( new WhereFieldCriteria( 'user_id', $userId ) );
		$this->userPointRepository->pushCriteria( new RequestCriteria( $request ) );
		$this->userPointRepository->pushCriteria( new LimitOffsetCriteria( $request ) );
		$userPoints = $this->userPointRepository->all();

		return $this->sendResponse( $userPoints->toArray(), 'User Points retrieved successfully' );
	}

	/**
	 * @param CreateClientPointAPIRequest $request
	 *
	 * @return Response
	 *
	 * @throws \Prettus\Validator\Exceptions\ValidatorException
	 * @SWG\Post(
	 *      path="/userPoints",
	 *      summary="Store a newly created ClientPoint in storage",
	 *      tags={"ClientPoint"},
	 *      description="Store ClientPoint",
	 *      produces={"application/json"},
	 *      @SWG\Parameter(
	 *          name="body",
	 *          in="body",
	 *          description="ClientPoint that should be stored",
	 *          required=false,
	 *          @SWG\Schema(ref="#/definitions/ClientPoint")
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="data",
	 *                  ref="#/definitions/ClientPoint"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
	public function store( CreateClientPointAPIRequest $request ) {
		try {
			$input = $request->all();

			$userPoints = $this->userPointRepository->create( $input );

			$client = Client::find( $userPoints->client_id );

			return $this->sendResponse( __( 'app.points_saved', [
				'points' => $userPoints->points,
				'email'  => $client->email
			] ), 'User Point saved successfully' );

		} catch ( \Exception $exception ) {
			return $this->sendError( $exception->getMessage() );
		}
	}


}
