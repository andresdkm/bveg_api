<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use App\Http\Requests\API\CreatePromotionAPIRequest;
use App\Http\Requests\API\UpdatePromotionAPIRequest;
use App\Infrastructure\Repositories\Criterias\WithRelationshipsCriteria;
use App\Models\Promotion;
use App\Repositories\PromotionRepository;
use App\Util\GeoUtil;
use Illuminate\Http\Request;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class PromotionController
 * @package App\Http\Controllers\API
 */
class PromotionAPIController extends AppBaseController {
	/** @var  PromotionRepository */
	private $promotionRepository;

	public function __construct( PromotionRepository $promotionRepo ) {
		$this->promotionRepository = $promotionRepo;
	}

	/**
	 * @param $userId
	 * @param Request $request
	 *
	 * @return Response
	 *
	 * @throws \Prettus\Repository\Exceptions\RepositoryException
	 * @SWG\Get(
	 *      path="/promotions",
	 *      summary="Get a listing of the Promotions.",
	 *      tags={"Promotion"},
	 *      description="Get all Promotions",
	 *      produces={"application/json"},
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="array",
	 *                  @SWG\Items(ref="#/definitions/Promotion")
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
	public function index( Request $request ) {
		$this->promotionRepository->pushCriteria( new RequestCriteria( $request ) );
		$this->promotionRepository->pushCriteria( new LimitOffsetCriteria( $request ) );
		$this->promotionRepository->pushCriteria( new WithRelationshipsCriteria( [ 'placeLite' ] ) );
		//$this->promotionRepository->pushCriteria(new WhereFieldCriteria('place_id',$places, 'in'));
		$promotions = $this->promotionRepository->paginate( 20 );

		return $this->sendResponse( $promotions->toArray(), 'Promotions retrieved successfully' );
	}

	/**
	 * @param CreatePromotionAPIRequest $request
	 *
	 * @return Response
	 *
	 * @SWG\Post(
	 *      path="/promotions",
	 *      summary="Store a newly created Promotion in storage",
	 *      tags={"Promotion"},
	 *      description="Store Promotion",
	 *      produces={"application/json"},
	 *      @SWG\Parameter(
	 *          name="body",
	 *          in="body",
	 *          description="Promotion that should be stored",
	 *          required=false,
	 *          @SWG\Schema(ref="#/definitions/Promotion")
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="data",
	 *                  ref="#/definitions/Promotion"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
	public function store( CreatePromotionAPIRequest $request ) {
		$input = $request->all();

		$promotions = $this->promotionRepository->create( $input );

		return $this->sendResponse( $promotions->toArray(), 'Promotion saved successfully' );
	}

	/**
	 * @param int $id
	 *
	 * @param Request $request
	 *
	 * @return Response
	 *
	 * @throws \Prettus\Repository\Exceptions\RepositoryException
	 * @SWG\Get(
	 *      path="/promotions/{id}",
	 *      summary="Display the specified Promotion",
	 *      tags={"Promotion"},
	 *      description="Get Promotion",
	 *      produces={"application/json"},
	 *      @SWG\Parameter(
	 *          name="id",
	 *          description="id of Promotion",
	 *          type="integer",
	 *          required=true,
	 *          in="path"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="data",
	 *                  ref="#/definitions/Promotion"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
	public function show( $id, Request $request ) {
		$this->promotionRepository->pushCriteria( new WithRelationshipsCriteria( [ 'place' ] ) );
		/** @var Promotion $promotion */
		$promotion = $this->promotionRepository->findWithoutFail( $id );

		if ( empty( $promotion ) ) {
			return $this->sendError( 'Promotion not found' );
		}

		$promotion->distance = GeoUtil::distance( $request->get( 'latitude' ), $request->get( 'longitude' ), $promotion->place->geolocation['coordinates'][1], $promotion->place->geolocation['coordinates'][0] );

		return $this->sendResponse( $promotion->toArray(), 'Promotion retrieved successfully' );
	}

	/**
	 * @param int $id
	 * @param UpdatePromotionAPIRequest $request
	 *
	 * @return Response
	 *
	 * @SWG\Put(
	 *      path="/promotions/{id}",
	 *      summary="Update the specified Promotion in storage",
	 *      tags={"Promotion"},
	 *      description="Update Promotion",
	 *      produces={"application/json"},
	 *      @SWG\Parameter(
	 *          name="id",
	 *          description="id of Promotion",
	 *          type="integer",
	 *          required=true,
	 *          in="path"
	 *      ),
	 *      @SWG\Parameter(
	 *          name="body",
	 *          in="body",
	 *          description="Promotion that should be updated",
	 *          required=false,
	 *          @SWG\Schema(ref="#/definitions/Promotion")
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="data",
	 *                  ref="#/definitions/Promotion"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
	public function update( $id, UpdatePromotionAPIRequest $request ) {
		$input = $request->all();

		/** @var Promotion $promotion */
		$promotion = $this->promotionRepository->findWithoutFail( $id );

		if ( empty( $promotion ) ) {
			return $this->sendError( 'Promotion not found' );
		}

		$promotion = $this->promotionRepository->update( $input, $id );

		return $this->sendResponse( $promotion->toArray(), 'Promotion updated successfully' );
	}

	/**
	 * @param int $id
	 *
	 * @return Response
	 *
	 * @SWG\Delete(
	 *      path="/promotions/{id}",
	 *      summary="Remove the specified Promotion from storage",
	 *      tags={"Promotion"},
	 *      description="Delete Promotion",
	 *      produces={"application/json"},
	 *      @SWG\Parameter(
	 *          name="id",
	 *          description="id of Promotion",
	 *          type="integer",
	 *          required=true,
	 *          in="path"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
	public function destroy( $id ) {
		/** @var Promotion $promotion */
		$promotion = $this->promotionRepository->findWithoutFail( $id );

		if ( empty( $promotion ) ) {
			return $this->sendError( 'Promotion not found' );
		}

		$promotion->delete();

		return $this->sendResponse( $id, 'Promotion deleted successfully' );
	}

}
