<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use App\Repositories\ClientRepository;
use Illuminate\Http\Request;
use Response;

/**
 * Class ClientController
 * @package App\Http\Controllers\API
 */
class ClientSearchAPIController extends AppBaseController
{
    /** @var  ClientRepository */
    private $clientRepository;

    public function __construct(ClientRepository $clientRepo)
    {
        $this->clientRepository = $clientRepo;
    }


    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/clients",
     *      summary="Store a newly created Client in storage",
     *      tags={"Client"},
     *      description="Store Client",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Client that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Client")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Client"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function search(Request $request)
    {
        if (!$request->has('code')) {
            return $this->sendError(__('qr_invalid'));
        }
        $code = $request->get('code');

        $client = $this->clientRepository->findWhere(['code' => $code],['_id', 'email', 'name'])->first();

        if (empty($client)) {
            return $this->sendError(__('user_not_found'));
        }
        return $this->sendResponse($client->toArray(), 'Client Retrieved');
    }

}
