<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use App\Http\Requests\API\CreateFavoriteAPIRequest;
use App\Infrastructure\Repositories\Criterias\WithRelationshipsCriteria;
use App\Models\Client;
use App\Models\Favorite;
use App\Models\Place;
use App\Repositories\ClientRepository;
use App\Repositories\FavoriteRepository;
use App\Util\TranslateUtil;
use Illuminate\Http\Request;
use Response;

/**
 * Class FavoriteController
 * @package App\Http\Controllers\API
 */
class ClientFavoriteAPIController extends AppBaseController
{
    /** @var  FavoriteRepository */
    private $clientRepository;

    public function __construct(ClientRepository $clientRepository)
    {
        $this->clientRepository = $clientRepository;
    }

    /**
     * @param $clientId
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/favorites",
     *      summary="Get a listing of the Favorites.",
     *      tags={"Favorite"},
     *      description="Get all Favorites",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Favorite")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index($clientId, Request $request)
    {

        $this->clientRepository->pushCriteria(new WithRelationshipsCriteria(['comments']));
        /** @var Client $client */
        $client = $this->clientRepository->findWithoutFail($clientId);

        if (empty($client)) {
            return $this->sendError('Client not found');
        }

        if (!isset($client->favorites)) {
            return $this->sendResponse([], 'Favorites retrieved successfully');
        }
        $places = Place::with('category')->whereIn('_id', $client->favorites)->get();

        foreach ($places as &$place) {
            TranslateUtil::makeTranslation($place, 'description');
            TranslateUtil::makeTranslation($place->category, 'name');
        }
        return $this->sendResponse($places->toArray(), 'Favorites retrieved successfully');
    }

    /**
     * @param $clientId
     * @param CreateFavoriteAPIRequest $request
     * @return Response
     *
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     * @SWG\Post(
     *      path="/favorites",
     *      summary="Store a newly created Favorite in storage",
     *      tags={"Favorite"},
     *      description="Store Favorite",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Favorite that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Favorite")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Favorite"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store($clientId, CreateFavoriteAPIRequest $request)
    {
        $this->clientRepository->pushCriteria(new WithRelationshipsCriteria(['comments']));
        /** @var Client $client */
        $client = $this->clientRepository->findWithoutFail($clientId);

        if (empty($client)) {
            return $this->sendError('Client not found');
        }

        $client->push('favorites', $request->get('place_id'));

        $client->save();

        return $this->sendResponse(true, 'Favorite saved successfully');
    }

    /**
     * @param int $id
     * @param Request $request
     * @return Response
     *
     * @SWG\Delete(
     *      path="/favorites/{id}",
     *      summary="Remove the specified Favorite from storage",
     *      tags={"Favorite"},
     *      description="Delete Favorite",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Favorite",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($clientId, $id, Request $request)
    {
        $client = $this->clientRepository->findWithoutFail($clientId);

        if (empty($client)) {
            return $this->sendError('Client not found');
        }

        $client->pull('favorites', $id);

        $client->save();

        return $this->sendResponse(true, 'Favorite deleted successfully');
    }
}
