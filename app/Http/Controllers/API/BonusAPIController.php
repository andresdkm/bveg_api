<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateBonusAPIRequest;
use App\Http\Requests\API\UpdateBonusAPIRequest;
use App\Infrastructure\Repositories\Criterias\WithRelationshipsCriteria;
use App\Models\Bonus;
use App\Repositories\BonusRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class BonusController
 * @package App\Http\Controllers\API
 */

class BonusAPIController extends AppBaseController
{
    /** @var  BonusRepository */
    private $bonusRepository;

    public function __construct(BonusRepository $bonusRepo)
    {
        $this->bonusRepository = $bonusRepo;
    }

	/**
	 * @param Request $request
	 *
	 * @return Response
	 *
	 * @throws \Prettus\Repository\Exceptions\RepositoryException
	 * @SWG\Get(
	 *      path="/bonuses",
	 *      summary="Get a listing of the Bonuses.",
	 *      tags={"Bonus"},
	 *      description="Get all Bonuses",
	 *      produces={"application/json"},
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="array",
	 *                  @SWG\Items(ref="#/definitions/Bonus")
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
    public function index(Request $request)
    {
        $this->bonusRepository->pushCriteria(new RequestCriteria($request));
        $this->bonusRepository->pushCriteria(new LimitOffsetCriteria($request));
        $bonuses = $this->bonusRepository->all();

        return $this->sendResponse($bonuses->toArray(), 'Bonuses retrieved successfully');
    }

	/**
	 * @param CreateBonusAPIRequest $request
	 *
	 * @return Response
	 *
	 * @throws \Prettus\Validator\Exceptions\ValidatorException
	 * @SWG\Post(
	 *      path="/bonuses",
	 *      summary="Store a newly created Bonus in storage",
	 *      tags={"Bonus"},
	 *      description="Store Bonus",
	 *      produces={"application/json"},
	 *      @SWG\Parameter(
	 *          name="body",
	 *          in="body",
	 *          description="Bonus that should be stored",
	 *          required=false,
	 *          @SWG\Schema(ref="#/definitions/Bonus")
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="data",
	 *                  ref="#/definitions/Bonus"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
    public function store(CreateBonusAPIRequest $request)
    {
        $input = $request->all();

        $bonus = $this->bonusRepository->create($input);

        return $this->sendResponse($bonus->toArray(), __('app.new_bonus'));
    }

    /**
     * @param int $id
     * @return Response
     *
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     * @SWG\Get(
     *      path="/bonuses/{id}",
     *      summary="Display the specified Bonus",
     *      tags={"Bonus"},
     *      description="Get Bonus",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Bonus",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Bonus"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {

        $this->bonusRepository->pushCriteria(new WithRelationshipsCriteria(['place']));
        /** @var Bonus $bonus */
        $bonus = $this->bonusRepository->findByField('public_code',$id)->first();

        if (empty($bonus)) {
            return $this->sendError(__('app.bonus_not_found'));
        }

        return $this->sendResponse($bonus->toArray(), 'Bonus retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateBonusAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/bonuses/{id}",
     *      summary="Update the specified Bonus in storage",
     *      tags={"Bonus"},
     *      description="Update Bonus",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Bonus",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Bonus that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Bonus")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Bonus"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateBonusAPIRequest $request)
    {
        $input = $request->all();

        /** @var Bonus $bonus */
        $bonus = $this->bonusRepository->findWithoutFail($id);

        if (empty($bonus)) {
            return $this->sendError('Bonus not found');
        }

        $bonus = $this->bonusRepository->update($input, $id);

        return $this->sendResponse($bonus->toArray(), 'Bonus updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/bonuses/{id}",
     *      summary="Remove the specified Bonus from storage",
     *      tags={"Bonus"},
     *      description="Delete Bonus",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Bonus",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Bonus $bonus */
        $bonus = $this->bonusRepository->findWithoutFail($id);

        if (empty($bonus)) {
            return $this->sendError('Bonus not found');
        }

        $bonus->delete();

        return $this->sendResponse($id, 'Bonus deleted successfully');
    }
}
