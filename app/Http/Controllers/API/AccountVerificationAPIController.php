<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use App\Http\Requests\VerificationRequest;
use App\Models\Country;
use App\Repositories\ClientRepository;
use App\Repositories\UserRepository;
use App\Services\SmsService;
use App\Util\NumberUtil;
use Illuminate\Support\Facades\Hash;
use Response;

/**
 * Class UserController
 * @package App\Http\Controllers\API
 */
class AccountVerificationAPIController extends AppBaseController
{
    /** @var  UserRepository */
    private $clientRepository;

    public function __construct(ClientRepository $clientRepository)
    {
        $this->clientRepository = $clientRepository;
    }

    /**
     * @param VerificationRequest $request
     * @return mixed
     */
    public function verify(VerificationRequest $request)
    {
        $input = $request->all();
        $client = $this->clientRepository->findWhere(['phone' => $input['value']])->first();
        if (empty($client)) {
            return $this->sendError(__('app.user_not_found'));
        }

        if ($input['type'] == 'code') {
            $code = NumberUtil::randomNumber();
            $client->password = bcrypt($code);
            $client->save();
            $service = new SmsService();
            $country = Country::find($client->country_id);
            if (empty($country)) {
                return $this->sendError(__('app.country_not_found'));
            }
            $service->send($country->country_code . $client->phone, __('app.sms_body', ['code' => $code]));
            return $this->sendResponse(__('app.send_sms'), 'Success');
        } else if ($input['type'] == 'verify') {
            if (!Hash::check($input['code'], $client->password)) {
                return $this->sendError('Codigo Invalido', 200);
            }
            $client->verify = true;
            $client->save();
            return $this->sendResponse(true, 'Success');
        }

    }
}
