<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use App\Http\Requests\API\CreateInquiryAPIRequest;
use App\Http\Requests\API\UpdateInquiryAPIRequest;
use App\Infrastructure\Repositories\Criterias\OrderByCriteria;
use App\Infrastructure\Repositories\Criterias\WhereFieldCriteria;
use App\Infrastructure\Repositories\Criterias\WithRelationshipsCriteria;
use App\Models\Client;
use App\Models\Inquiry;
use App\Repositories\InquiryRepository;
use Illuminate\Http\Request;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class InquiryController
 * @package App\Http\Controllers\API
 */
class InquiryAPIController extends AppBaseController
{
    /** @var  InquiryRepository */
    private $inquiryRepository;

    public function __construct(InquiryRepository $inquiryRepo)
    {
        $this->inquiryRepository = $inquiryRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     * @SWG\Get(
     *      path="/inquiries",
     *      summary="Get a listing of the Inquiries.",
     *      tags={"Inquiry"},
     *      description="Get all Inquiries",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Inquiry")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->inquiryRepository->pushCriteria(new WithRelationshipsCriteria(['placeLite', 'client']));
        $this->inquiryRepository->pushCriteria(new RequestCriteria($request));
        $this->inquiryRepository->pushCriteria(new LimitOffsetCriteria($request));
        $this->inquiryRepository->pushCriteria(new OrderByCriteria('created_at'));
        if ($request->has('userId')) {
            $userId = $request->get('userId');
            $client = Client::find($userId);
            if ($client->role == 'owner') {
                $this->inquiryRepository->pushCriteria(new WhereFieldCriteria('place_id', $client->places_admin, 'in'));
            } else {
                $this->inquiryRepository->pushCriteria(new WhereFieldCriteria('client_id', $userId));
            }
            $inquiries = $this->inquiryRepository->all(['_id', 'client_id', 'place_id', 'status', 'description', 'created_at']);
        } else {
            $inquiries = $this->inquiryRepository->all(['_id', 'client_id', 'place_id', 'status', 'description', 'created_at']);
        }


        return $this->sendResponse($inquiries->toArray(), 'Inquiries retrieved successfully');
    }

    /**
     * @param CreateInquiryAPIRequest $request
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     * @SWG\Post(
     *      path="/inquiries",
     *      summary="Store a newly created Inquiry in storage",
     *      tags={"Inquiry"},
     *      description="Store Inquiry",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Inquiry that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Inquiry")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Inquiry"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateInquiryAPIRequest $request)
    {
        $input = $request->all();

        $inquiry = $this->inquiryRepository->create($input);

        return $this->sendResponse($inquiry->toArray(), 'Inquiry saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     * @SWG\Get(
     *      path="/inquiries/{id}",
     *      summary="Display the specified Inquiry",
     *      tags={"Inquiry"},
     *      description="Get Inquiry",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Inquiry",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Inquiry"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        $this->inquiryRepository->pushCriteria(new WithRelationshipsCriteria(['messages', 'placeLite','client']));
        /** @var Inquiry $inquiry */
        $inquiry = $this->inquiryRepository->findWithoutFail($id);

        if (empty($inquiry)) {
            return $this->sendError('Inquiry not found');
        }

        return $this->sendResponse($inquiry->toArray(), 'Inquiry retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateInquiryAPIRequest $request
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     * @SWG\Put(
     *      path="/inquiries/{id}",
     *      summary="Update the specified Inquiry in storage",
     *      tags={"Inquiry"},
     *      description="Update Inquiry",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Inquiry",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Inquiry that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Inquiry")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Inquiry"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateInquiryAPIRequest $request)
    {
        $input = $request->all();

        /** @var Inquiry $inquiry */
        $inquiry = $this->inquiryRepository->findWithoutFail($id);

        if (empty($inquiry)) {
            return $this->sendError('Inquiry not found');
        }

        $inquiry = $this->inquiryRepository->update($input, $id);

        return $this->sendResponse($inquiry->toArray(), 'Inquiry updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @throws \Exception
     * @SWG\Delete(
     *      path="/inquiries/{id}",
     *      summary="Remove the specified Inquiry from storage",
     *      tags={"Inquiry"},
     *      description="Delete Inquiry",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Inquiry",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Inquiry $inquiry */
        $inquiry = $this->inquiryRepository->findWithoutFail($id);

        if (empty($inquiry)) {
            return $this->sendError('Inquiry not found');
        }

        $inquiry->delete();

        return $this->sendResponse($id, 'Inquiry deleted successfully');
    }

    public function search(Request $request)
    {
        $input = $request->all();

        $this->inquiryRepository->pushCriteria(new WithRelationshipsCriteria(['placeLite', 'client']));
        $inquiry = $this->inquiryRepository->findWhere([
            'client_id' => $input['client_id'],
            'place_id' => $input['place_id'],
            'status' => Inquiry::$open
        ],['_id', 'client_id', 'place_id', 'status', 'description', 'created_at'])->first();

        if (empty($inquiry)) {
            return $this->sendResponse($inquiry, 'Inquiry retrieved successfully');
        } else {
            return $this->sendResponse($inquiry->toArray(), 'Inquiry retrieved successfully');
        }

    }
}
