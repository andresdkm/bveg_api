<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use App\Mail\LoginMail;
use App\Models\Country;
use App\Notifications\LoginEmailNotification;
use App\Repositories\ClientRepository;
use App\Repositories\UserRepository;
use App\Services\SmsService;
use App\Util\NumberUtil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use Response;

/**
 * Class UserController
 * @package App\Http\Controllers\API
 */
class ClientVerificationAPIController extends AppBaseController
{
    /** @var  UserRepository */
    private $clientRepository;

    public function __construct(ClientRepository $clientRepository)
    {
        $this->clientRepository = $clientRepository;
    }

    /**
     * @param $userId
     * @param Request $request
     *
     * @return mixed
     */
    public function verify(Request $request)
    {
        $input = $request->all();

        if ($input['type'] == 'sms') {
            $client = $this->clientRepository->findWhere(['phone' => $input['value']])->first();
        } else {
            $client = $this->clientRepository->findWhere(['email' => $input['value']])->first();
        }
        if (empty($client)) {
            return $this->sendError(__('app.user_login_not_found'));
        }
        $code = NumberUtil::randomNumber();
        $client->password = bcrypt($code);
        $client->save();
        $service = new SmsService();
        if ($input['type'] == 'sms') {
        	$country = Country::find($client->country_id);
            $service->send($country->country_code . $client->phone, __('app.sms_body', ['code' => $code]));
        } else {
            Mail::to($client)->send(new LoginMail( __('app.mail_login_title'), __('app.mail_login_body', ['code' => $code])));
        }
        return $this->sendResponse(__('app.send_sms'), 'Success');

    }

    public function login(Request $request)
    {
        $input = $request->all();

        if ($input['type'] == 'sms') {
            $client = $this->clientRepository->findWhere(['phone' => $input['value']])->first();
        } else {
            $client = $this->clientRepository->findWhere(['email' => $input['value']])->first();
        }

        if (empty($client)) {
            return $this->sendError(__('app.user_login_not_found'), 200);
        }

        if (!Hash::check($input['code'], $client->password)) {
            return $this->sendError(__('user_invalid'), 200);
        }
        if(!$client->verify){
	        $client->verify = true;
        }
        $client->lang = $input['lang'];
        $client->save();

        return $this->sendResponse($client->toArray(), 'Login Success');

    }
}
