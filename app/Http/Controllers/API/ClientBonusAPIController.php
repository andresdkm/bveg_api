<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use App\Infrastructure\Repositories\Criterias\OrderByCriteria;
use App\Infrastructure\Repositories\Criterias\WhereFieldCriteria;
use App\Infrastructure\Repositories\Criterias\WithRelationshipsCriteria;
use App\Repositories\BonusRepository;
use App\Util\BonusUtil;
use Illuminate\Http\Request;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class BonusController
 * @package App\Http\Controllers\API
 */
class ClientBonusAPIController extends AppBaseController
{
    /** @var  BonusRepository */
    private $bonusRepository;

    public function __construct(BonusRepository $bonusRepo)
    {
        $this->bonusRepository = $bonusRepo;
    }

    /**
     * @param $clientId
     * @param Request $request
     *
     * @return Response
     *
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     * @SWG\Get(
     *      path="/bonuses",
     *      summary="Get a listing of the Bonuses.",
     *      tags={"Bonus"},
     *      description="Get all Bonuses",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Bonus")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index($clientId, Request $request)
    {
        $this->bonusRepository->pushCriteria(new RequestCriteria($request));
        $this->bonusRepository->pushCriteria(new LimitOffsetCriteria($request));
        $this->bonusRepository->pushCriteria(new WhereFieldCriteria('client_id', $clientId));
	    $this->bonusRepository->pushCriteria(new OrderByCriteria('status', 'asc'));
	    $this->bonusRepository->pushCriteria(new OrderByCriteria('created_at', 'desc'));
        $this->bonusRepository->pushCriteria(new WithRelationshipsCriteria(['placeLite']));
        $bonuses = $this->bonusRepository->all();
        return $this->sendResponse($bonuses->toArray(), 'Bonuses retrieved successfully');
    }

}
