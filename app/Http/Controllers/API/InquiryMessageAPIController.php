<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use App\Http\Requests\API\CreateInquiryMessageAPIRequest;
use App\Infrastructure\Repositories\Criterias\WithRelationshipsCriteria;
use App\Models\Inquiry;
use App\Repositories\InquiryRepository;
use App\Util\PushUtil;
use Carbon\Carbon;
use Response;

/**
 * Class InquiryMessageController
 * @package App\Http\Controllers\API
 */
class InquiryMessageAPIController extends AppBaseController {
	/** @var  InquiryRepository */
	private $inquiryRepository;

	public function __construct( InquiryRepository $inquiryRepo ) {
		$this->inquiryRepository = $inquiryRepo;
	}


	/**
	 * @param $inquiryId
	 *
	 * @return Response
	 *
	 * @throws \Prettus\Repository\Exceptions\RepositoryException
	 * @SWG\Get(
	 *      path="/inquiries/{inquiryId}",
	 *      summary="Display the specified Inquiry",
	 *      tags={"Inquiry"},
	 *      description="Get Inquiry",
	 *      produces={"application/json"},
	 *      @SWG\Parameter(
	 *          name="id",
	 *          description="id of Inquiry",
	 *          type="integer",
	 *          required=true,
	 *          in="path"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="data",
	 *                  ref="#/definitions/Inquiry"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
	public function index( $inquiryId ) {
		$this->inquiryRepository->pushCriteria( new WithRelationshipsCriteria( [ 'messages.client' ] ) );
		/** @var Inquiry $inquiry */
		$inquiry = $this->inquiryRepository->findWithoutFail( $inquiryId );

		if ( empty( $inquiry ) ) {
			return $this->sendError( 'Inquiry not found' );
		}

		return $this->sendResponse( $inquiry->messages->toArray(), 'Inquiry retrieved successfully' );
	}

	/**
	 * @param $inquiryId
	 * @param CreateInquiryMessageAPIRequest $request
	 *
	 * @return Response
	 *
	 * @SWG\Post(
	 *      path="/inquiries",
	 *      summary="Store a newly created Inquiry in storage",
	 *      tags={"Inquiry"},
	 *      description="Store Inquiry",
	 *      produces={"application/json"},
	 *      @SWG\Parameter(
	 *          name="body",
	 *          in="body",
	 *          description="Inquiry that should be stored",
	 *          required=false,
	 *          @SWG\Schema(ref="#/definitions/Inquiry")
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="data",
	 *                  ref="#/definitions/Inquiry"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
	public function store( $inquiryId, CreateInquiryMessageAPIRequest $request ) {
		/** @var Inquiry $inquiry */
		$inquiry = $this->inquiryRepository->findWithoutFail( $inquiryId );

		if ( empty( $inquiry ) ) {
			return $this->sendError( 'Inquiry not found' );
		}
		$input = $request->all();

		$response = $inquiry->messages()->create( $input );
		if ( $inquiry->client_id != $input['client_id'] ) {
			$pushUtil = new PushUtil();
			$pushUtil->answerInquiry( $inquiry->client_id, $inquiry->place_id );
		}

		$this->inquiryRepository->pushCriteria( new WithRelationshipsCriteria( [ 'messages', 'placeLite' ] ) );
		/** @var Inquiry $inquiry */
		$inquiry = $this->inquiryRepository->findWithoutFail( $inquiryId );

		$messages = [];

		if ( $inquiry->messages != null ) {
			foreach ( $inquiry->messages as $message ) {
				if ( Carbon::parse( $message->created_at )->greaterThanOrEqualTo( Carbon::parse( $response->created_at ) ) ) {
					array_push( $messages, $message );
				}
			}
		}

		return $this->sendResponse( $messages, 'Inquiry saved successfully' );
	}
}
