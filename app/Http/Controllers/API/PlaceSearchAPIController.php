<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use App\Models\Place;
use App\Services\PlaceService;
use App\Util\TranslateUtil;
use Illuminate\Http\Request;

class PlaceSearchAPIController extends AppBaseController
{

    public function getAllSites(Request $request)
    {
        $input = $request->all();
        $queryMongodb[] =
            [
                '$geoNear' => [
                    'near' => [
                        'type' => 'Point',
                        'coordinates' => [
                            floatval($input['longitude']),
                            floatval($input['latitude']),
                        ],
                    ],
                    'distanceField' => "distance",
                    "spherical" => true,
                    "minDistance" => 0
                ]

            ];
        $queryMongodb[] = [
            '$limit' => 1000
        ];
        $queryMongodb[] = [
            '$match' => [
                'status' => true
            ],

        ];
        $queryMongodb[] = [
            '$project' => [
                'geolocation' => 1,
                '_id' => 1,
                'category_id' => 1
            ]
        ];
        $data = Place::raw(function ($collection) use ($queryMongodb) {
            return $collection->aggregate($queryMongodb);
        });
        $places = Place::hydrate($data->toArray());

        return $places;
    }

    public function search(Request $request)
    {


        $input = $request->all();
        $queryMongodb = [];
        $maxDistance = $this->getZoom($request->get('zoom', 0));
        $paginate = 0;
        if ($maxDistance > 0) {
            $paginate = $this->getPage($request->get('zoom', 0));
        }
        if ($request->exists('text') && $request->get('text') != null) {
            $data = Place::whereRaw(array(
                '$text' => array('$search' => "\"" . $request->get('text') . "\""),
                'status' => true
            ))->get();
        } else if ($request->exists('all') && $request->get('all') != null) {
            $places = $this->getAllSites($request);
            return $this->sendResponse($places, 'Places retrieved successfully' . $maxDistance);

        } else if ($maxDistance == 0) {
            $queryMongodb[] =
                [
                    '$geoNear' => [
                        'near' => [
                            'type' => 'Point',
                            'coordinates' => [
                                floatval($input['longitude']),
                                floatval($input['latitude']),
                            ],
                        ],
                        'distanceField' => "distance",
                        "spherical" => true,
                        "minDistance" => 0
                    ],


                ];
            $queryMongodb[] = [
                '$limit' => 1000
            ];

            if ($request->exists('category') && $request->get('category') != null) {
                $queryMongodb[] = [
                    '$match' => [
                        'category_id' => $request->get('category')
                    ]
                ];
            }

            if ($request->exists('rating') && $request->get('rating') != null) {
                $queryMongodb[] = [
                    '$match' => [
                        'rating' => intval($request->get('rating'))
                    ]
                ];
            }

            $queryMongodb[] = [
                '$match' => [
                    'status' => true
                ]
            ];
            $data = Place::raw(function ($collection) use ($queryMongodb) {
                return $collection->aggregate($queryMongodb);
            });
        } else {
            $queryMongodb[] =
                [
                    '$geoNear' => [
                        'near' => [
                            'type' => 'Point',
                            'coordinates' => [
                                floatval($input['longitude']),
                                floatval($input['latitude']),
                            ],
                        ],
                        'distanceField' => "distance",
                        "spherical" => true,
                        "maxDistance" => $maxDistance,
                    ]


                ];

            if ($request->exists('category') && $request->get('category') != null) {
                $queryMongodb[] = [
                    '$match' => [
                        'category_id' => $request->get('category')
                    ]
                ];
            }

            if ($request->exists('rating') && $request->get('rating') != null) {
                $queryMongodb[] = [
                    '$match' => [
                        'rating' => intval($request->get('rating'))
                    ]
                ];
            }

            $queryMongodb[] = [
                '$match' => [
                    'status' => true
                ]
            ];
            $data = Place::raw(function ($collection) use ($queryMongodb) {
                return $collection->aggregate($queryMongodb);
            });
        }


        $places = Place::hydrate($data->toArray());
        $placesTop = PlaceService::getTopPlaces($request->get('category', null))->pluck('_id');

        if ($request->exists('paginate')) {
            $responseModel = new \stdClass();
            $responseModel->current_page = intval($request->get('page', 1));
            $responseModel->per_page = intval($request->get('paginate'));
            $responseModel->total = $places->count();
            $responseModel->data = $places->forPage($responseModel->current_page, $responseModel->per_page)->values()->all();
            $responseModel->last_page = ceil($responseModel->total / $responseModel->per_page);
            foreach ($responseModel->data as &$place) {
                TranslateUtil::makeTranslation($place, 'description');
                TranslateUtil::makeTranslation($place->category, 'name');
                foreach ($placesTop as $index => $_id) {
                    if ($_id == $place->_id) {
                        $place->top = $index + 1;
                    }
                }
            }

            return $this->sendResponse($responseModel, 'Places retrieved successfully');
        } else if ($paginate > 0) {
            $responseModel = new \stdClass();
            $responseModel->current_page = intval($request->get('page', 1));
            $responseModel->per_page = intval($paginate);
            $responseModel->total = $places->count();
            $responseModel->data = $places->forPage($responseModel->current_page, $responseModel->per_page)->values()->all();
            $responseModel->last_page = ceil($responseModel->total / $responseModel->per_page);
            foreach ($responseModel->data as &$place) {
                TranslateUtil::makeTranslation($place, 'description');
                TranslateUtil::makeTranslation($place->category, 'name');
                foreach ($placesTop as $index => $_id) {
                    if ($_id == $place->_id) {
                        $place->top = $index + 1;
                    }
                }
            }

            return $this->sendResponse($responseModel, 'Places retrieved successfully');
        } else {
            foreach ($places as &$place) {
                TranslateUtil::makeTranslation($place, 'description');
                TranslateUtil::makeTranslation($place->category, 'name');
                $valueTop = $placesTop->search($place->_id);
                if ($valueTop) {
                    $place->top = $valueTop;
                }
            }

            return $this->sendResponse($places, 'Places retrieved successfully' . $maxDistance);
        }


    }

    public function getZoom($zoom)
    {
        switch ($zoom) {
            case 20:
                return 1128;
                break;
            case 19:
                return 2256;
                break;
            case 18:
                return 4513;
                break;
            case 17:
                return 9027;
                break;
            case 16:
                return 18055;
                break;
            case 15:
                return 36111;
                break;
            case 14:
                return 72223;
                break;
            case 13:
                return 144447;
                break;
            case 12:
                return 288895;
                break;
            case 11:
                return 577790;
                break;
            case 10:
                return 1155581;
                break;
            case 9:
                return 2311162;
                break;
            case 8:
                return 4622324;
                break;
            case 7:
                return 9244649;
                break;
            case 6:
                return 18489298;
                break;
            case 5:
                return 36978596;
                break;
            case 4:
                return 73957193;
                break;
            case 3:
                return 147914387;
                break;
            case 2:
                return 295828775;
                break;
            case 1:
                return 591657550;
                break;
            case 0:
                return 0;
                break;
        }
    }


    public function getPage($zoom)
    {
        switch ($zoom) {
            case 20:
                return 30;
                break;
            case 19:
                return 30;
                break;
            case 18:
                return 30;
                break;
            case 17:
                return 30;
                break;
            case 16:
                return 30;
                break;
            case 15:
                return 30;
                break;
            case 14:
                return 30;
                break;
            case 13:
                return 30;
                break;
            case 12:
                return 30;
                break;
            case 11:
                return 30;
                break;
            case 10:
                return 30;
                break;
            case 9:
                return 30;
                break;
            case 8:
                return 30;
                break;
            case 7:
                return 30;
                break;
            case 6:
                return 30;
                break;
            case 5:
                return 30;
                break;
            case 4:
                return 30;
                break;
            case 3:
                return 30;
                break;
            case 2:
                return 30;
                break;
            case 1:
                return 30;
                break;
            case 0:
                return 30;
                break;
        }
    }
}
