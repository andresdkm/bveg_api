<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use App\Http\Requests\API\UpdatePlaceAPIRequest;
use App\Models\Place;
use App\Repositories\PlaceRepository;
use Illuminate\Http\Request;

class PlaceConfigurationAPIController extends AppBaseController
{

    /** @var  PlaceRepository */
    private $placeRepository;

    public function __construct(PlaceRepository $placeRepo)
    {
        $this->placeRepository = $placeRepo;
    }

    /**
     * @param $placeId
     * @param Request $request
     * @return Response
     * @SWG\Get(
     *      path="/places/{id}",
     *      summary="Display the specified Place",
     *      tags={"Place"},
     *      description="Get Place",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Place",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Place"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($placeId, Request $request)
    {

        /** @var Place $place */
        $place = $this->placeRepository->findWithoutFail($placeId);

        if (empty($place)) {
            return $this->sendError('Place not found');
        }
        if ($place->configuration != null) {
            return $this->sendResponse($place->configuration->toArray(), 'Place Configuration retrieved successfully');
        } else {
            return $this->sendResponse(null, 'Place Configuration retrieved successfully');
        }

    }

    /**
     * @param $placeId
     * @param UpdatePlaceAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/places/{id}",
     *      summary="Update the specified Place in storage",
     *      tags={"Place"},
     *      description="Update Place",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Place",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Place that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Place")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Place"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($placeId, UpdatePlaceAPIRequest $request)
    {
        $input = $request->all();

        /** @var Place $place */
        $place = $this->placeRepository->findWithoutFail($placeId);

        if (empty($place)) {
            return $this->sendError('Place not found');
        }

        $place->configuration()->create($input);

        return $this->sendResponse($place->configuration->toArray(), 'Place updated successfully');
    }
}
