<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateNotifyAPIRequest;
use App\Http\Requests\API\UpdateNotifyAPIRequest;
use App\Models\Notify;
use App\Repositories\NotifyRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class NotifyController
 * @package App\Http\Controllers\API
 */

class NotifyAPIController extends AppBaseController
{
    /** @var  NotifyRepository */
    private $notifyRepository;

    public function __construct(NotifyRepository $notifyRepo)
    {
        $this->notifyRepository = $notifyRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/notifies",
     *      summary="Get a listing of the Notifies.",
     *      tags={"Notify"},
     *      description="Get all Notifies",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Notify")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->notifyRepository->pushCriteria(new RequestCriteria($request));
        $this->notifyRepository->pushCriteria(new LimitOffsetCriteria($request));
        $notifies = $this->notifyRepository->all();

        return $this->sendResponse($notifies->toArray(), 'Notifies retrieved successfully');
    }

    /**
     * @param CreateNotifyAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/notifies",
     *      summary="Store a newly created Notify in storage",
     *      tags={"Notify"},
     *      description="Store Notify",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Notify that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Notify")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Notify"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateNotifyAPIRequest $request)
    {
        $input = $request->all();

        $notifies = $this->notifyRepository->create($input);

        return $this->sendResponse($notifies->toArray(), 'Notify saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/notifies/{id}",
     *      summary="Display the specified Notify",
     *      tags={"Notify"},
     *      description="Get Notify",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Notify",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Notify"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Notify $notify */
        $notify = $this->notifyRepository->findWithoutFail($id);

        if (empty($notify)) {
            return $this->sendError('Notify not found');
        }

        return $this->sendResponse($notify->toArray(), 'Notify retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateNotifyAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/notifies/{id}",
     *      summary="Update the specified Notify in storage",
     *      tags={"Notify"},
     *      description="Update Notify",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Notify",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Notify that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Notify")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Notify"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateNotifyAPIRequest $request)
    {
        $input = $request->all();

        /** @var Notify $notify */
        $notify = $this->notifyRepository->findWithoutFail($id);

        if (empty($notify)) {
            return $this->sendError('Notify not found');
        }

        $notify = $this->notifyRepository->update($input, $id);

        return $this->sendResponse($notify->toArray(), 'Notify updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/notifies/{id}",
     *      summary="Remove the specified Notify from storage",
     *      tags={"Notify"},
     *      description="Delete Notify",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Notify",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Notify $notify */
        $notify = $this->notifyRepository->findWithoutFail($id);

        if (empty($notify)) {
            return $this->sendError('Notify not found');
        }

        $notify->delete();

        return $this->sendResponse($id, 'Notify deleted successfully');
    }
}
