<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateReplyAPIRequest;
use App\Http\Requests\API\UpdateReplyAPIRequest;
use App\Models\Comment;
use App\Models\Reply;
use App\Repositories\CommentRepository;
use App\Repositories\ReplyRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class ReplyController
 * @package App\Http\Controllers\API
 */

class ReplyAPIController extends AppBaseController
{
    /** @var  ReplyRepository */
    private $replyRepository;

    /** @var  CommentRepository */
    private $commentRepository;


    public function __construct(ReplyRepository $replyRepo, CommentRepository $commentRepo)
    {
        $this->replyRepository = $replyRepo;

        $this->commentRepository = $commentRepo;
    }

    /**
     * @param $commentId
     * @param Request $request
     * @return Response
     *
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     * @SWG\Get(
     *      path="/replies",
     *      summary="Get a listing of the Replies.",
     *      tags={"Reply"},
     *      description="Get all Replies",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Reply")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index($commentId, Request $request)
    {
        $this->replyRepository->pushCriteria(new RequestCriteria($request));
        $this->replyRepository->pushCriteria(new LimitOffsetCriteria($request));
        $replies = $this->replyRepository->all();

        return $this->sendResponse($replies->toArray(), 'Replies retrieved successfully');
    }

    /**
     * @param $commentId
     * @param CreateReplyAPIRequest $request
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     * @SWG\Post(
     *      path="/replies",
     *      summary="Store a newly created Reply in storage",
     *      tags={"Reply"},
     *      description="Store Reply",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Reply that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Reply")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Reply"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store($commentId, CreateReplyAPIRequest $request)
    {
        $input = $request->all();

        /** @var Comment $comment */
        $comment = $this->commentRepository->findWithoutFail($commentId);

        if (empty($comment)) {
            return $this->sendError('Comment not found');
        }

        $reply = $comment->replies()->create($input);
        return $this->sendResponse($reply->toArray(), 'Reply saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/replies/{id}",
     *      summary="Display the specified Reply",
     *      tags={"Reply"},
     *      description="Get Reply",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Reply",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Reply"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Reply $reply */
        $reply = $this->replyRepository->findWithoutFail($id);

        if (empty($reply)) {
            return $this->sendError('Reply not found');
        }

        return $this->sendResponse($reply->toArray(), 'Reply retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateReplyAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/replies/{id}",
     *      summary="Update the specified Reply in storage",
     *      tags={"Reply"},
     *      description="Update Reply",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Reply",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Reply that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Reply")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Reply"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateReplyAPIRequest $request)
    {
        $input = $request->all();

        /** @var Reply $reply */
        $reply = $this->replyRepository->findWithoutFail($id);

        if (empty($reply)) {
            return $this->sendError('Reply not found');
        }

        $reply = $this->replyRepository->update($input, $id);

        return $this->sendResponse($reply->toArray(), 'Reply updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/replies/{id}",
     *      summary="Remove the specified Reply from storage",
     *      tags={"Reply"},
     *      description="Delete Reply",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Reply",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Reply $reply */
        $reply = $this->replyRepository->findWithoutFail($id);

        if (empty($reply)) {
            return $this->sendError('Reply not found');
        }

        $reply->delete();

        return $this->sendResponse($id, 'Reply deleted successfully');
    }
}
