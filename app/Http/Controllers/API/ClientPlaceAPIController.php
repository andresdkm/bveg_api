<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use App\Infrastructure\Repositories\Criterias\WhereFieldCriteria;
use App\Repositories\ClientRepository;
use App\Repositories\PlaceRepository;
use Illuminate\Http\Request;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class PlaceController
 * @package App\Http\Controllers\API
 */
class ClientPlaceAPIController extends AppBaseController
{
    /** @var  PlaceRepository */
    private $placeRepository;

    public function __construct(PlaceRepository $placeRepo)
    {
        $this->placeRepository = $placeRepo;
    }

    /**
     * @param $clientId
     * @param Request $request
     *
     * @param ClientRepository $clientRepository
     * @return Response
     *
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     * @SWG\Get(
     *      path="/places",
     *      summary="Get a listing of the Places.",
     *      tags={"Place"},
     *      description="Get all Places",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Place")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index($clientId, Request $request, ClientRepository $clientRepository)
    {
        $this->placeRepository->pushCriteria(new RequestCriteria($request));
        $this->placeRepository->pushCriteria(new LimitOffsetCriteria($request));
        $client = $clientRepository->findWithoutFail($clientId);
        if (empty($client)) {
            return $this->sendError(__('user_not_found'));
        }
        if( $client->places_admin){
            $this->placeRepository->pushCriteria(new WhereFieldCriteria('_id', $client->places_admin, 'in'));
            $places = $this->placeRepository->all(['id', 'name','configuration']);
        }else{
            return $this->sendResponse([], 'Places retrieved successfully');
        }

        return $this->sendResponse($places->toArray(), 'Places retrieved successfully');
    }


}
