<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class LoginAPIController extends AppBaseController {
	/** @var  UserRepository */
	private $userRepository;

	public function __construct( UserRepository $userRepo ) {
		$this->userRepository = $userRepo;
	}


	public function login( Request $request ) {
		$input = $request->all();

		$user = $this->userRepository->findByField( 'email', $input['email'] )->first();

		if ( empty( $user ) ) {
			return $this->sendError( 'User not found' );
		}

		if (trim($user->password) == trim($input['password'])) {
			$user->password = null;
			return $this->sendResponse( $user->toArray(), 'Login successfully' );
		} else {
			return $this->sendError( 'Invalid password', 200 );
		}

	}
}
