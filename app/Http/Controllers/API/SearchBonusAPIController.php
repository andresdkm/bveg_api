<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use App\Repositories\BonusRepository;
use Illuminate\Http\Request;
use Response;

/**
 * Class BonusController
 * @package App\Http\Controllers\API
 */
class SearchBonusAPIController extends AppBaseController {
	/** @var  BonusRepository */
	private $bonusRepository;

	public function __construct( BonusRepository $bonusRepo ) {
		$this->bonusRepository = $bonusRepo;
	}

	/**
	 * @param Request $request
	 *
	 * @return Response
	 *
	 * @throws \Prettus\Validator\Exceptions\ValidatorException
	 * @SWG\Post(
	 *      path="/bonuses",
	 *      summary="Store a newly created Bonus in storage",
	 *      tags={"Bonus"},
	 *      description="Store Bonus",
	 *      produces={"application/json"},
	 *      @SWG\Parameter(
	 *          name="body",
	 *          in="body",
	 *          description="Bonus that should be stored",
	 *          required=false,
	 *          @SWG\Schema(ref="#/definitions/Bonus")
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="data",
	 *                  ref="#/definitions/Bonus"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
	public function store( Request $request ) {
		try {
			$input = $request->all();

			$bonus = $this->bonusRepository->assign( $input );

			return $this->sendResponse( $bonus->toArray(), __( 'app.new_bonus' ) );

		} catch ( \Exception $exception ) {
			return $this->sendError( $exception->getMessage() );
		}

	}


}
