<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Util\NumberUtil;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class UserCodeController extends Controller
{
    public function show($userId)
    {

        $user = Client::find($userId);

        if (empty($user)) {
            return "";
        }
        if ($user->code == null) {
            $code = NumberUtil::randomString('u', 15);
            $user->code = $code;
            $user->save();
        }
        $image = QrCode::format('png')
            ->merge(storage_path('images/icon.png'), 0.2, true)
            ->size(300)->errorCorrection('H')
            ->backgroundColor(0, 184, 74)
            ->generate($user->code);

        return response($image)->header('Content-type', 'image/png');

    }
}
