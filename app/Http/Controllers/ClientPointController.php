<?php

namespace App\Http\Controllers;

use App\DataTables\ClientPointDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateClientPointRequest;
use App\Http\Requests\UpdateClientPointRequest;
use App\Repositories\ClientPointRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Response;

class ClientPointController extends AppBaseController
{
    /** @var  ClientPointRepository */
    private $clientPointRepository;

    public function __construct(ClientPointRepository $clientPointRepo)
    {
        $this->clientPointRepository = $clientPointRepo;
    }

	/**
	 * Display a listing of the ClientPoint.
	 *
	 * @param $placeId
	 * @param ClientPointDataTable $clientPointDataTable
	 * @param Request $request
	 *
	 * @return Response
	 */
    public function index($placeId, ClientPointDataTable $clientPointDataTable, Request $request)
    {
	    return $clientPointDataTable
		    ->with('place_id', $placeId)
		    ->render('client_points.index');
    }

    /**
     * Show the form for creating a new ClientPoint.
     *
     * @return Response
     */
    public function create()
    {
        return view('client_points.create');
    }

    /**
     * Store a newly created ClientPoint in storage.
     *
     * @param CreateClientPointRequest $request
     *
     * @return Response
     */
    public function store(CreateClientPointRequest $request)
    {
        $input = $request->all();

        $clientPoint = $this->clientPointRepository->create($input);

        Flash::success('Client Point saved successfully.');

        return redirect(route('clientPoints.index'));
    }

    /**
     * Display the specified ClientPoint.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $clientPoint = $this->clientPointRepository->findWithoutFail($id);

        if (empty($clientPoint)) {
            Flash::error('Client Point not found');

            return redirect(route('clientPoints.index'));
        }

        return view('client_points.show')->with('clientPoint', $clientPoint);
    }

    /**
     * Show the form for editing the specified ClientPoint.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $clientPoint = $this->clientPointRepository->findWithoutFail($id);

        if (empty($clientPoint)) {
            Flash::error('Client Point not found');

            return redirect(route('clientPoints.index'));
        }

        return view('client_points.edit')->with('clientPoint', $clientPoint);
    }

    /**
     * Update the specified ClientPoint in storage.
     *
     * @param  int              $id
     * @param UpdateClientPointRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateClientPointRequest $request)
    {
        $clientPoint = $this->clientPointRepository->findWithoutFail($id);

        if (empty($clientPoint)) {
            Flash::error('Client Point not found');

            return redirect(route('clientPoints.index'));
        }

        $clientPoint = $this->clientPointRepository->update($request->all(), $id);

        Flash::success('Client Point updated successfully.');

        return redirect(route('clientPoints.index'));
    }

    /**
     * Remove the specified ClientPoint from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $clientPoint = $this->clientPointRepository->findWithoutFail($id);

        if (empty($clientPoint)) {
            Flash::error('Client Point not found');

            return redirect(route('clientPoints.index'));
        }

        $this->clientPointRepository->delete($id);

        Flash::success('Client Point deleted successfully.');

        return redirect(route('clientPoints.index'));
    }
}
