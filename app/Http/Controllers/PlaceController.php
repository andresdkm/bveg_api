<?php

namespace App\Http\Controllers;

use App\DataTables\PlaceDataTable;
use App\Http\Requests\CreatePlaceRequest;
use App\Http\Requests\UpdatePlaceRequest;
use App\Models\Category;
use App\Repositories\PlaceRepository;
use App\Util\FileImageUtil;
use Flash;
use Illuminate\Support\Facades\View;
use Response;

class PlaceController extends AppBaseController {
	/** @var  PlaceRepository */
	private $placeRepository;

	public function __construct( PlaceRepository $placeRepo ) {
		$this->placeRepository = $placeRepo;
		$categories            = Category::all()->pluck( 'name.es', '_id' );;
		View::share( 'categories', $categories );
	}

	/**
	 * Display a listing of the Place.
	 *
	 * @param PlaceDataTable $placeDataTable
	 *
	 * @return Response
	 */
	public function index( PlaceDataTable $placeDataTable ) {
		return $placeDataTable->render( 'places.index' );
	}

	/**
	 * Show the form for creating a new Place.
	 *
	 * @return Response
	 */
	public function create() {
		return view( 'places.create' );
	}

	/**
	 * Store a newly created Place in storage.
	 *
	 * @param CreatePlaceRequest $request
	 *
	 * @return Response
	 */
	public function store( CreatePlaceRequest $request ) {
		$input = $request->all();

		$input = FileImageUtil::uploadFile( $request, 'image', 'image', $input );

		$place = $this->placeRepository->create( $input );

		Flash::success( 'Place saved successfully.' );

		return redirect( route( 'places.index' ) );
	}

	/**
	 * Display the specified Place.
	 *
	 * @param int $id
	 *
	 * @return Response
	 */
	public function show( $id ) {
		$place = $this->placeRepository->findWithoutFail( $id );

		if ( empty( $place ) ) {
			Flash::error( 'Place not found' );

			return redirect( route( 'places.index' ) );
		}

		return view( 'places.show' )->with( 'place', $place );
	}

	/**
	 * Show the form for editing the specified Place.
	 *
	 * @param int $id
	 *
	 * @return Response
	 */
	public function edit( $id ) {
		$place = $this->placeRepository->findWithoutFail( $id );

		if ( empty( $place ) ) {
			Flash::error( 'Place not found' );

			return redirect( route( 'places.index' ) );
		}

		return view( 'places.edit' )
			->with( 'place', $place );
	}

	/**
	 * Update the specified Place in storage.
	 *
	 * @param int $id
	 * @param UpdatePlaceRequest $request
	 *
	 * @return Response
	 */
	public function update( $id, UpdatePlaceRequest $request ) {
		$place = $this->placeRepository->findWithoutFail( $id );

		if ( empty( $place ) ) {
			Flash::error( 'Place not found' );

			return redirect( route( 'places.index' ) );
		}
		if($request->has('action')){
			if($request->get('action') == 'remove_image'){
				$photoName = $request->get('photo_name');

				$place->pull( 'photos', $photoName );

				$place->save();

				Flash::success( 'Place updated successfully.' );

				return redirect( route( 'places.show',[$id] ) );
			}
		}

		if ( $request->hasFile( 'photo' ) ) {
			$file = $request->file( 'photo' );

			$name = FileImageUtil::simpleUploadFile( $file );

			$place->push( 'photos', $name );

			$place->save();

			Flash::success( 'Place updated successfully.' );

			return redirect( route( 'places.show',[$id] ) );
		} else {
			$input = $request->all();

			$input = FileImageUtil::uploadFile( $request, 'image', 'image', $input );

			$place = $this->placeRepository->update( $input, $id );

			Flash::success( 'Place updated successfully.' );

			return redirect( route( 'places.show',[$id] ) );
		}


	}

	/**
	 * Remove the specified Place from storage.
	 *
	 * @param int $id
	 *
	 * @return Response
	 */
	public function destroy( $id ) {
		$place = $this->placeRepository->findWithoutFail( $id );

		if ( empty( $place ) ) {
			Flash::error( 'Place not found' );

			return redirect( route( 'places.index' ) );
		}

		$this->placeRepository->delete( $id );

		Flash::success( 'Place deleted successfully.' );

		return redirect( route( 'places.index' ) );
	}
}
