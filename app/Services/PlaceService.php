<?php
/**
 * Created by PhpStorm.
 * User: andresdkm
 * Date: 16/01/19
 * Time: 10:49 AM
 */

namespace App\Services;


use App\Models\Comment;
use App\Models\Place;
use Illuminate\Support\Facades\Cache;

class PlaceService {

	public static function getTopPlaces( $categoryId = null ) {
		$queryMongodb = [
			[
				'$group' => [
					'_id'   => '$place_id',
					'count' => [
						'$sum' => 1
					]
				],
			],
			[
				'$sort' => [
					'count' => - 1
				]
			]
		];

		if ( $categoryId != null ) {
			$placesId       = Place::where( 'category_id', $categoryId )->get()->pluck( '_id' )->toArray();
			$queryMongodb[] = [
				'$match' => [
					'_id' => [
						'$in' => $placesId
					]
				]
			];
		}

		$data = Cache::remember( 'sites_all_2324' . $categoryId, 3600, function () use ( $queryMongodb ) {
			return Comment::raw( function ( $collection ) use ( $queryMongodb ) {
				return $collection->aggregate( $queryMongodb );
			} );
		} );

		return $data;
	}
}