<?php
/**
 * Created by PhpStorm.
 * User: andresdkm
 * Date: 27/01/19
 * Time: 05:40 PM
 */

namespace App\Services;


use App\Models\UserTravelFile;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class FileService
{


    public static function uploadFile($file, $userTravelId)
    {
        $extension = $file->getClientOriginalExtension();
        $now = new Carbon();
        $path = $now->timestamp . '.' . $extension;
        Storage::disk('public')->put($path, File::get($file));
        $userTravelFile = new UserTravelFile();
        $userTravelFile->user_travel_id = $userTravelId;
        $userTravelFile->file = $path;
        $userTravelFile->save();

        return $path;
    }

    public static function simpleUploadFile($file)
    {
        $extension = $file->getClientOriginalExtension();
        $now = new Carbon();
        $path = $now->timestamp . '.' . $extension;
        Storage::disk('public')->put($path, File::get($file));
        return $path;
    }

    public static function deleteDirectory($dirPath)
    {
        if (is_dir($dirPath)) {
            $objects = scandir($dirPath);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (filetype($dirPath . DIRECTORY_SEPARATOR . $object) == "dir") {
                        FileService::deleteDirectory($dirPath . DIRECTORY_SEPARATOR . $object);
                    } else {
                        unlink($dirPath . DIRECTORY_SEPARATOR . $object);
                    }
                }
            }
            reset($objects);
            rmdir($dirPath);
        }
    }
}