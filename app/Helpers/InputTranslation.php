<?php


namespace App\Helpers;


use App\Util\TranslateUtil;

class InputTranslation {

	public static function input( $label, $id, $model = null ) {

		$html = '';
		foreach ( TranslateUtil::$languagesAvailable as $lang ) {
			$value = null;
			if ( $model != null && isset( $model->{$id}[ $lang ] ) ) {
				$value = $model->{$id}[ $lang ];
			}
			$html .= '<div class="form-group col-sm-6">' .
			         '<label for="' . $id . '_' . $lang . '">' . $label . ' (' . $lang . ') </label>' .
			         '<input type="text" class="form-control" name="' . $id . '_' . $lang . '" value="' . $value . '" />' .
			         '</div>';
		}


		return $html;
	}

	public static function textarea( $label, $id, $model = null ) {

		$html = '';
		foreach ( TranslateUtil::$languagesAvailable as $lang ) {
			$value = null;
			if ( $model != null && isset( $model->{$id}[ $lang ] ) ) {
				$value = $model->{$id}[ $lang ];
			}
			$html .= '<div class="form-group col-sm-6">' .
			         '<label for="' . $id . '_' . $lang . '">' . $label . ' (' . $lang . ') </label>' .
			         '<textarea class="form-control" name="' . $id . '_' . $lang . '" >' . $value . '</textarea>' .
			         '</div>';
		}

		return $html;
	}


}