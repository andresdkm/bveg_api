<?php


namespace App\Observers;


use App\Mail\NewBonus;
use App\Models\Bonus;
use App\Models\Place;
use App\Util\PushUtil;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class BonusObserver {

	public function created( Bonus $bonus ) {
		if ( $bonus->email != null ) {
			$image = QrCode::format( 'png' )
			               ->merge( storage_path( 'images/icon.png' ), 0.2, true )
			               ->size( 300 )->errorCorrection( 'H' )
			               ->backgroundColor( 0, 184, 74 )
			               ->generate( env( 'APP_URL' ) . '/bonus/' . $bonus->public_code );

			$output_file = '/img/qr-code/img-' . time() . '.png';

			Storage::disk( 'public' )->put( $output_file, $image ); //storage/app/public/img/qr-code/img-1557309130.png

			$place = Place::find( $bonus->place_id );

			Mail::to( $bonus->email )->send( new NewBonus( $output_file, $place->name, $bonus->amount, $bonus->public_code ) );

		}

		if ( $bonus->client_id != null ) {
			$pushUtil = new PushUtil();
			$pushUtil->sendBonus( $bonus->client_id, $bonus->place_id );
		}


	}
}