<?php


namespace App\Observers;


use App\Models\ClientPoint;
use App\Util\PushUtil;

class ClientPointObserver {

	public function created( ClientPoint $clientPoint ) {
		$pushUtil = new PushUtil();
		$pushUtil->sendPoints( $clientPoint->client_id, $clientPoint->place_id, $clientPoint->points );
	}
}