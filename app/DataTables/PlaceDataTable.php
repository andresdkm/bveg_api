<?php

namespace App\DataTables;

use App\Models\Place;
use Pimlie\DataTables\MongodbDataTable;
use Yajra\DataTables\Services\DataTable;

class PlaceDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new MongodbDataTable($query);

        return $dataTable->addColumn('action', 'places.datatables_actions')
	        ->editColumn('status', function($class) {
		        $status = ($class->status)
			        ? 'Activo'
			        : 'Inactivo';
		        return $status;
	        })
            ->editColumn('latitude', function($class) {
                return $class->geolocation['coordinates'][1];
            })
            ->editColumn('longitude', function($class) {
                return $class->geolocation['coordinates'][0];
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Place $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Place $model)
    {
        return $model->with('category')->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px'])
            ->parameters([
	            'language' => ['url' => url('/lang/Spanish.json')],
	            'dom'     => 'Bfrtip',
                'order'   => [[0, 'desc']],
                'buttons' => [
                    ['extend' => 'create', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'export', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'print', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reset', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reload', 'className' => 'btn btn-default btn-sm no-corner',],
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' =>'name', 'title' => 'Nombre'],
	        ['data' =>'phone', 'title' => 'Teléfono'],
	        ['data' =>'address', 'title' => 'Dirección'],
	        ['data' =>'category.name.es', 'title' => 'Categoria'],
	        ['data' =>'status', 'title' => 'Estado'],
            ['data' =>'latitude', 'title' => 'Latitud'],
            ['data' =>'longitude', 'title' => 'Longitud'],
            ['data' =>'created_at', 'title' => 'F. Creación'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'placesdatatable_' . time();
    }
}
