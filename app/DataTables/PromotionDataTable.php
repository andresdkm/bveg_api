<?php

namespace App\DataTables;

use App\Models\Promotion;
use Pimlie\DataTables\MongodbDataTable;
use Yajra\DataTables\Services\DataTable;

class PromotionDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new MongodbDataTable($query);

        return $dataTable->addColumn('action', 'promotions.datatables_actions')
	        ->editColumn( 'name', function ( $class ) {
		        $title = ( $class->name && $class->name['es'] )
			        ? $class->name['es']
			        : ' ';

		        return $title;
	        } )
	        ->addColumn( 'image_blade', 'promotions.image' )
	        ->rawColumns( [ 'image_blade', 'action' ] );
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Promotion $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Promotion $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false])
            ->parameters([
	            'language' => ['url' => url('/lang/Spanish.json')],
	            'dom'       => 'Bfrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    ['extend' => 'create', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'export', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'print', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reset', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reload', 'className' => 'btn btn-default btn-sm no-corner',],
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
	        [ 'data' => 'name', 'title' => 'Nombre' ],
	        [ 'data' => 'image_blade', 'title' => 'Imagen' ],
	        [ 'data' => 'created_at', 'title' => 'Fecha' ],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'promotionsdatatable_' . time();
    }
}
