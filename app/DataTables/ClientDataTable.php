<?php

namespace App\DataTables;

use App\Models\Client;
use Pimlie\DataTables\MongodbDataTable;
use Yajra\DataTables\Services\DataTable;

class ClientDataTable extends DataTable {
	/**
	 * Build DataTable class.
	 *
	 * @param mixed $query Results from query() method.
	 *
	 * @return \Yajra\DataTables\DataTableAbstract
	 */
	public function dataTable( $query ) {
		$dataTable = new MongodbDataTable( $query );

		return $dataTable->addColumn( 'action', 'clients.datatables_actions' )
		                 ->editColumn( 'phone', function ( $class ) {
			                 $phone = ( $class->phone )
				                 ?  $class->phone
				                 : '';
			                 return $phone;
		                 } )
		                 ->editColumn( 'lang', function ( $class ) {
			                 $lang= ( $class->lang )
				                 ? $class->lang
				                 : '';
			                 return $lang;
		                 } );
	}

	/**
	 * Get query source of dataTable.
	 *
	 * @param \App\Models\Client $model
	 *
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function query( Client $model ) {
		return $model->newQuery();
	}

	/**
	 * Optional method if you want to use html builder.
	 *
	 * @return \Yajra\DataTables\Html\Builder
	 */
	public function html() {
		return $this->builder()
		            ->columns( $this->getColumns() )
		            ->minifiedAjax()
		            ->addAction( [ 'width' => '120px', 'printable' => false ] )
		            ->parameters( [
			            'language'  => [ 'url' => url( '/lang/Spanish.json' ) ],
			            'dom'       => 'Bfrtip',
			            'stateSave' => true,
			            'order'     => [ [ 0, 'desc' ] ],
			            'buttons'   => [
				            [ 'extend' => 'export', 'className' => 'btn btn-default btn-sm no-corner', ],
				            [ 'extend' => 'print', 'className' => 'btn btn-default btn-sm no-corner', ],
				            [ 'extend' => 'reset', 'className' => 'btn btn-default btn-sm no-corner', ],
				            [ 'extend' => 'reload', 'className' => 'btn btn-default btn-sm no-corner', ],
			            ],
		            ] );
	}

	/**
	 * Get columns.
	 *
	 * @return array
	 */
	protected function getColumns() {
		return [
			[ 'data' => 'name', 'title' => 'Nombre' ],
			[ 'data' => 'email', 'title' => 'Correo' ],
			[ 'data' => 'phone', 'title' => 'Teléfono' ],
			[ 'data' => 'lang', 'title' => 'Idioma' ],

		];
	}

	/**
	 * Get filename for export.
	 *
	 * @return string
	 */
	protected function filename() {
		return 'clientsdatatable_' . time();
	}
}
