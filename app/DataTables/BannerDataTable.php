<?php

namespace App\DataTables;

use App\Models\Banner;
use Pimlie\DataTables\MongodbDataTable;
use Yajra\DataTables\Services\DataTable;

class BannerDataTable extends DataTable {
	/**
	 * Build DataTable class.
	 *
	 * @param mixed $query Results from query() method.
	 *
	 * @return \Yajra\DataTables\DataTableAbstract
	 */
	public function dataTable( $query ) {
		$dataTable = new MongodbDataTable( $query );

		return $dataTable->addColumn( 'action', 'banners.datatables_actions' )
		                 ->editColumn( 'title', function ( $class ) {
			                 $title = ( $class->title && $class->title['es'] )
				                 ? $class->title['es']
				                 : ' ';

			                 return $title;
		                 } )
		                 ->editColumn( 'subtitle', function ( $class ) {
			                 $title = ( $class->subtitle && $class->subtitle['es'] )
				                 ? $class->subtitle['es']
				                 : ' ';

			                 return $title;
		                 } )
		                 ->addColumn( 'image_blade', 'banners.image' )
		                 ->rawColumns( [ 'image_blade', 'action' ] );
	}

	/**
	 * Get query source of dataTable.
	 *
	 * @param \App\Models\Banner $model
	 *
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function query( Banner $model ) {
		return $model->newQuery();
	}

	/**
	 * Optional method if you want to use html builder.
	 *
	 * @return \Yajra\DataTables\Html\Builder
	 */
	public function html() {
		return $this->builder()
		            ->columns( $this->getColumns() )
		            ->minifiedAjax()
		            ->addAction( [ 'width' => '120px', 'printable' => false ] )
		            ->parameters( [
			            'language'  => [ 'url' => url( '/lang/Spanish.json' ) ],
			            'dom'       => 'Bfrtip',
			            'stateSave' => true,
			            'order'     => [ [ 0, 'desc' ] ],
			            'buttons'   => [
				            [ 'extend' => 'create', 'className' => 'btn btn-default btn-sm no-corner', ],
				            [ 'extend' => 'export', 'className' => 'btn btn-default btn-sm no-corner', ],
				            [ 'extend' => 'print', 'className' => 'btn btn-default btn-sm no-corner', ],
				            [ 'extend' => 'reset', 'className' => 'btn btn-default btn-sm no-corner', ],
				            [ 'extend' => 'reload', 'className' => 'btn btn-default btn-sm no-corner', ],
			            ],
		            ] );
	}

	/**
	 * Get columns.
	 *
	 * @return array
	 */
	protected function getColumns() {
		return [
			[ 'data' => 'title', 'title' => 'Titulo' ],
			[ 'data' => 'subtitle', 'title' => 'Subtitulo' ],
			[ 'data' => 'image_blade', 'title' => 'Imagen' ],
			[ 'data' => 'created_at', 'title' => 'Fecha' ],

		];
	}

	/**
	 * Get filename for export.
	 *
	 * @return string
	 */
	protected function filename() {
		return 'bannersdatatable_' . time();
	}
}
