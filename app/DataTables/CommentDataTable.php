<?php

namespace App\DataTables;

use App\Models\Comment;
use Pimlie\DataTables\MongodbDataTable;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class CommentDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new MongodbDataTable($query);

        return $dataTable->addColumn('action', 'comments.datatables_actions')
	        ->editColumn( 'status', function ( $class ) {
		        $status= ( $class->status=='published' )
			        ? 'Publicado'
			        : 'No publicado';
		        return $status;
	        } );
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Comment $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Comment $model)
    {
        return $model->with('client')->with('place')->newQuery()->orderBy('created_at','DESC');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false])
            ->parameters([
	            'language' => ['url' => url('/lang/Spanish.json')],
	            'dom'       => 'Bfrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    ['extend' => 'export', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'print', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reset', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reload', 'className' => 'btn btn-default btn-sm no-corner',],
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
	    return [
		    ['data' =>'rating', 'title' => 'Calificación'],
		    ['data' =>'description', 'title' => 'Descripción'],
		    ['data' =>'status', 'title' => 'Estado'],
		    ['data' =>'client.email', 'title' => 'Cliente'],
		    ['data' =>'place.name', 'title' => 'Establecimiento'],
		    ['data' =>'created_at', 'title' => 'Fecha de creación'],
	    ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'commentsdatatable_' . time();
    }
}
