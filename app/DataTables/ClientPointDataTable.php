<?php

namespace App\DataTables;

use App\Models\ClientPoint;
use Illuminate\Http\Request;
use Pimlie\DataTables\MongodbDataTable;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class ClientPointDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
	    $dataTable = new MongodbDataTable($query);

	    return $dataTable;
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\ClientPoint $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(ClientPoint $model)
    {
        return $model->where('place_id', $this->place_id)->with('client')->with('author')->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->parameters([
                'dom'       => 'Bfrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    ['extend' => 'export', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'print', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reset', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reload', 'className' => 'btn btn-default btn-sm no-corner',],
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
	    return [
		    ['data' =>'points', 'title' => 'Puntos'],
		    ['data' =>'client.name', 'title' => 'Cliente'],
		    ['data' =>'description', 'title' => 'Descripción'],
		    ['data' =>'author.name', 'title' => 'Autor'],
		    ['data' =>'created_at', 'title' => 'Fecha'],
	    ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'client_pointsdatatable_' . time();
    }
}
