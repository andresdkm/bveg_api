<?php

namespace App\Repositories;

use App\Models\ClientPoint;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ClientPointRepository
 * @package App\Repositories
 * @version November 17, 2019, 5:55 am UTC
 *
 * @method ClientPoint findWithoutFail($id, $columns = ['*'])
 * @method ClientPoint find($id, $columns = ['*'])
 * @method ClientPoint first($columns = ['*'])
*/
class ClientPointRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ClientPoint::class;
    }
}
