<?php

namespace App\Repositories;

use App\Models\Notify;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class NotifyRepository
 * @package App\Repositories
 * @version December 21, 2018, 9:06 pm UTC
 *
 * @method Notify findWithoutFail($id, $columns = ['*'])
 * @method Notify find($id, $columns = ['*'])
 * @method Notify first($columns = ['*'])
*/
class NotifyRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'message',
        'total',
        'date_add',
        'link',
        'poster_id',
        'price',
        'price_ofert',
        'title',
        'date_valid'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Notify::class;
    }
}
