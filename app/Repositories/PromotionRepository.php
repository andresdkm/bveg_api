<?php

namespace App\Repositories;

use App\Models\Promotion;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PromotionRepository
 * @package App\Repositories
 * @version August 7, 2019, 6:16 am UTC
 *
 * @method Promotion findWithoutFail($id, $columns = ['*'])
 * @method Promotion find($id, $columns = ['*'])
 * @method Promotion first($columns = ['*'])
*/
class PromotionRepository extends TranslateRepository
{

	protected $fieldTranslates = [
		'description',
		'name'
	];

    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Promotion::class;
    }
}
