<?php

namespace App\Repositories;

use App\Models\Category;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class CategoryRepository
 * @package App\Repositories
 * @version December 21, 2018, 9:05 pm UTC
 *
 * @method Category findWithoutFail($id, $columns = ['*'])
 * @method Category find($id, $columns = ['*'])
 * @method Category first($columns = ['*'])
*/
class CategoryRepository extends TranslateRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'status',
        'sort',
        'photo',
        'icon',
    ];

    protected $fieldTranslates = [
        'name'
    ];
    /**
     * Configure the Model
     **/
    public function model()
    {
        return Category::class;
    }
}
