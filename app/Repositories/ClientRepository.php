<?php

namespace App\Repositories;

use App\Models\Client;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ClientRepository
 * @package App\Repositories
 * @version July 29, 2019, 8:36 pm UTC
 *
 * @method Client findWithoutFail( $id, $columns = [ '*' ] )
 * @method Client find( $id, $columns = [ '*' ] )
 * @method Client first( $columns = [ '*' ] )
 */
class ClientRepository extends BaseRepository {
	/**
	 * @var array
	 */
	protected $fieldSearchable = [
		'code'
	];

	/**
	 * Configure the Model
	 **/
	public function model() {
		return Client::class;
	}

	public function create( array $attributes ) {
		$attributes['status'] = false;
		$attributes['verify'] = false;

		return parent::create( $attributes ); // TODO: Change the autogenerated stub
	}
}
