<?php

namespace App\Repositories;

use App\Models\Offer;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class OfferRepository
 * @package App\Repositories
 * @version February 12, 2019, 6:27 pm UTC
 *
 * @method Offer findWithoutFail($id, $columns = ['*'])
 * @method Offer find($id, $columns = ['*'])
 * @method Offer first($columns = ['*'])
*/
class OfferRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'message',
        'total',
        'date_add',
        'link',
        'poster_id',
        'price',
        'price_ofert',
        'title',
        'date_valid'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Offer::class;
    }
}
