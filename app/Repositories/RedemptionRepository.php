<?php

namespace App\Repositories;

use App\Models\Bonus;
use App\Models\Client;
use App\Models\ClientPoint;
use App\Models\Redemption;
use App\Util\BonusUtil;
use App\Util\PushUtil;
use Carbon\Carbon;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class RedemptionRepository
 * @package App\Repositories
 * @version July 15, 2019, 9:43 pm UTC
 *
 * @method Redemption findWithoutFail( $id, $columns = [ '*' ] )
 * @method Redemption find( $id, $columns = [ '*' ] )
 * @method Redemption first( $columns = [ '*' ] )
 */
class RedemptionRepository extends BaseRepository {
	/**
	 * @var array
	 */
	protected $fieldSearchable = [
		'user_id',
		'place_id',
		'points',
		'description',
		'author_id',
		'created_at',
		'updated_at'
	];

	/**
	 * Configure the Model
	 **/
	public function model() {
		return ClientPoint::class;
	}

	public function create( array $attributes ) {
		if ( $attributes['type'] == 'bonus' ) {
			$bonus = Bonus::where( 'code', '=', $attributes['code'] )->get()->first();
			if ( empty( $bonus ) ) {
				throw new \Exception( __( 'app.invalid_bonus' ) );
			}
			if ( $bonus->place_id != $attributes['place_id'] ) {
				throw new \Exception( __( 'app.bonus_invalid_place' ) );
			}

			if ( $bonus->status != BonusUtil::$state_available ) {
				throw new \Exception( __( 'app.bonus_used' ) );
			}

			if ( $bonus->type == 'product' ) {
				$bonus->quantity = $bonus->quantity - 1;
				if ( $bonus->quantity == 0 ) {
					$bonus->status = BonusUtil::$state_used;
				}
			} else {
				$bonus->status = BonusUtil::$state_used;
			}

			$bonus->movements()->create( [
				'type'      => BonusUtil::$movement_used,
				'client_id' => $attributes['author_id']
			] );
			$bonus->save();

			$pushUtil = new PushUtil();
			$pushUtil->redemptionBonus( $bonus->client_id, $bonus->place_id );

			return __( 'app.bonus_updated' );
		} else {

			$user = Client::where( 'code', '=', $attributes['code'] )->get()->first();

			if ( empty( $user ) ) {
				throw new \Exception( __( 'app.user_invalid' ) );
			}

			$attributes['client_id'] = $user->_id;

			$points = ClientPoint::where( 'client_id', $user->_id )
			                     ->where( 'place_id', $attributes['place_id'] )
			                     ->where( 'expiration_at', '>=', Carbon::now() )
			                     ->sum( 'points' );

			if ( empty( $points ) || $points < 1 ) {
				throw new \Exception( __( 'app.user_unavailable_points_place' ) );
			}

			if ( $points < $attributes['points'] ) {
				throw new \Exception( __( 'app.user_unavailable_points_amount' ) );
			}

			$data = parent::create( [
				'points'      => ( 0 - $attributes['points'] ),
				'client_id'   => $user->_id,
				'place_id'    => $attributes['place_id'],
				'description' => $attributes['description'],
				'author_id'   => $attributes['author_id'],
			] );

			return __( 'app.points_free', [
					'points' => ( - 1 * $data->points ),
					'email'  => $user->email
				]
			);

		}


	}
}
