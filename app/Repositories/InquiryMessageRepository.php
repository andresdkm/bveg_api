<?php

namespace App\Repositories;

use App\Models\InquiryMessage;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class InquiryMessageRepository
 * @package App\Repositories
 * @version August 21, 2019, 10:06 pm UTC
 *
 * @method InquiryMessage findWithoutFail($id, $columns = ['*'])
 * @method InquiryMessage find($id, $columns = ['*'])
 * @method InquiryMessage first($columns = ['*'])
*/
class InquiryMessageRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return InquiryMessage::class;
    }
}
