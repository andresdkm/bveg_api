<?php

namespace App\Repositories;

use App\Models\CmsImage;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class CmsImageRepository
 * @package App\Repositories
 * @version December 21, 2018, 9:07 pm UTC
 *
 * @method CmsImage findWithoutFail($id, $columns = ['*'])
 * @method CmsImage find($id, $columns = ['*'])
 * @method CmsImage first($columns = ['*'])
*/
class CmsImageRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'file',
        'width',
        'height',
        'created_at',
        'focal_point_x',
        'focal_point_y',
        'focal_point_width',
        'focal_point_height',
        'uploaded_by_user_id',
        'file_size',
        'collection_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CmsImage::class;
    }
}
