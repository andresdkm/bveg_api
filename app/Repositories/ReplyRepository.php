<?php

namespace App\Repositories;

use App\Models\Reply;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ReplyRepository
 * @package App\Repositories
 * @version August 21, 2019, 4:03 pm UTC
 *
 * @method Reply findWithoutFail($id, $columns = ['*'])
 * @method Reply find($id, $columns = ['*'])
 * @method Reply first($columns = ['*'])
*/
class ReplyRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Reply::class;
    }
}
