<?php


namespace App\Repositories;


use App\Util\TranslateUtil;
use Illuminate\Database\Eloquent\Builder;
use InfyOm\Generator\Common\BaseRepository;

abstract class TranslateRepository extends BaseRepository {

	/**
	 * @var array
	 */
	protected $fieldTranslates = [];

	public function create( array $attributes ) {
		// Have to skip presenter to get a model not some data
		$temporarySkipPresenter = $this->skipPresenter;
		$this->skipPresenter( true );
		$translates = [];
		foreach ( $this->fieldTranslates as $fieldTranslate ) {
			unset( $attributes[ $fieldTranslate ] );
			foreach ( TranslateUtil::$languagesAvailable as $lang ) {
				if ( isset( $attributes[ $fieldTranslate . '_' . $lang ] ) ) {
					if ( ! isset( $translates[ $fieldTranslate ] ) ) {
						$translates[ $fieldTranslate ] = [];
					}
					$translates[ $fieldTranslate ][ $lang ] = $attributes[ $fieldTranslate . '_' . $lang ];
					unset( $attributes[ $fieldTranslate . '_' . $lang ] );
				}
			}
		}
		$model = parent::create( $attributes );
		$this->skipPresenter( $temporarySkipPresenter );

		$model = $this->updateRelations( $model, $attributes );
		$model->withoutEvents( function () use ( $model ) {
			$model->save();
		} );
		foreach ( $translates as $key => $translate ) {
			$model->{$key} = $translate;
		}
		$model->save();

		return $this->parserResult( $model );
	}

	public function update( array $attributes, $id ) {
		// Have to skip presenter to get a model not some data
		$temporarySkipPresenter = $this->skipPresenter;
		$this->skipPresenter( true );
		$model = parent::update( $attributes, $id );
		$this->skipPresenter( $temporarySkipPresenter );
		$translates = [];
		foreach ( $this->fieldTranslates as $fieldTranslate ) {
			unset( $attributes[ $fieldTranslate ] );
			foreach ( TranslateUtil::$languagesAvailable as $lang ) {
				if ( isset( $attributes[ $fieldTranslate . '_' . $lang ] ) ) {
					if ( ! isset( $translates[ $fieldTranslate ] ) ) {
						$translates[ $fieldTranslate ] = [];
					}
					$translates[ $fieldTranslate ][ $lang ] = $attributes[ $fieldTranslate . '_' . $lang ];
					unset( $attributes[ $fieldTranslate . '_' . $lang ] );
				}
			}
		}
		$model = $this->updateRelations( $model, $attributes );
		$model->withoutEvents( function () use ( $model ) {
			$model->save();
		} );
		foreach ( $translates as $key => $translate ) {
			$model->{$key} = $translate;
		}
		$model->save();

		return $this->parserResult( $model );
	}

	public function all( $columns = [ '*' ] ) {
		$this->applyCriteria();
		$this->applyScope();

		if ( $this->model instanceof Builder ) {
			$results = $this->model->get( $columns );
		} else {
			$results = $this->model->all( $columns );
		}

		$this->resetModel();
		$this->resetScope();

		$lang = session( 'language', null );
		if ( $lang != null ) {
			foreach ( $results as $result ) {
				foreach ( $this->fieldTranslates as $fieldTranslate ) {
					if ( ! empty( $result->{$fieldTranslate} ) ) {
						if ( empty( $result->{$fieldTranslate}[ $lang ] ) ) {
							$lang = TranslateUtil::$languageDefault;
						}
						$result->{$fieldTranslate} = $result->{$fieldTranslate}[ $lang ];
					}
				}
			}
		}


		return $this->parserResult( $results );
	}

	public function paginate( $limit = null, $columns = [ '*' ], $method = "paginate" ) {
		$this->applyCriteria();
		$this->applyScope();
		$limit   = is_null( $limit ) ? config( 'repository.pagination.limit', 15 ) : $limit;
		$results = $this->model->{$method}( $limit, $columns );
		$results->appends( app( 'request' )->query() );
		$this->resetModel();
		$lang = session( 'language', null );
		if ( $lang != null ) {
			foreach ( $results as $result ) {
				foreach ( $this->fieldTranslates as $fieldTranslate ) {
					if ( ! empty( $result->{$fieldTranslate} ) ) {
						if ( empty( $result->{$fieldTranslate}[ $lang ] ) ) {
							$lang = TranslateUtil::$languageDefault;
						}
						$result->{$fieldTranslate} = $result->{$fieldTranslate}[ $lang ];
					}
				}
			}
		}

		return $this->parserResult( $results );
	}

	public function find( $id, $columns = [ '*' ] ) {
		$this->applyCriteria();
		$this->applyScope();
		$model = $this->model->findOrFail( $id, $columns );
		$this->resetModel();

		$lang = session( 'language', null );
		if ( $lang != null ) {
			foreach ( $this->fieldTranslates as $fieldTranslate ) {
				if ( ! empty( $model->{$fieldTranslate} ) ) {
					if ( empty( $model->{$fieldTranslate}[ $lang ] ) ) {
						$lang = TranslateUtil::$languageDefault;
					}
					$model->{$fieldTranslate} = $model->{$fieldTranslate}[ $lang ];
				}
			}
		}


		return $this->parserResult( $model );
	}
}