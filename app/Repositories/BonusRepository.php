<?php

namespace App\Repositories;

use App\Models\Bonus;
use App\Models\User;
use App\Util\BonusUtil;
use App\Util\NumberUtil;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class BonusRepository
 * @package App\Repositories
 * @version July 21, 2019, 8:58 pm UTC
 *
 * @method Bonus findWithoutFail($id, $columns = ['*'])
 * @method Bonus find($id, $columns = ['*'])
 * @method Bonus first($columns = ['*'])
 */
class BonusRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'code',
        'email',
        'place_id',
        'user_id',
        'author_id',
        'used_at',
        'amount',
        'active',
        'created_at',
        'updated_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Bonus::class;
    }

    public function create(array $attributes)
    {

        $attributes['code'] = NumberUtil::randomString('b', 20);
        $attributes['public_code'] = NumberUtil::randomString('s', 30);
        $attributes['status'] = BonusUtil::$state_available;
        $model = parent::create($attributes);
        $model->movements()->create([
            'type' => BonusUtil::$movement_created,
            'client_id' => $attributes['author_id']
        ]);

        return $model;
    }

    public function assign(array $attributes)
    {

        $bonus = Bonus::with('place')->where('public_code', '=', $attributes['code'])->get()->first();

        if (empty($bonus)) {
            throw new \Exception(__('app.invalid_bonus'));
        }

        if ($bonus->user_id != null) {
            throw new \Exception(__('app.bonus_assigned'));
        }

        $user = User::find($attributes['user_id']);

        if (empty($user)) {
            throw new \Exception(__('app.user_not_found'));
        }

        if ($user->email != $bonus->email) {
            throw new \Exception(__('app.bonus_user_invalid'));
        }

        $bonus->user_id = $user->id;

        $bonus->save();

        return $bonus;

    }
}
