<?php

namespace App\Repositories;

use App\Models\Banner;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class BannerRepository
 * @package App\Repositories
 * @version December 21, 2019, 7:46 pm UTC
 *
 * @method Banner findWithoutFail($id, $columns = ['*'])
 * @method Banner find($id, $columns = ['*'])
 * @method Banner first($columns = ['*'])
*/
class BannerRepository extends TranslateRepository {

	protected $fieldTranslates = [
		'description',
		'title',
		'subtitle'
	];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Banner::class;
    }
}
