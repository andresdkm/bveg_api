<?php

namespace App\Repositories;

use App\Models\Client;
use App\Models\ClientPoint;
use App\Models\Place;
use Carbon\Carbon;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class UserPointRepository
 * @package App\Repositories
 * @version July 9, 2019, 12:12 am UTC
 *
 * @method ClientPoint findWithoutFail($id, $columns = ['*'])
 * @method ClientPoint find($id, $columns = ['*'])
 * @method ClientPoint first($columns = ['*'])
 */
class UserPointRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'place_id',
        'points',
        'description',
        'created_at',
        'updated_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ClientPoint::class;
    }

    public function create(array $attributes)
    {

        $place = Place::find($attributes['place_id']);

        if (empty($place)) {
            throw new \Exception(__('app.place_denied'));
        }

        $client = Client::find($attributes['client_id']);

        if (empty($client)) {
            throw new \Exception(__('app.user_invalid'));
        }

        if ($place->configuration == null) {
            throw new \Exception(__('app.configuration_invalid'));
        }

        $points = intval($attributes['value'] / $place->configuration->loyalty_value);

        $attributes['points'] = $points;

        $months = $place->configuration->points_expiration ?? 12;

        $attributes['expiration_at'] = Carbon::now()->addMonth($months);

        return parent::create($attributes);


    }
}
