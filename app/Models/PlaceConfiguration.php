<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class PlaceConfiguration extends Eloquent
{

    public $table = 'configuration';

    public $timestamps = false;

    protected $primaryKey = '_id';

    public $fillable = [
        'loyalty_enable',
        'loyalty_value',
        'points_expiration'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'loyalty_enable' => 'boolean',
        'loyalty_value' => 'double',
        'points_expiration' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
    ];


}
