<?php

namespace App\Models;

use Eloquent as Model;

/**
 * @SWG\Definition(
 *      definition="Offer",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="message",
 *          description="message",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="total",
 *          description="total",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="link",
 *          description="link",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="poster_id",
 *          description="poster_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="price",
 *          description="price",
 *          type="number",
 *          format="float"
 *      ),
 *      @SWG\Property(
 *          property="price_ofert",
 *          description="price_ofert",
 *          type="number",
 *          format="float"
 *      ),
 *      @SWG\Property(
 *          property="title",
 *          description="title",
 *          type="string"
 *      )
 * )
 */
class Offer extends Model
{

    public $table = 'tb_notify';
    
    public $timestamps = false;

    protected $appends= ['photos'];

    public $fillable = [
        'message',
        'total',
        'date_add',
        'link',
        'poster_id',
        'price',
        'price_ofert',
        'title',
        'date_valid'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'message' => 'string',
        'total' => 'integer',
        'link' => 'string',
        'poster_id' => 'integer',
        'title' => 'string'
    ];


    public function getPhotosAttribute()
    {
        return CmsImage::where('id', $this->poster_id)->get();
    }

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

}
