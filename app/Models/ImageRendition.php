<?php

namespace App\Models;

use Eloquent as Model;

/**
 * @SWG\Definition(
 *      definition="ImageRendition",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="file",
 *          description="file",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="width",
 *          description="width",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="height",
 *          description="height",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="focal_point_key",
 *          description="focal_point_key",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="image_id",
 *          description="image_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="filter_spec",
 *          description="filter_spec",
 *          type="string"
 *      )
 * )
 */
class ImageRendition extends Model
{

    public $table = 'wagtailimages_rendition';
    
    public $timestamps = false;



    public $fillable = [
        'file',
        'width',
        'height',
        'focal_point_key',
        'image_id',
        'filter_spec'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'file' => 'string',
        'width' => 'integer',
        'height' => 'integer',
        'focal_point_key' => 'string',
        'image_id' => 'integer',
        'filter_spec' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function wagtailimagesImage()
    {
        return $this->belongsTo(\App\Models\WagtailimagesImage::class);
    }
}
