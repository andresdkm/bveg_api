<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

/**
 * @SWG\Definition(
 *      definition="Inquiry",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Inquiry extends Eloquent
{

    public static $open = 'open';
    public static $close = 'closed';


    public $collection = 'inquiries';


    public $fillable = [
        'client_id',
        'place_id',
        'description',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'client_id' => 'string',
        'place_id' => 'string',
        'description' => 'string',
        'status' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function messages()
    {
        return $this->embedsMany(InquiryMessage::class);
    }

    public function placeLite()
    {
        return $this->belongsTo(\App\Models\Place::class, 'place_id', '_id')->select('_id','name','image');
    }

    public function client()
    {
        return $this->belongsTo(\App\Models\Client::class, 'client_id', '_id')->select('_id','name','photo');
    }


}
