<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;


/**
 * @SWG\Definition(
 *      definition="Promotion",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Promotion extends Eloquent
{

	protected $primaryKey = '_id';

	public $collection = 'promotions';


    public $fillable = [
        'name',
	    'description',
	    'place_id',
	    'discount',
	    'amount',
	    'amount_offer',
	    'points',
	    'image',
	    'type'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

	public function place()
	{
		return $this->belongsTo(\App\Models\Place::class, 'place_id', '_id');
	}

	public function placeLite()
	{
		return $this->belongsTo(\App\Models\Place::class, 'place_id', '_id')->select('_id','name');
	}


}
