<?php

namespace App\Models;

use App\Scopes\ClassReservationScope;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

/**
 * @SWG\Definition(
 *      definition="Place",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="description",
 *          description="description",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="phone",
 *          description="phone",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="website",
 *          description="website",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="featured",
 *          description="featured",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="address",
 *          description="address",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="geolocation",
 *          description="geolocation",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="category_id",
 *          description="category_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="photo_1_id",
 *          description="photo_1_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="photo_2_id",
 *          description="photo_2_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="photo_3_id",
 *          description="photo_3_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="photo_4_id",
 *          description="photo_4_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="status",
 *          description="status",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="userapp_id",
 *          description="userapp_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="rating",
 *          description="rating",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="description_ca",
 *          description="description_ca",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="description_en",
 *          description="description_en",
 *          type="string"
 *      )
 * )
 */
class Place extends Eloquent
{

    protected $primaryKey = '_id';

    public $collection = 'places';

    public $fillable = [
        'name',
        'phone',
        'website',
        'address',
        'geolocation',
        'category_id',
        'photos',
        'status',
        'rating',
        'loyalty',
        'points_value',
        'distance',
	    'description',
	    'provider_id',
	    'image'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        '_id' => 'string',
        'name' => 'string',
        'phone' => 'string',
        'website' => 'string',
        'address' => 'string',
        'category_id' => 'string',
        'status' => 'boolean',
        'rating' => 'integer',
        'points_value' => 'double',
        'loyalty' => 'boolean',
	    'provider_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function category()
    {
        return $this->belongsTo(\App\Models\Category::class, 'category_id', '_id')->select(['_id', 'icon', 'name']);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function comments()
    {
        return $this->hasMany(\App\Models\Comment::class,'place_id','_id')->where('status','published')->with('client');
    }

    public function configuration()
    {
        return $this->embedsOne(PlaceConfiguration::class);
    }

}
