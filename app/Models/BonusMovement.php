<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class BonusMovement extends Eloquent
{

    public $table = 'movements';

    public $timestamps = true;

    protected $primaryKey = '_id';

    public $fillable = [
        'type',
        'client_id',
        'created_at',
        'updated_at',
        'public_code'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
    ];

    public function client()
    {
        return $this->belongsTo(\App\Models\Client::class, 'client_id', '_id');
    }


}
