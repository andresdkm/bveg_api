<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

/**
 * @SWG\Definition(
 *      definition="Comment",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Comment extends Eloquent
{

	protected $primaryKey = '_id';

	public $collection = 'comments';
    


    public $fillable = [
        'rating',
	    'description',
	    'status',
	    'place',
	    'client_id',
	    'place_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
	    'rating' => 'integer',
	    'description' => 'string',
	    'status' => 'string',
	    'client_id' => 'string',
	    'place_id' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function replies()
    {
        return $this->embedsMany(Reply::class)->with('client');
    }

    public function client()
    {
        return $this->belongsTo(\App\Models\Client::class, 'client_id', '_id')->select('_id','name','email','photo');
    }

    public function place()
    {
        return $this->belongsTo(\App\Models\Place::class, 'place_id', '_id');
    }

}
