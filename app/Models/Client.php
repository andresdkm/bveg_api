<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Jenssegers\Mongodb\Auth\User as Authenticatable;

/**
 * @SWG\Definition(
 *      definition="Client",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Client extends Authenticatable
{

    use Notifiable;

    public $collection = 'clients';

    protected $primaryKey = '_id';

    protected $hidden = ['password'];

    protected $appends = ['reviews'];

    public $fillable = [
        'name',
        'photo',
        'email',
        'verify',
        'country_id',
        'phone',
        'login_code',
        'verification_code',
        'lang',
        'token',
        'code',
        'geolocation',
        'password',
        'favorites',
        'role',
        'places_admin',
	    'os',
	    'fcm',
	    'lang',
	    'provider_id'

    ];



    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function comments()
    {
        return $this->hasMany(\App\Models\Comment::class, 'client_id', '_id')->with('place');
    }

    public function getReviewsAttribute()
    {
        return Comment::where('client_id',$this->id)->count();
    }

    public function country()
    {
        return $this->hasOne(\App\Models\Country::class,'_id','country_id');
    }


}
