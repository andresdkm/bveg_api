<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

/**
 * @SWG\Definition(
 *      definition="Bonus",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="code",
 *          description="code",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="email",
 *          description="email",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="place_id",
 *          description="place_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="user_id",
 *          description="user_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="author_id",
 *          description="author_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="used_at",
 *          description="used_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="amount",
 *          description="amount",
 *          type="float",
 *          format="float"
 *      ),
 *      @SWG\Property(
 *          property="active",
 *          description="active",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Bonus extends Eloquent
{

    public $collection = 'bonuses';

    public $timestamps = true;

    protected $primaryKey = '_id';

    public $fillable = [
        'code',
        'place_id',
        'author_id',
        'public_code',
        'description',
        'client_id',
        'quantity',
        'type',
        'amount',
        'status'

    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'code' => 'string',
        'place_id' => 'string',
        'client_id' => 'string',
        'author_id' => 'string',
        'status' => 'string',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id' => 'required',
        'code' => 'required',
        'place_id' => 'required',
        'amount' => 'required',
        'active' => 'required'
    ];

    public function movements()
    {
        return $this->embedsMany(BonusMovement::class);
    }

	public function client()
	{
		return $this->belongsTo(\App\Models\Client::class);
	}

    public function author()
    {
        return $this->belongsTo(\App\Models\Client::class);
    }

    public function place()
    {
        return $this->belongsTo(\App\Models\Place::class);
    }

    public function placeLite()
    {
        return $this->belongsTo(\App\Models\Place::class, 'place_id', '_id')->select('_id','name','image');
    }

}
