<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

/**
 * @SWG\Definition(
 *      definition="ClientPoint",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="user_id",
 *          description="user_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="place_id",
 *          description="place_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="points",
 *          description="points",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="description",
 *          description="description",
 *          type="string"
 *      )
 * )
 */
class ClientPoint extends Eloquent
{

    public $collection = 'client_points';

    public $timestamps = true;

    public $fillable = [
        'client_id',
        'place_id',
        'author_id',
        'description',
        'points',
        'expiration_at',
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'client_id' => 'string',
        'place_id' => 'string',
        'points' => 'integer',
        'description' => 'string',
        'expiration_at' => 'datetime'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/

	public function client()
	{
		return $this->belongsTo(\App\Models\Client::class);
	}

	public function author()
	{
		return $this->belongsTo(\App\Models\Client::class);
	}

    public function place()
    {
        return $this->belongsTo(\App\Models\Place::class);
    }

	public function placeLite()
	{
		return $this->belongsTo(\App\Models\Place::class, 'place_id', '_id')->select('_id','name','image');
	}

}
