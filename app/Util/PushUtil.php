<?php


namespace App\Util;


use App\Models\Client;
use App\Models\Place;
use Edujugon\PushNotification\PushNotification;
use Illuminate\Support\Facades\Log;

class PushUtil {


	public function sendBonus( $clientId, $bonusId ) {
		$client  = Client::find( $clientId );
		$place   = Place::find( $bonusId );
		$title   = __( 'app.bonus.title', [], $client->lang );
		$message = __( 'app.bonus.message', [ 'name' => $place->name ], $client->lang );
		$this->push( $client->token, $client->os, $title, $message, null );

	}

	public function redemptionBonus( $clientId, $bonusId ) {
		$client  = Client::find( $clientId );
		$place   = Place::find( $bonusId );
		$title   = __( 'app.bonus.redemption.title', [], $client->lang );
		$message = __( 'app.bonus.redemption.message', [ 'name' => $place->name ], $client->lang );
		$this->push( $client->token, $client->os, $title, $message, null );

	}

	public function answerInquiry( $clientId, $bonusId ) {
		$client  = Client::find( $clientId );
		$place   = Place::find( $bonusId );
		$title   = __( 'inquiry.answer.client.title', [], $client->lang );
		$message = __( 'inquiry.answer.client.message', [ 'name' => $place->name ], $client->lang );
		$this->push( $client->token, $client->os, $title, $message, null );

	}

	public function sendPoints( $clientId, $placeId, $points ) {
		$client = Client::find( $clientId );
		$place  = Place::find( $placeId );
		if ( $points > 0 ) {
			$title   = __( 'app.points.title', [], $client->lang );
			$message = __( 'app.points.message', [ 'name' => $place->name, 'points' => $points ], $client->lang );
		} else {
			$title   = __( 'app.points.redemption.title', [], $client->lang );
			$message = __( 'app.points.redemption.message', [
				'name'   => $place->name,
				'points' => abs( $points )
			], $client->lang );
		}

		$this->push( $client->token, $client->os, $title, $message, null );

	}

	private function push( $token, $os, $title, $message, $params ) {
		$push = new PushNotification( 'fcm' );
		switch ( $os ) {
			case 'Android':
				$result = $push->setMessage( [
					'data' => [
						'title' => $title,
						'body'  => $message,
						'sound' => 'default',
						'extra' => $params
					]
				] )->setDevicesToken( [ $token ] )->send();
				Log::debug( 'Push result Android', [ 'response' => $push->getFeedback() ] );
				break;
		}

	}

}