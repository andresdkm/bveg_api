<?php


namespace App\Util;


class TranslateUtil
{

    public static $languagesAvailable = [
        'ca',
        'en',
        'es'
    ];

    public static  $languageDefault = 'es';


    public static function makeTranslation(&$model, $field)
    {
        $lang = session('language', TranslateUtil::$languageDefault);
        if (isset($model->{$field}[$lang])) {
            $model->{$field} = $model->{$field}[$lang];
        }

    }
}