<?php


namespace App\Util;


class GeoUtil {

	public static function distance( $point1_lat, $point1_long, $point2_lat, $point2_long, $decimals = 2 ) {
		$degrees = rad2deg( acos( ( sin( deg2rad( $point1_lat ) ) * sin( deg2rad( $point2_lat ) ) ) + ( cos( deg2rad( $point1_lat ) ) * cos( deg2rad( $point2_lat ) ) * cos( deg2rad( $point1_long - $point2_long ) ) ) ) );

		$distance = $degrees * 111.13384;

		return round( $distance, $decimals );
	}

}