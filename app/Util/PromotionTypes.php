<?php


namespace App\Util;


class PromotionTypes {

	public static $discount = 'discount';

	public static $amount = 'amount';

	public static $points = 'points';

	public static $product = 'product';


	public static function types() {
		return [
			PromotionTypes::$discount => 'Descuento',
			PromotionTypes::$amount   => 'Valor',
			PromotionTypes::$points   => 'Puntos',
			PromotionTypes::$product  => 'Producto
			',

		];
	}

	public static function status() {
		return [
			true  => 'Activo',
			false => 'Inactivo',
		];
	}
}