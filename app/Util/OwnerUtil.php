<?php


namespace App\Util;


use App\Models\Client;
use Illuminate\Http\Request;

class OwnerUtil
{

    public static function getAuthorize(Request $request)
    {
        $authorId = $request->get('author_id');
        $placeId = $request->get('place_id');
        $client = Client::find($authorId);
        if (empty($client)) {
            return false;
        }
        if (!isset($client->places_admin)) {
            return false;
        }
        return in_array($placeId, $client->places_admin);
    }
}