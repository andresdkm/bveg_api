<?php


namespace App\Util;


class NumberUtil {
	public static function randomNumber() {
		return rand( 1111, 9999 );
	}

	public static function randomString( $prefix, $length = 15 ) {
		$characters       = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen( $characters );
		$randomString     = '';
		for ( $i = 0; $i < $length; $i ++ ) {
			$randomString .= $characters[ rand( 0, $charactersLength - 1 ) ];
		}

		return $prefix . '-' . $randomString;
	}

}