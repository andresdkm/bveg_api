<?php


namespace App\Util;


use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class FileImageUtil {

	public static function uploadFile( Request $request, string $fileName, string $name, array $input ) {
		if ( $request->hasFile( $fileName ) ) {
			$file = $request->file( $fileName );

			$extension = $file->getClientOriginalExtension();
			$now       = new Carbon();
			$path      = $now->timestamp . '.' . $extension;
			Storage::put( $path, File::get( $file ) );

			$input[ $name ] = $path;
		}

		return $input;
	}

	public static function simpleUploadFile( $file ) {
		$extension = $file->getClientOriginalExtension();
		$now       = new Carbon();
		$path      = $now->timestamp . '.' . $extension;
		Storage::put( $path, File::get( $file ) );

		return $path;
	}
}