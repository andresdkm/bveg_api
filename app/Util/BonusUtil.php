<?php


namespace App\Util;


class BonusUtil
{

    public static $amount = 'amount';

    public static $product = 'product';

    public static $state_available = 'available';

    public static $state_canceled = 'canceled';

    public static $state_used = 'used';

    public static $movement_created = 'created';

    public static $movement_used = 'used';

    public static $movement_canceled = 'canceled';

    public static function types()
    {
        return [
            BonusUtil::$amount => 'app.bonus.type.amount',
            BonusUtil::$product => 'app.bonus.type.product',
        ];
    }
}