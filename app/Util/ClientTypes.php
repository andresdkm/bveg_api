<?php


namespace App\Util;


class ClientTypes {

	public static $none = 'none';

	public static $owner = 'owner';



	public static function types() {
		return [
            ClientTypes::$none => 'Cliente',
            ClientTypes::$owner   => 'Propietario',
		];
	}
}