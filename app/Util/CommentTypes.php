<?php


namespace App\Util;


class CommentTypes {

	public static $published = 'published';

	public static $unpublished = 'unpublished';



	public static function status() {
		return [
			CommentTypes::$published => 'Publicado',
			CommentTypes::$unpublished   => 'No publicado',
		];
	}
}