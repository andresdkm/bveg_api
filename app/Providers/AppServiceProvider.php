<?php

namespace App\Providers;

use App\Models\Bonus;
use App\Models\ClientPoint;
use App\Observers\BonusObserver;
use App\Observers\ClientPointObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider {
	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot() {
		Bonus::observe( BonusObserver::class );
		ClientPoint::observe( ClientPointObserver::class );
	}

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register() {
		//
	}
}
