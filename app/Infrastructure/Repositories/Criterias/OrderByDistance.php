<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 25/03/16
 * Time: 11:18 AM
 */

namespace App\Infrastructure\Repositories\Criterias;


use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class OrderByDistance implements CriteriaInterface
{
    private $latitude, $longitude;

    //
    public function __construct($latitude, $longitude)
    {
        $this->latitude = $latitude;
        $this->longitude = $longitude;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->where('geolocation', '$near', [
                '$geometry' => [
                    'type' => 'Point',
                    'coordinates' => [
                        floatval($this->longitude),
                        floatval($this->latitude),
                    ],
                ],
                '$distanceField' => "dist.calculated",
                '$includeLocs' => "geolocation",
                '$spherical' => true
            ]
        );
    }
}