<?php


namespace App\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Support\Facades\Auth;

class PlaceScope implements Scope
{

    public function apply(Builder $builder, Model $model)
    {
        if (Auth::guard('web')->check()) {
            $user = Auth::guard('web')->user();
            if ($user->role != 'admin') {
                $builder->where('userapp_id', '=', $user->id);
            }
        }
    }

}