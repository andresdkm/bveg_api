<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreatePlacesCollection extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('places', function ($collection) {
            $collection->index('category_id');
            $collection->geospatial('geolocation', '2dsphere');
            $collection->index(['name' => 'text', 'description.es' => 'text', 'description.en' => 'text', 'description.ca' => 'text','phone' => 'text','address' => 'text'],'full_text');
            $collection->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('places');
    }
}
