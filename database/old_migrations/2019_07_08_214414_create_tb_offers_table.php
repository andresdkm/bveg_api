<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_offers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title',300);
            $table->string('description',300);
            $table->decimal('price',10,2)->nullable();
            $table->decimal('price_offer',10,2)->nullable();
            $table->integer('discount')->nullable();
            $table->string('image',300);
            $table->date('start_at')->nullable();
            $table->date('end_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_offers');
    }
}
