<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTbPlacesTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table( 'tb_place', function ( Blueprint $table ) {
			$table->boolean( 'loyalty' )->default( false );
			$table->decimal( 'points_value', 10, 2 )->nullable();
			$table->string( 'code', 300 )->nullable();
			$table->timestamps();
		} );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table( 'tb_place', function ( Blueprint $table ) {
			$table->dropColumn( 'loyalty' );
			$table->dropColumn( 'points_value' );
			$table->dropColumn( 'code' );
			$table->dropColumn( 'created_at' );
			$table->dropColumn( 'updated_at' );

		} );
	}
}
