<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_user_points', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->index('fk_tb_user_points_tb_user_app');
            $table->integer('place_id')->index('fk_tb_user_points_tb_place');
            $table->foreign('user_id', 'fk_tb_user_points_tb_user_app')->references('id')->on('tb_user_app')->onUpdate('NO ACTION')->onDelete('NO ACTION');
	        $table->integer('author_id')->nullable()->index('fk_tb_bonus_author_tb_user_app');
	        $table->foreign('place_id', 'fk_tb_user_points_tb_place')->references('id')->on('tb_place')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->integer('points');
            $table->string('description',300);
            $table->dateTime('expiration_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_user_points');
    }
}
