<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBonusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_bonus', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code',300);
	        $table->string('public_code',300);
	        $table->string('email',300)->nullable();
	        $table->integer('place_id')->index('fk_tb_bonus_tb_places');
	        $table->integer('user_id')->nullable()->index('fk_tb_bonus_tb_user_app');
	        $table->integer('author_id')->nullable()->index('fk_tb_bonus_author_tb_user_app');
	        $table->boolean('active')->default(true);
	        $table->dateTime('used_at')->nullable();
	        $table->decimal('amount',10,2);
	        $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_bonus');
    }
}
