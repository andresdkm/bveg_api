<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTbUserAppTable1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tb_user_app', function (Blueprint $table) {
            $table->string('remember_token',100)->nullable();
            $table->enum('role', ['admin', 'owner', 'user'])->default('user');
            $table->string('country_code', 5)->nullable();
            $table->string('email', 100)->nullable();
            $table->string('phone', 12)->nullable();
	        $table->string('old_password', 200)->nullable();
            $table->string('code',300)->nullable();
	        $table->string('verification_code',5)->nullable();
	        $table->string('login_code',5)->nullable();
	        $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tb_user_app', function (Blueprint $table) {
            $table->dropColumn('remember_token');
            $table->dropColumn('role');
            $table->dropColumn('email');
            $table->dropColumn('country_code');
            $table->dropColumn('phone');
            $table->dropColumn('old_password');
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
	        $table->dropColumn('code');
	        $table->dropColumn('verification_code');
	        $table->dropColumn('login_code');

        });
    }
}
