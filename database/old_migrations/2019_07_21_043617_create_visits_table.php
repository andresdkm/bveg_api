<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_visits', function (Blueprint $table) {
            $table->increments('id');
	        $table->integer('place_id')->index('fk_tb_visits_tb_places');
	        $table->integer('user_id')->nullable()->index('fk_tb_visits_tb_user_app');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_visits');
    }
}
