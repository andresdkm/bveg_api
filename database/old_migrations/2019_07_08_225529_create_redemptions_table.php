<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRedemptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_promotions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title',100);
            $table->string('description',300);
            $table->string('image',300);
            $table->decimal('price',10,2)->nullable();
            $table->date('start_at')->nullable();
            $table->date('end_at')->nullable();
            $table->integer('points');
            $table->integer('place_id')->index('fk_tb_redemptions_tb_places');
            $table->foreign('place_id', 'fk_tb_redemptions_tb_places')->references('id')->on('tb_place')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_promotions');
    }
}
