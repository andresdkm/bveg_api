<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new \App\Models\User();
        $user->name = 'Andres Fabian Diaz';
        $user->email = 'andresdkm@gmail.com';
        $user->role = 'administrator';
        $user->password = bcrypt('20659914');
        $user->save();
    }
}
