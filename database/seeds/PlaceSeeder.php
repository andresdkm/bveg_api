<?php

use Illuminate\Database\Seeder;

class PlaceSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run( \App\Repositories\PlaceRepository $placeRepository ) {
		$data = DB::collection( 'places' )->select( 'provider_id' )->orderBy( 'provider_id', 'desc' )->first();
		if ( $data != null ) {
			$lastProviderId = $data['provider_id'];
		} else {
			$lastProviderId = 0;
		}
		echo "The last provider for places id is " . $lastProviderId;
		for ( $i = 1; $i <= 6; $i ++ ) {
			$oldCateogry = Collect( DB::connection( 'mysql' )->select( 'select * from tb_category where sort= ?', [ $i ] ) )->first();
			$places      = DB::connection( 'mysql' )->select( 'select * from tb_place where category_id= ? and id > ? ORDER BY id DESC', [
				$oldCateogry->id,
				$lastProviderId
			] );
			$category    = \App\Models\Category::where( 'sort', '=', $i )->get()->first();

			foreach ( $places as $place ) {
				$geo    = explode( ',', $place->geolocation );
				$images = [];
				if ( $place->photo_1_id != null ) {
					$image    = Collect( DB::connection( 'mysql' )->select( 'SELECT * FROM wagtailimages_image WHERE `id` = ?', [ $place->photo_1_id ] ) )->first();
					$images[] = str_replace( 'original_images/', '', $image->file );
				}
				if ( $place->photo_2_id != null ) {
					$image    = Collect( DB::connection( 'mysql' )->select( 'SELECT * FROM wagtailimages_image WHERE `id` = ?', [ $place->photo_2_id ] ) )->first();
					$images[] = str_replace( 'original_images/', '', $image->file );
				}
				if ( $place->photo_3_id != null ) {
					$image    = Collect( DB::connection( 'mysql' )->select( 'SELECT * FROM wagtailimages_image WHERE `id` = ?', [ $place->photo_3_id ] ) )->first();
					$images[] = str_replace( 'original_images/', '', $image->file );
				}
				if ( $place->photo_4_id != null ) {
					$image    = Collect( DB::connection( 'mysql' )->select( 'SELECT * FROM wagtailimages_image WHERE `id` = ?', [ $place->photo_4_id ] ) )->first();
					$images[] = str_replace( 'original_images/', '', $image->file );
				}
				try {

					\App\Models\Place::create( [
						'name'        => $place->name,
						'description' => [
							'es' => $place->description,
							'ca' => $place->description_ca,
							'en' => $place->description_en
						],
						'phone'       => $place->phone,
						'website'     => $place->website,
						'address'     => $place->address,
						'geolocation' => [ 'type' => 'Point', 'coordinates' => [ (double) $geo[1], (double) $geo[0] ] ],
						'category_id' => $category->_id,
						'rating'      => $place->rating,
						'image'       => $images[0],
						'photos'      => $images,
						'status'      => true,
						'provider_id' => $place->id
					] );
				} catch ( Exception $exception ) {
					\Illuminate\Support\Facades\Log::debug( "error with site", [
						'exception' => $exception,
						'place'     => $place
					] );
				}

			}
		}


	}
}
