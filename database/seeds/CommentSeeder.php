<?php

use Illuminate\Database\Seeder;

class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    $data = DB::collection( 'comments' )->select( 'provider_id' )->orderBy( 'provider_id', 'desc' )->first();
	    if ( $data != null ) {
		    $lastProviderId = $data['provider_id'];
	    } else {
		    $lastProviderId = 0;
	    }
	    echo "The last provider for comments id is ".$lastProviderId;
        $comments = DB::connection('mysql')->select('SELECT c.id, c.calificate,c.description,p.name,p.id as place_id , u.correo, u.id as client_id FROM `tb_comment` c INNER JOIN tb_place p ON (c.place_id = p.id) INNER JOIN tb_user_app u ON (c.userapp_id = u.id) WHERE c.id > ? ORDER BY c.id DESC',[$lastProviderId]);
        foreach ($comments as $item){
            $place = \App\Models\Place::where('provider_id','=',$item->place_id)->get()->first();
            $client = \App\Models\Client::where('email','=',$item->correo)->get()->first();
            if(!empty($place) && !empty($client)){
                $comment = new \App\Models\Comment();
                $comment->rating = $item->calificate;
                $comment->description = $item->description;
                $comment->status = 'published';
                $comment->place_id = $place->_id;
                $comment->client_id = $client->_id;
	            $comment->provider_id = $item->id;
                $comment->save();
            }else{
                \Illuminate\Support\Facades\Log::debug("nos e puede guardar este comentario", ['data' => $item,'place' => $place, 'client' => $client]);
            }

        }
    }
}
