<?php

use Illuminate\Database\Seeder;

class ClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    $data = DB::collection( 'clients' )->select( 'provider_id' )->orderBy( 'provider_id', 'desc' )->first();
	    if ( $data != null ) {
		    $lastProviderId = $data['provider_id'];
	    } else {
		    $lastProviderId = 0;
	    }
	    echo "The last provider for clients id is ".$lastProviderId;

        $clients = DB::connection('mysql')->select('select * from tb_user_app where id > ? ORDER BY id DESC',[$lastProviderId]);
        foreach ($clients as $item) {
            $client = new \App\Models\Client();
            $client->name = $item->name;
            $client->status = true;
            $client->email = $item->correo;
            $client->token = $item->dispositive;
            $client->role = 'none';
	        $client->provider_id = $item->id;
            if ($item->photo) {
                $client->photo = $item->photo;
            } else {
                $client->photo = 'default.png';
            }
            try {
                $client->save();
            } catch (Exception $exception) {
                \Illuminate\Support\Facades\Log::debug("error with site", ['data' => $client]);
            }
        }


    }
}
