<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

	    //$this->call(CategorySeeder::class);
        $this->call(PlaceSeeder::class);
        $this->call(ClientSeeder::class);
        $this->call(CommentSeeder::class);
        //$this->call(UsersSeeder::class);
        //$this->call(CountrySeeder::class);
    }
}
