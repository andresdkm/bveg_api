<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(\App\Repositories\CategoryRepository $categoryRepository)
    {
        $categoryRepository->create([
            'name_es' => 'VEGANOS',
            'name_ca' => 'VEGANS',
            'name_en' => 'VEGAN',
            'status' => true,
            'sort' => 1,
            'icon' => '1.png',
            'photo' => 'veganos.jpg'
        ]);
        $categoryRepository->create([
            'name_es' => 'VEGETARIANOS',
            'name_ca' => 'VEGETARIANS',
            'name_en' => 'VEGETARIAN',
            'status' => true,
            'sort' => 2,
            'icon' => '2.png',
            'photo' => 'vegetarianos.jpg'
        ]);
        $categoryRepository->create([
            'name_es' => 'VEG-FRIENDLY',
            'name_ca' => 'VEG-FRIENDLY',
            'name_en' => 'VEG-FRIENDLY',
            'status' => true,
            'sort' => 3,
            'icon' => '3.png',
            'photo' => 'veg-friendy.jpg'
        ]);
        $categoryRepository->create([
            'name_es' => 'PASTELERIAS-PANADERIAS-HELADERIAS',
            'name_ca' => 'PASTISSERIES-FORNS DE PA-GELATERIES',
            'name_en' => 'CAKES-BREAD-ICECREAM',
            'status' => true,
            'sort' => 4,
            'icon' => '4.png',
            'photo' => 'mas-cosas.jpg'
        ]);
        $categoryRepository->create([
            'name_es' => 'TIENDAS',
            'name_ca' => 'BOTIGUES',
            'name_en' => 'SHOPS',
            'status' => true,
            'sort' => 5,
            'icon' => '5.png',
            'photo' => 'tiendas.jpg'
        ]);
        $categoryRepository->create([
            'name_es' => 'PELUQUERIAS&ESTETICA',
            'name_ca' => 'PERRUQUERIES&ESTETICA',
            'name_en' => 'HAIRDRESSING&AESTHETICS',
            'status' => true,
            'sort' => 6,
            'icon' => '6.png',
            'photo' => 'Peluqueria.jpg'
        ]);

    }
}
