<?php

use Illuminate\Database\Seeder;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(\App\Repositories\CountryRepository $countryRepository)
    {
        $countryRepository->create([
            'name_es' => 'España',
            'name_ca' => 'Espanya',
            'name_en' => 'Spain',
            'country_code' => '+34',
            'image' => 'es.png',
            'enable' => true,
	        'iso_code' => 'ES'
        ]);

        $countryRepository->create([
            'name_es' => 'Colombia',
            'name_ca' => 'Colòmbia',
            'name_en' => 'Colombia',
            'country_code' => '+57',
            'image' => 'co.png',
            'enable' => true,
            'iso_code' => 'CO'
        ]);
    }
}
