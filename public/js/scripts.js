$('.btn-expand-collapse').click(function (e) {
    $('.navbar-primary').toggleClass('collapsed');
});

$(document).ready(function () {
    if ($('#gradeTable').length) {
        if (!$.fn.dataTable.isDataTable('#gradeTable')) {
            var id = $('#gradeTable').attr('school');
            $('#gradeTable').DataTable({
                language: {'url': '/lang/Spanish.json'},
                dom: 'Bfrtip',
                processing: true,
                serverSide: true,
                order: [[0, 'desc']],
                buttons: [
                    'reload'
                ],
                ajax: '/api/schools/' + id + '/grades/table',
                columns: [
                    {data: 'id', name: 'id', title: '#'},
                    {data: 'name', name: 'name', title: 'Nombre'},
                    {data: 'code', name: 'code', title: 'Codigo'},
                    {
                        "defaultContent": "",
                        "data": "action",
                        "name": "action",
                        "title": "",
                        "render": null,
                        "orderable": false,
                        "searchable": false, "width": "120px"
                    }
                ],
            });
        }
    }
    if ($('#gradeTable').length) {

        if (!$.fn.dataTable.isDataTable('#travelTable')) {
            var id = $('#travelTable').attr('school');
            $('#travelTable').DataTable({
                language: {'url': '/lang/Spanish.json'},
                dom: 'Bfrtip',
                processing: true,
                serverSide: true,
                order: [[0, 'desc']],
                buttons: [
                    'reload'
                ],
                ajax: '/api/schools/' + id + '/travels/table',
                columns: [
                    {data: 'id', title: '#'},
                    {data: 'grade.name', title: 'Grado'},
                    {data: 'travel.name', title: 'Destino'},
                    {data: 'travel.value', title: 'Valor'},
                    {
                        "defaultContent": "",
                        "data": "action",
                        "name": "action",
                        "title": "",
                        "render": null,
                        "orderable": false,
                        "searchable": false, "width": "120px"
                    }
                ],
            });
        }
    }

    $('#school_id').on('change', function () {
        var value = this.value;
        console.log(value);
        if (value) {
            $.get("/api/schools/" + value + "/grades",
                function (response) {
                    var sel = $("#grade_id");
                    sel.empty();
                    sel.append('<option value="" selected hidden>Seleccione...</option>');
                    for (var i = 0; i < response.data.length; i++) {
                        sel.append('<option value="' + response.data[i].id + '">' + response.data[i].name + '</option>');
                    }
                }, "json");
        }
    });
    $('#grade_id').on('change', function () {
        var value = this.value;
        console.log(value);
        if (value) {
            $.get("/api/grades/" + value + "/travels?userId=" + window.user,
                function (response) {
                    var sel = $("#travel_id");
                    sel.empty();
                    sel.append('<option value="" selected hidden>Seleccione...</option>');
                    for (var i = 0; i < response.data.length; i++) {
                        sel.append('<option value="' + response.data[i].id + '">' + response.data[i].name + '</option>');
                    }
                }, "json");
        }
    });

    $(".duesSelector").bind("change paste keyup", function () {
        var newValue = $(this).val();
        if (newValue > 0) {
            $("#duesTravelTable").show();
            $(".duesTbody").empty();
            for (var i = 1; i <= newValue; i++) {
                $(".duesTbody").append('<tr>\n' +
                    '                <td>\n' +
                    '                    <input type="text" name="due_' + i + '" value="' + i + '" disabled="" class="form-control">\n' +
                    '                </td>\n' +
                    '                <td>\n' +
                    '                    <input type="text" name="due_value_' + i + '"  required class="form-control currency">\n' +
                    '                </td>\n' +
                    '<td>\n' +
                    '                    <input type="date" name="due_date_' + i + '"  required class="form-control">\n' +
                    '                </td>\n' +
                    '                <td>\n' +
                    '                    <input type="text"  name="penalty_fee_' + i + '"   class="form-control currency">\n' +
                    '                </td>\n' +
                    '\n' +
                    '            </tr>');
                $('.currency').inputmask("numeric", {
                    radixPoint: ".",
                    groupSeparator: ",",
                    digits: 2,
                    autoGroup: true,
                    prefix: '$', //No Space, this will truncate the first character
                    rightAlign: false,
                    removeMaskOnSubmit: true,
                    oncleared: function () {
                        self.Value('');
                    }
                });

            }
        } else {
            $("#duesTravelTable").hide();
        }

    });

});

jQuery(document).ready(function ($) {

    // Disable scroll when focused on a number input.
    $('form').on('focus', 'input[type=number]', function (e) {
        $(this).on('wheel', function (e) {
            e.preventDefault();
        });
    });

    // Restore scroll on number inputs.
    $('form').on('blur', 'input[type=number]', function (e) {
        $(this).off('wheel');
    });

    // Disable up and down keys.
    $('form').on('keydown', 'input[type=number]', function (e) {
        if (e.which == 38 || e.which == 40)
            e.preventDefault();
    });

    if (window.studentGender) {
        $('#genderSelect option[value=' + window.studentGender + ']').attr('selected', 'selected');
    }
    if (window.studentDocumentType) {
        $('#documentTypeSelect option[value=' + window.studentDocumentType + ']').attr('selected', 'selected');
    }
});

$(function () {
    $("[data-toggle='tooltip']").tooltip();
});

$('#ya-viajo').on('hidden.bs.modal', function (e) {
    location.href = "/newTravel/create";
})

$('.currency').inputmask("numeric", {
    radixPoint: ".",
    groupSeparator: ",",
    digits: 2,
    autoGroup: true,
    prefix: '$', //No Space, this will truncate the first character
    rightAlign: false,
    removeMaskOnSubmit: true,
    oncleared: function () {
        self.Value('');
    }
});

$('form').submit(function (e) {
    var isOk = true;
    $('input[type=file]').each(function () {
        if (typeof this.files[0] !== 'undefined') {
            var maxSize = parseInt(3000000, 10),
                size = this.files[0].size;
            console.log(size,maxSize);
            isOk = maxSize > size;
            if (!isOk) {
                e.stopPropagation();
                e.preventDefault();
                alert("Los achivos superan el limite de 3MB");
            }
        }
    });

});