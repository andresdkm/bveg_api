<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CmsImageApiTest extends TestCase
{
    use MakeCmsImageTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateCmsImage()
    {
        $cmsImage = $this->fakeCmsImageData();
        $this->json('POST', '/api/v1/cmsImages', $cmsImage);

        $this->assertApiResponse($cmsImage);
    }

    /**
     * @test
     */
    public function testReadCmsImage()
    {
        $cmsImage = $this->makeCmsImage();
        $this->json('GET', '/api/v1/cmsImages/'.$cmsImage->id);

        $this->assertApiResponse($cmsImage->toArray());
    }

    /**
     * @test
     */
    public function testUpdateCmsImage()
    {
        $cmsImage = $this->makeCmsImage();
        $editedCmsImage = $this->fakeCmsImageData();

        $this->json('PUT', '/api/v1/cmsImages/'.$cmsImage->id, $editedCmsImage);

        $this->assertApiResponse($editedCmsImage);
    }

    /**
     * @test
     */
    public function testDeleteCmsImage()
    {
        $cmsImage = $this->makeCmsImage();
        $this->json('DELETE', '/api/v1/cmsImages/'.$cmsImage->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/cmsImages/'.$cmsImage->id);

        $this->assertResponseStatus(404);
    }
}
