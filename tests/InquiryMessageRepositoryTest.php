<?php namespace Tests\Repositories;

use App\Models\InquiryMessage;
use App\Repositories\InquiryMessageRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\Traits\MakeInquiryMessageTrait;
use Tests\ApiTestTrait;

class InquiryMessageRepositoryTest extends TestCase
{
    use MakeInquiryMessageTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var InquiryMessageRepository
     */
    protected $inquiryMessageRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->inquiryMessageRepo = \App::make(InquiryMessageRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_inquiry_message()
    {
        $inquiryMessage = $this->fakeInquiryMessageData();
        $createdInquiryMessage = $this->inquiryMessageRepo->create($inquiryMessage);
        $createdInquiryMessage = $createdInquiryMessage->toArray();
        $this->assertArrayHasKey('id', $createdInquiryMessage);
        $this->assertNotNull($createdInquiryMessage['id'], 'Created InquiryMessage must have id specified');
        $this->assertNotNull(InquiryMessage::find($createdInquiryMessage['id']), 'InquiryMessage with given id must be in DB');
        $this->assertModelData($inquiryMessage, $createdInquiryMessage);
    }

    /**
     * @test read
     */
    public function test_read_inquiry_message()
    {
        $inquiryMessage = $this->makeInquiryMessage();
        $dbInquiryMessage = $this->inquiryMessageRepo->find($inquiryMessage->id);
        $dbInquiryMessage = $dbInquiryMessage->toArray();
        $this->assertModelData($inquiryMessage->toArray(), $dbInquiryMessage);
    }

    /**
     * @test update
     */
    public function test_update_inquiry_message()
    {
        $inquiryMessage = $this->makeInquiryMessage();
        $fakeInquiryMessage = $this->fakeInquiryMessageData();
        $updatedInquiryMessage = $this->inquiryMessageRepo->update($fakeInquiryMessage, $inquiryMessage->id);
        $this->assertModelData($fakeInquiryMessage, $updatedInquiryMessage->toArray());
        $dbInquiryMessage = $this->inquiryMessageRepo->find($inquiryMessage->id);
        $this->assertModelData($fakeInquiryMessage, $dbInquiryMessage->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_inquiry_message()
    {
        $inquiryMessage = $this->makeInquiryMessage();
        $resp = $this->inquiryMessageRepo->delete($inquiryMessage->id);
        $this->assertTrue($resp);
        $this->assertNull(InquiryMessage::find($inquiryMessage->id), 'InquiryMessage should not exist in DB');
    }
}
