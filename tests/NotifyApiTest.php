<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class NotifyApiTest extends TestCase
{
    use MakeNotifyTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateNotify()
    {
        $notify = $this->fakeNotifyData();
        $this->json('POST', '/api/v1/notifies', $notify);

        $this->assertApiResponse($notify);
    }

    /**
     * @test
     */
    public function testReadNotify()
    {
        $notify = $this->makeNotify();
        $this->json('GET', '/api/v1/notifies/'.$notify->id);

        $this->assertApiResponse($notify->toArray());
    }

    /**
     * @test
     */
    public function testUpdateNotify()
    {
        $notify = $this->makeNotify();
        $editedNotify = $this->fakeNotifyData();

        $this->json('PUT', '/api/v1/notifies/'.$notify->id, $editedNotify);

        $this->assertApiResponse($editedNotify);
    }

    /**
     * @test
     */
    public function testDeleteNotify()
    {
        $notify = $this->makeNotify();
        $this->json('DELETE', '/api/v1/notifies/'.$notify->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/notifies/'.$notify->id);

        $this->assertResponseStatus(404);
    }
}
