<?php

use App\Models\Notify;
use App\Repositories\NotifyRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class NotifyRepositoryTest extends TestCase
{
    use MakeNotifyTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var NotifyRepository
     */
    protected $notifyRepo;

    public function setUp()
    {
        parent::setUp();
        $this->notifyRepo = App::make(NotifyRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateNotify()
    {
        $notify = $this->fakeNotifyData();
        $createdNotify = $this->notifyRepo->create($notify);
        $createdNotify = $createdNotify->toArray();
        $this->assertArrayHasKey('id', $createdNotify);
        $this->assertNotNull($createdNotify['id'], 'Created Notify must have id specified');
        $this->assertNotNull(Notify::find($createdNotify['id']), 'Notify with given id must be in DB');
        $this->assertModelData($notify, $createdNotify);
    }

    /**
     * @test read
     */
    public function testReadNotify()
    {
        $notify = $this->makeNotify();
        $dbNotify = $this->notifyRepo->find($notify->id);
        $dbNotify = $dbNotify->toArray();
        $this->assertModelData($notify->toArray(), $dbNotify);
    }

    /**
     * @test update
     */
    public function testUpdateNotify()
    {
        $notify = $this->makeNotify();
        $fakeNotify = $this->fakeNotifyData();
        $updatedNotify = $this->notifyRepo->update($fakeNotify, $notify->id);
        $this->assertModelData($fakeNotify, $updatedNotify->toArray());
        $dbNotify = $this->notifyRepo->find($notify->id);
        $this->assertModelData($fakeNotify, $dbNotify->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteNotify()
    {
        $notify = $this->makeNotify();
        $resp = $this->notifyRepo->delete($notify->id);
        $this->assertTrue($resp);
        $this->assertNull(Notify::find($notify->id), 'Notify should not exist in DB');
    }
}
