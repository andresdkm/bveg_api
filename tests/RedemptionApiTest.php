<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\Traits\MakeRedemptionTrait;
use Tests\ApiTestTrait;

class RedemptionApiTest extends TestCase
{
    use MakeRedemptionTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_redemption()
    {
        $redemption = $this->fakeRedemptionData();
        $this->response = $this->json('POST', '/api/redemptions', $redemption);

        $this->assertApiResponse($redemption);
    }

    /**
     * @test
     */
    public function test_read_redemption()
    {
        $redemption = $this->makeRedemption();
        $this->response = $this->json('GET', '/api/redemptions/'.$redemption->id);

        $this->assertApiResponse($redemption->toArray());
    }

    /**
     * @test
     */
    public function test_update_redemption()
    {
        $redemption = $this->makeRedemption();
        $editedRedemption = $this->fakeRedemptionData();

        $this->response = $this->json('PUT', '/api/redemptions/'.$redemption->id, $editedRedemption);

        $this->assertApiResponse($editedRedemption);
    }

    /**
     * @test
     */
    public function test_delete_redemption()
    {
        $redemption = $this->makeRedemption();
        $this->response = $this->json('DELETE', '/api/redemptions/'.$redemption->id);

        $this->assertApiSuccess();
        $this->response = $this->json('GET', '/api/redemptions/'.$redemption->id);

        $this->response->assertStatus(404);
    }
}
