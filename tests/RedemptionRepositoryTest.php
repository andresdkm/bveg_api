<?php namespace Tests\Repositories;

use App\Models\Redemption;
use App\Repositories\RedemptionRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\Traits\MakeRedemptionTrait;
use Tests\ApiTestTrait;

class RedemptionRepositoryTest extends TestCase
{
    use MakeRedemptionTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var RedemptionRepository
     */
    protected $redemptionRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->redemptionRepo = \App::make(RedemptionRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_redemption()
    {
        $redemption = $this->fakeRedemptionData();
        $createdRedemption = $this->redemptionRepo->create($redemption);
        $createdRedemption = $createdRedemption->toArray();
        $this->assertArrayHasKey('id', $createdRedemption);
        $this->assertNotNull($createdRedemption['id'], 'Created Redemption must have id specified');
        $this->assertNotNull(Redemption::find($createdRedemption['id']), 'Redemption with given id must be in DB');
        $this->assertModelData($redemption, $createdRedemption);
    }

    /**
     * @test read
     */
    public function test_read_redemption()
    {
        $redemption = $this->makeRedemption();
        $dbRedemption = $this->redemptionRepo->find($redemption->id);
        $dbRedemption = $dbRedemption->toArray();
        $this->assertModelData($redemption->toArray(), $dbRedemption);
    }

    /**
     * @test update
     */
    public function test_update_redemption()
    {
        $redemption = $this->makeRedemption();
        $fakeRedemption = $this->fakeRedemptionData();
        $updatedRedemption = $this->redemptionRepo->update($fakeRedemption, $redemption->id);
        $this->assertModelData($fakeRedemption, $updatedRedemption->toArray());
        $dbRedemption = $this->redemptionRepo->find($redemption->id);
        $this->assertModelData($fakeRedemption, $dbRedemption->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_redemption()
    {
        $redemption = $this->makeRedemption();
        $resp = $this->redemptionRepo->delete($redemption->id);
        $this->assertTrue($resp);
        $this->assertNull(Redemption::find($redemption->id), 'Redemption should not exist in DB');
    }
}
