<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\Traits\MakeInquiryMessageTrait;
use Tests\ApiTestTrait;

class InquiryMessageApiTest extends TestCase
{
    use MakeInquiryMessageTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_inquiry_message()
    {
        $inquiryMessage = $this->fakeInquiryMessageData();
        $this->response = $this->json('POST', '/api/inquiryMessages', $inquiryMessage);

        $this->assertApiResponse($inquiryMessage);
    }

    /**
     * @test
     */
    public function test_read_inquiry_message()
    {
        $inquiryMessage = $this->makeInquiryMessage();
        $this->response = $this->json('GET', '/api/inquiryMessages/'.$inquiryMessage->id);

        $this->assertApiResponse($inquiryMessage->toArray());
    }

    /**
     * @test
     */
    public function test_update_inquiry_message()
    {
        $inquiryMessage = $this->makeInquiryMessage();
        $editedInquiryMessage = $this->fakeInquiryMessageData();

        $this->response = $this->json('PUT', '/api/inquiryMessages/'.$inquiryMessage->id, $editedInquiryMessage);

        $this->assertApiResponse($editedInquiryMessage);
    }

    /**
     * @test
     */
    public function test_delete_inquiry_message()
    {
        $inquiryMessage = $this->makeInquiryMessage();
        $this->response = $this->json('DELETE', '/api/inquiryMessages/'.$inquiryMessage->id);

        $this->assertApiSuccess();
        $this->response = $this->json('GET', '/api/inquiryMessages/'.$inquiryMessage->id);

        $this->response->assertStatus(404);
    }
}
