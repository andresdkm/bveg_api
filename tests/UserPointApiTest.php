<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserPointApiTest extends TestCase
{
    use MakeUserPointTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateUserPoint()
    {
        $userPoint = $this->fakeUserPointData();
        $this->json('POST', '/api/v1/userPoints', $userPoint);

        $this->assertApiResponse($userPoint);
    }

    /**
     * @test
     */
    public function testReadUserPoint()
    {
        $userPoint = $this->makeUserPoint();
        $this->json('GET', '/api/v1/userPoints/'.$userPoint->id);

        $this->assertApiResponse($userPoint->toArray());
    }

    /**
     * @test
     */
    public function testUpdateUserPoint()
    {
        $userPoint = $this->makeUserPoint();
        $editedUserPoint = $this->fakeUserPointData();

        $this->json('PUT', '/api/v1/userPoints/'.$userPoint->id, $editedUserPoint);

        $this->assertApiResponse($editedUserPoint);
    }

    /**
     * @test
     */
    public function testDeleteUserPoint()
    {
        $userPoint = $this->makeUserPoint();
        $this->json('DELETE', '/api/v1/userPoints/'.$userPoint->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/userPoints/'.$userPoint->id);

        $this->assertResponseStatus(404);
    }
}
