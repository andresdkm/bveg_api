<?php

use App\Models\CmsImage;
use App\Repositories\CmsImageRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CmsImageRepositoryTest extends TestCase
{
    use MakeCmsImageTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var CmsImageRepository
     */
    protected $cmsImageRepo;

    public function setUp()
    {
        parent::setUp();
        $this->cmsImageRepo = App::make(CmsImageRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateCmsImage()
    {
        $cmsImage = $this->fakeCmsImageData();
        $createdCmsImage = $this->cmsImageRepo->create($cmsImage);
        $createdCmsImage = $createdCmsImage->toArray();
        $this->assertArrayHasKey('id', $createdCmsImage);
        $this->assertNotNull($createdCmsImage['id'], 'Created CmsImage must have id specified');
        $this->assertNotNull(CmsImage::find($createdCmsImage['id']), 'CmsImage with given id must be in DB');
        $this->assertModelData($cmsImage, $createdCmsImage);
    }

    /**
     * @test read
     */
    public function testReadCmsImage()
    {
        $cmsImage = $this->makeCmsImage();
        $dbCmsImage = $this->cmsImageRepo->find($cmsImage->id);
        $dbCmsImage = $dbCmsImage->toArray();
        $this->assertModelData($cmsImage->toArray(), $dbCmsImage);
    }

    /**
     * @test update
     */
    public function testUpdateCmsImage()
    {
        $cmsImage = $this->makeCmsImage();
        $fakeCmsImage = $this->fakeCmsImageData();
        $updatedCmsImage = $this->cmsImageRepo->update($fakeCmsImage, $cmsImage->id);
        $this->assertModelData($fakeCmsImage, $updatedCmsImage->toArray());
        $dbCmsImage = $this->cmsImageRepo->find($cmsImage->id);
        $this->assertModelData($fakeCmsImage, $dbCmsImage->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteCmsImage()
    {
        $cmsImage = $this->makeCmsImage();
        $resp = $this->cmsImageRepo->delete($cmsImage->id);
        $this->assertTrue($resp);
        $this->assertNull(CmsImage::find($cmsImage->id), 'CmsImage should not exist in DB');
    }
}
