<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\Traits\MakeInquiryTrait;
use Tests\ApiTestTrait;

class InquiryApiTest extends TestCase
{
    use MakeInquiryTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_inquiry()
    {
        $inquiry = $this->fakeInquiryData();
        $this->response = $this->json('POST', '/api/inquiries', $inquiry);

        $this->assertApiResponse($inquiry);
    }

    /**
     * @test
     */
    public function test_read_inquiry()
    {
        $inquiry = $this->makeInquiry();
        $this->response = $this->json('GET', '/api/inquiries/'.$inquiry->id);

        $this->assertApiResponse($inquiry->toArray());
    }

    /**
     * @test
     */
    public function test_update_inquiry()
    {
        $inquiry = $this->makeInquiry();
        $editedInquiry = $this->fakeInquiryData();

        $this->response = $this->json('PUT', '/api/inquiries/'.$inquiry->id, $editedInquiry);

        $this->assertApiResponse($editedInquiry);
    }

    /**
     * @test
     */
    public function test_delete_inquiry()
    {
        $inquiry = $this->makeInquiry();
        $this->response = $this->json('DELETE', '/api/inquiries/'.$inquiry->id);

        $this->assertApiSuccess();
        $this->response = $this->json('GET', '/api/inquiries/'.$inquiry->id);

        $this->response->assertStatus(404);
    }
}
