<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\Traits\MakeReplyTrait;
use Tests\ApiTestTrait;

class ReplyApiTest extends TestCase
{
    use MakeReplyTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_reply()
    {
        $reply = $this->fakeReplyData();
        $this->response = $this->json('POST', '/api/replies', $reply);

        $this->assertApiResponse($reply);
    }

    /**
     * @test
     */
    public function test_read_reply()
    {
        $reply = $this->makeReply();
        $this->response = $this->json('GET', '/api/replies/'.$reply->id);

        $this->assertApiResponse($reply->toArray());
    }

    /**
     * @test
     */
    public function test_update_reply()
    {
        $reply = $this->makeReply();
        $editedReply = $this->fakeReplyData();

        $this->response = $this->json('PUT', '/api/replies/'.$reply->id, $editedReply);

        $this->assertApiResponse($editedReply);
    }

    /**
     * @test
     */
    public function test_delete_reply()
    {
        $reply = $this->makeReply();
        $this->response = $this->json('DELETE', '/api/replies/'.$reply->id);

        $this->assertApiSuccess();
        $this->response = $this->json('GET', '/api/replies/'.$reply->id);

        $this->response->assertStatus(404);
    }
}
