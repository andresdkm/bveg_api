<?php namespace Tests\Traits;

use Faker\Factory as Faker;
use App\Models\InquiryMessage;
use App\Repositories\InquiryMessageRepository;

trait MakeInquiryMessageTrait
{
    /**
     * Create fake instance of InquiryMessage and save it in database
     *
     * @param array $inquiryMessageFields
     * @return InquiryMessage
     */
    public function makeInquiryMessage($inquiryMessageFields = [])
    {
        /** @var InquiryMessageRepository $inquiryMessageRepo */
        $inquiryMessageRepo = \App::make(InquiryMessageRepository::class);
        $theme = $this->fakeInquiryMessageData($inquiryMessageFields);
        return $inquiryMessageRepo->create($theme);
    }

    /**
     * Get fake instance of InquiryMessage
     *
     * @param array $inquiryMessageFields
     * @return InquiryMessage
     */
    public function fakeInquiryMessage($inquiryMessageFields = [])
    {
        return new InquiryMessage($this->fakeInquiryMessageData($inquiryMessageFields));
    }

    /**
     * Get fake data of InquiryMessage
     *
     * @param array $inquiryMessageFields
     * @return array
     */
    public function fakeInquiryMessageData($inquiryMessageFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $inquiryMessageFields);
    }
}
