<?php

use Faker\Factory as Faker;
use App\Models\ClientPoint;
use App\Repositories\UserPointRepository;

trait MakeUserPointTrait
{
    /**
     * Create fake instance of ClientPoint and save it in database
     *
     * @param array $userPointFields
     * @return ClientPoint
     */
    public function makeUserPoint($userPointFields = [])
    {
        /** @var UserPointRepository $userPointRepo */
        $userPointRepo = App::make(UserPointRepository::class);
        $theme = $this->fakeUserPointData($userPointFields);
        return $userPointRepo->create($theme);
    }

    /**
     * Get fake instance of ClientPoint
     *
     * @param array $userPointFields
     * @return ClientPoint
     */
    public function fakeUserPoint($userPointFields = [])
    {
        return new ClientPoint($this->fakeUserPointData($userPointFields));
    }

    /**
     * Get fake data of ClientPoint
     *
     * @param array $postFields
     * @return array
     */
    public function fakeUserPointData($userPointFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'user_id' => $fake->randomDigitNotNull,
            'place_id' => $fake->randomDigitNotNull,
            'points' => $fake->randomDigitNotNull,
            'description' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $userPointFields);
    }
}
