<?php

use Faker\Factory as Faker;
use App\Models\Notify;
use App\Repositories\NotifyRepository;

trait MakeNotifyTrait
{
    /**
     * Create fake instance of Notify and save it in database
     *
     * @param array $notifyFields
     * @return Notify
     */
    public function makeNotify($notifyFields = [])
    {
        /** @var NotifyRepository $notifyRepo */
        $notifyRepo = App::make(NotifyRepository::class);
        $theme = $this->fakeNotifyData($notifyFields);
        return $notifyRepo->create($theme);
    }

    /**
     * Get fake instance of Notify
     *
     * @param array $notifyFields
     * @return Notify
     */
    public function fakeNotify($notifyFields = [])
    {
        return new Notify($this->fakeNotifyData($notifyFields));
    }

    /**
     * Get fake data of Notify
     *
     * @param array $postFields
     * @return array
     */
    public function fakeNotifyData($notifyFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'message' => $fake->text,
            'total' => $fake->randomDigitNotNull,
            'date_add' => $fake->date('Y-m-d H:i:s'),
            'link' => $fake->text,
            'poster_id' => $fake->randomDigitNotNull,
            'price' => $fake->word,
            'price_ofert' => $fake->word,
            'title' => $fake->text,
            'date_valid' => $fake->date('Y-m-d H:i:s')
        ], $notifyFields);
    }
}
