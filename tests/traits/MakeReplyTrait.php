<?php namespace Tests\Traits;

use Faker\Factory as Faker;
use App\Models\Reply;
use App\Repositories\ReplyRepository;

trait MakeReplyTrait
{
    /**
     * Create fake instance of Reply and save it in database
     *
     * @param array $replyFields
     * @return Reply
     */
    public function makeReply($replyFields = [])
    {
        /** @var ReplyRepository $replyRepo */
        $replyRepo = \App::make(ReplyRepository::class);
        $theme = $this->fakeReplyData($replyFields);
        return $replyRepo->create($theme);
    }

    /**
     * Get fake instance of Reply
     *
     * @param array $replyFields
     * @return Reply
     */
    public function fakeReply($replyFields = [])
    {
        return new Reply($this->fakeReplyData($replyFields));
    }

    /**
     * Get fake data of Reply
     *
     * @param array $replyFields
     * @return array
     */
    public function fakeReplyData($replyFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $replyFields);
    }
}
