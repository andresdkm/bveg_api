<?php namespace Tests\Traits;

use Faker\Factory as Faker;
use App\Models\Inquiry;
use App\Repositories\InquiryRepository;

trait MakeInquiryTrait
{
    /**
     * Create fake instance of Inquiry and save it in database
     *
     * @param array $inquiryFields
     * @return Inquiry
     */
    public function makeInquiry($inquiryFields = [])
    {
        /** @var InquiryRepository $inquiryRepo */
        $inquiryRepo = \App::make(InquiryRepository::class);
        $theme = $this->fakeInquiryData($inquiryFields);
        return $inquiryRepo->create($theme);
    }

    /**
     * Get fake instance of Inquiry
     *
     * @param array $inquiryFields
     * @return Inquiry
     */
    public function fakeInquiry($inquiryFields = [])
    {
        return new Inquiry($this->fakeInquiryData($inquiryFields));
    }

    /**
     * Get fake data of Inquiry
     *
     * @param array $inquiryFields
     * @return array
     */
    public function fakeInquiryData($inquiryFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $inquiryFields);
    }
}
