<?php

use Faker\Factory as Faker;
use App\Models\CmsImage;
use App\Repositories\CmsImageRepository;

trait MakeCmsImageTrait
{
    /**
     * Create fake instance of CmsImage and save it in database
     *
     * @param array $cmsImageFields
     * @return CmsImage
     */
    public function makeCmsImage($cmsImageFields = [])
    {
        /** @var CmsImageRepository $cmsImageRepo */
        $cmsImageRepo = App::make(CmsImageRepository::class);
        $theme = $this->fakeCmsImageData($cmsImageFields);
        return $cmsImageRepo->create($theme);
    }

    /**
     * Get fake instance of CmsImage
     *
     * @param array $cmsImageFields
     * @return CmsImage
     */
    public function fakeCmsImage($cmsImageFields = [])
    {
        return new CmsImage($this->fakeCmsImageData($cmsImageFields));
    }

    /**
     * Get fake data of CmsImage
     *
     * @param array $postFields
     * @return array
     */
    public function fakeCmsImageData($cmsImageFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'title' => $fake->word,
            'file' => $fake->word,
            'width' => $fake->randomDigitNotNull,
            'height' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'focal_point_x' => $fake->randomDigitNotNull,
            'focal_point_y' => $fake->randomDigitNotNull,
            'focal_point_width' => $fake->randomDigitNotNull,
            'focal_point_height' => $fake->randomDigitNotNull,
            'uploaded_by_user_id' => $fake->randomDigitNotNull,
            'file_size' => $fake->randomDigitNotNull,
            'collection_id' => $fake->randomDigitNotNull
        ], $cmsImageFields);
    }
}
