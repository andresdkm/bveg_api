<?php namespace Tests\Traits;

use Faker\Factory as Faker;
use App\Models\Redemption;
use App\Repositories\RedemptionRepository;

trait MakeRedemptionTrait
{
    /**
     * Create fake instance of Redemption and save it in database
     *
     * @param array $redemptionFields
     * @return Redemption
     */
    public function makeRedemption($redemptionFields = [])
    {
        /** @var RedemptionRepository $redemptionRepo */
        $redemptionRepo = \App::make(RedemptionRepository::class);
        $theme = $this->fakeRedemptionData($redemptionFields);
        return $redemptionRepo->create($theme);
    }

    /**
     * Get fake instance of Redemption
     *
     * @param array $redemptionFields
     * @return Redemption
     */
    public function fakeRedemption($redemptionFields = [])
    {
        return new Redemption($this->fakeRedemptionData($redemptionFields));
    }

    /**
     * Get fake data of Redemption
     *
     * @param array $redemptionFields
     * @return array
     */
    public function fakeRedemptionData($redemptionFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'user_id' => $fake->randomDigitNotNull,
            'place_id' => $fake->randomDigitNotNull,
            'points' => $fake->word,
            'description' => $fake->word,
            'author_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $redemptionFields);
    }
}
