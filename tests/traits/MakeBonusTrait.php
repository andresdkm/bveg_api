<?php namespace Tests\Traits;

use Faker\Factory as Faker;
use App\Models\Bonus;
use App\Repositories\BonusRepository;

trait MakeBonusTrait
{
    /**
     * Create fake instance of Bonus and save it in database
     *
     * @param array $bonusFields
     * @return Bonus
     */
    public function makeBonus($bonusFields = [])
    {
        /** @var BonusRepository $bonusRepo */
        $bonusRepo = \App::make(BonusRepository::class);
        $theme = $this->fakeBonusData($bonusFields);
        return $bonusRepo->create($theme);
    }

    /**
     * Get fake instance of Bonus
     *
     * @param array $bonusFields
     * @return Bonus
     */
    public function fakeBonus($bonusFields = [])
    {
        return new Bonus($this->fakeBonusData($bonusFields));
    }

    /**
     * Get fake data of Bonus
     *
     * @param array $bonusFields
     * @return array
     */
    public function fakeBonusData($bonusFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'code' => $fake->word,
            'email' => $fake->word,
            'place_id' => $fake->randomDigitNotNull,
            'user_id' => $fake->randomDigitNotNull,
            'author_id' => $fake->randomDigitNotNull,
            'used_at' => $fake->date('Y-m-d H:i:s'),
            'amount' => $fake->word,
            'active' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $bonusFields);
    }
}
