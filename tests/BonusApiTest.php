<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\Traits\MakeBonusTrait;
use Tests\ApiTestTrait;

class BonusApiTest extends TestCase
{
    use MakeBonusTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_bonus()
    {
        $bonus = $this->fakeBonusData();
        $this->response = $this->json('POST', '/api/bonuses', $bonus);

        $this->assertApiResponse($bonus);
    }

    /**
     * @test
     */
    public function test_read_bonus()
    {
        $bonus = $this->makeBonus();
        $this->response = $this->json('GET', '/api/bonuses/'.$bonus->id);

        $this->assertApiResponse($bonus->toArray());
    }

    /**
     * @test
     */
    public function test_update_bonus()
    {
        $bonus = $this->makeBonus();
        $editedBonus = $this->fakeBonusData();

        $this->response = $this->json('PUT', '/api/bonuses/'.$bonus->id, $editedBonus);

        $this->assertApiResponse($editedBonus);
    }

    /**
     * @test
     */
    public function test_delete_bonus()
    {
        $bonus = $this->makeBonus();
        $this->response = $this->json('DELETE', '/api/bonuses/'.$bonus->id);

        $this->assertApiSuccess();
        $this->response = $this->json('GET', '/api/bonuses/'.$bonus->id);

        $this->response->assertStatus(404);
    }
}
