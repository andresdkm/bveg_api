<?php

use App\Models\ClientPoint;
use App\Repositories\UserPointRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserPointRepositoryTest extends TestCase
{
    use MakeUserPointTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var UserPointRepository
     */
    protected $userPointRepo;

    public function setUp()
    {
        parent::setUp();
        $this->userPointRepo = App::make(UserPointRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateUserPoint()
    {
        $userPoint = $this->fakeUserPointData();
        $createdUserPoint = $this->userPointRepo->create($userPoint);
        $createdUserPoint = $createdUserPoint->toArray();
        $this->assertArrayHasKey('id', $createdUserPoint);
        $this->assertNotNull($createdUserPoint['id'], 'Created ClientPoint must have id specified');
        $this->assertNotNull(ClientPoint::find($createdUserPoint['id']), 'ClientPoint with given id must be in DB');
        $this->assertModelData($userPoint, $createdUserPoint);
    }

    /**
     * @test read
     */
    public function testReadUserPoint()
    {
        $userPoint = $this->makeUserPoint();
        $dbUserPoint = $this->userPointRepo->find($userPoint->id);
        $dbUserPoint = $dbUserPoint->toArray();
        $this->assertModelData($userPoint->toArray(), $dbUserPoint);
    }

    /**
     * @test update
     */
    public function testUpdateUserPoint()
    {
        $userPoint = $this->makeUserPoint();
        $fakeUserPoint = $this->fakeUserPointData();
        $updatedUserPoint = $this->userPointRepo->update($fakeUserPoint, $userPoint->id);
        $this->assertModelData($fakeUserPoint, $updatedUserPoint->toArray());
        $dbUserPoint = $this->userPointRepo->find($userPoint->id);
        $this->assertModelData($fakeUserPoint, $dbUserPoint->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteUserPoint()
    {
        $userPoint = $this->makeUserPoint();
        $resp = $this->userPointRepo->delete($userPoint->id);
        $this->assertTrue($resp);
        $this->assertNull(ClientPoint::find($userPoint->id), 'ClientPoint should not exist in DB');
    }
}
