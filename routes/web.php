<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/user/code/{userId}', 'UserCodeController@show');

Route::get('/bonus/code/{bonusId}', 'BonusCodeController@show');

Route::get('/bonus/{code}', 'BonusCodeController@view');

Route::get('/logout', 'HomeController@logout');

Route::middleware(['auth'])->group(function () {

    Route::get('/', 'HomeController@index');

    Route::resource('users', 'UserController');

    Route::resource('places', 'PlaceController');

	Route::resource('places/{placeId}/points', 'ClientPointController');

	Route::resource('places/{placeId}/bonus', 'BonusController');

    Route::resource('promotions', 'PromotionController');

    Route::resource('redemptions', 'RedemptionController');
});


Route::resource('clients', 'ClientController');

Route::resource('promotions', 'PromotionController');

Route::resource('banners', 'BannerController');

Route::get('images/{name}', 'ImageController@make');


Route::get("/mail", function () {

});





Route::resource('comments', 'CommentController');

Route::resource('banners', 'BannerController');