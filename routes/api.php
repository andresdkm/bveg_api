<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get( 'places/search', 'PlaceSearchAPIController@search' );

Route::get( 'places/{placeId}/configuration', 'PlaceConfigurationAPIController@show' );

Route::post( 'places/{placeId}/configuration', 'PlaceConfigurationAPIController@update' );

Route::resource( 'places', 'PlaceAPIController' );

Route::post( 'points', 'ClientPointAPIController@store' );

/***** public ****/


Route::resource( 'clients/{clientId}/favorites', 'ClientFavoriteAPIController' );

Route::resource( 'promotions', 'PromotionAPIController' );

Route::get( 'clients/{clientId}/places', 'ClientPlaceAPIController@index' );

Route::post( 'clients/search', 'ClientSearchAPIController@search' );

Route::get( 'clients/{clientId}/bonuses', 'ClientBonusAPIController@index' );

Route::get( 'clients/{clientId}/points', 'PointAPIController@index' );

Route::get( 'clients/{clientId}/points/places/{placeId}', 'PointAPIController@show' );

/*******************************************************************************/

Route::get( 'clients/{clientId}/comments', 'ClientCommentAPIController@index' );


Route::get( 'users/{userId}/points/places/{placeId}', 'PointAPIController@show' );


Route::resource( 'users', 'UserAPIController' );

Route::resource( 'places', 'PlaceAPIController' );

Route::resource( 'comments', 'CommentAPIController' );

Route::resource( 'comments/{commentId}/replies', 'ReplyAPIController' );

Route::resource( 'categories', 'CategoryAPIController' );

Route::resource( 'notifies', 'NotifyAPIController' );

Route::resource( 'cms_images', 'CmsImageAPIController' );


Route::resource( 'offers', 'OfferAPIController' );

Route::post( 'login', 'LoginAPIController@login' );

Route::post( 'redemptions', 'RedemptionAPIController@store' );


Route::post( 'users/{userId}/send', 'ClientVerificationAPIController@send' );


Route::get( 'places/{placeId}/bonuses', 'PlaceBonusAPIController@index' );

Route::post( 'bonuses/search', 'SearchBonusAPIController@store' );

Route::resource( 'bonuses', 'BonusAPIController' );

Route::resource( 'clients', 'ClientAPIController' );


Route::post( 'clients/verify', 'ClientVerificationAPIController@verify' );

Route::post( 'clients/login', 'ClientVerificationAPIController@login' );


Route::resource( 'banners', 'BannerAPIController' ,['only' => 'index,show']);

Route::post( 'inquiries/search', 'InquiryAPIController@search' );

Route::get( 'inquiries/{inquiryId}/messages', 'InquiryMessageAPIController@index' );

Route::post( 'inquiries/{inquiryId}/messages', 'InquiryMessageAPIController@store' );

Route::resource( 'inquiries', 'InquiryAPIController' );

Route::post( 'devices', 'DeviceAPIController@store' );

Route::post( 'resources', 'ResourceAPIController@store' );

Route::resource('countries', 'CountryAPIController');

Route::post( 'account/verify', 'AccountVerificationAPIController@verify' );


Route::resource('banners', 'BannerAPIController');